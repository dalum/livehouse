import json
import os

from copy import deepcopy
from pathlib import Path


class Bank:
    def __init__(self, name):
        self.name = name

        self.load_globals()
        self.load_patches()
        self.load_default_session()
        self.session = deepcopy(self.default_session)

    @property
    def banks_path(self):
        return Path(
            os.environ.get("LIVEHOUSE_BANKS", os.path.expanduser("~/.livehouse/banks"))
        )

    ##################################################
    ## Globals
    ##################################################

    def get_globals_path(self):
        return self.banks_path.joinpath("globals").joinpath(f"{self.name}.json")

    def load_globals(self):
        file_path = self.get_globals_path()
        if os.path.exists(file_path):
            try:
                with open(file_path, "r") as fh:
                    self.globals = json.load(fh)
            except json.decoder.JSONDecodeError:
                self.globals = {}
        else:
            self.globals = {}

    def save_globals(self):
        file_path = self.get_globals_path()
        file_path.parents[0].mkdir(parents=True, exist_ok=True)
        with open(file_path, "w") as fh:
            json.dump(self.globals, fh, indent=2)

    def get_global(self, setting, default=None):
        if setting not in self.globals:
            self.globals[setting] = default
        return self.globals[setting]

    def set_global(self, setting, value):
        self.globals[setting] = value

    ##################################################
    ## Patches
    ##################################################

    def get_patches_path(self):
        return self.banks_path.joinpath("patches").joinpath(f"{self.name}.json")

    def load_patches(self):
        file_path = self.get_patches_path()
        if os.path.exists(file_path):
            try:
                with open(file_path, "r") as fh:
                    self.patches = json.load(fh)
            except json.decoder.JSONDecodeError:
                self.patches = {}
        else:
            self.patches = {}

    def save_patches(self):
        file_path = self.get_patches_path()
        file_path.parents[0].mkdir(parents=True, exist_ok=True)
        with open(file_path, "w") as fh:
            json.dump(self.patches, fh, indent=2)

    def get_patch(self, idx, default=None):
        if idx not in self.patches:
            self.patches[idx] = {"metadata": {}, "data": default}
        return self.patches[idx]

    def set_patch(self, idx, data, metadata=None):
        self.patches[idx] = {
            "metadata": {} if metadata is None else metadata,
            "data": data,
        }

    ##################################################
    ## Session
    ##################################################

    def get_default_session_path(self):
        return self.banks_path.joinpath("default").joinpath(f"{self.name}.json")

    def load_default_session(self):
        file_path = self.get_default_session_path()
        if os.path.exists(file_path):
            try:
                with open(file_path, "r") as fh:
                    self.default_session = json.load(fh)
                    if not isinstance(self.default_session, dict):
                        self.default_session = {"data": self.default_session}
            except json.decoder.JSONDecodeError:
                self.default_session = {}
        else:
            self.default_session = {}

    def get_session_path(self, idx):
        return self.banks_path.joinpath(str(idx)).joinpath(f"{self.name}.json")

    def load_session(self, idx):
        file_path = self.get_session_path(idx)
        if os.path.exists(file_path):
            try:
                with open(file_path, "r") as fh:
                    self.session = json.load(fh)
                    if not isinstance(self.session, dict):
                        self.session = {"data": self.session}
            except json.decoder.JSONDecodeError:
                self.session = deepcopy(self.default_session)
        else:
            self.session = deepcopy(self.default_session)

    def clear_session(self, idx):
        self.session = deepcopy(self.default_session)
        self.save_session(idx)
        print("[BANK %s] Cleared session: %s" % (self.name, idx))

    def save_session(self, idx):
        file_path = self.get_session_path(idx)
        print("[BANK %s] Saving to: %s ..." % (self.name, file_path), end=" ")
        try:
            file_path.parents[0].mkdir(parents=True, exist_ok=True)
            with open(file_path, "w") as fh:
                json.dump(self.session, fh, indent=2)
        except Exception:
            print(self.session)
            raise

        print("done.")

    def get(self, setting, default=None):
        if setting not in self.session:
            self.session[setting] = default
        return self.session[setting]

    def set(self, setting, value):
        self.session[setting] = value

    ##################################################
    ## Session peeking
    ##################################################

    def get_from_session(self, idx, setting, default=None):
        file_path = self.get_session_path(idx)
        if not os.path.exists(file_path):
            return default
        with open(file_path, "r") as fh:
            return json.load(fh).get(setting, default)

    def set_in_session(self, idx, setting, value):
        file_path = self.get_session_path(idx)
        if not os.path.exists(file_path):
            return

        with open(file_path, "r") as fh:
            session = json.load(fh)

        session[setting] = value

        with open(file_path, "w") as fh:
            json.dump(session, fh)
