from threading import Thread
from time import sleep

class Buffer:
    interval = 0.001

    def start(self):
        t = Thread(target=self.send_loop, args=())
        t.daemon = True
        t.start()

    def send_loop(self):
        while True:
            self.send()
            sleep(self.interval)

    def send(self):
        pass
