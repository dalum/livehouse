from .miditypes import *


def note_to_tuple(note):
    if "mod" in note:
        return (note["tag"], note["channel"], note["note"], note["mod"])
    return (note["tag"], note["channel"], note["note"])


def tuple_to_note(tup):
    if len(tup) == 3:
        return {
            "tag": tup[0],
            "channel": tup[1],
            "note": tup[2],
        }
    else:
        return {
            "tag": tup[0],
            "channel": tup[1],
            "note": tup[2],
            "mod": tup[3],
        }


def spec_to_pair(spec):
    if spec["type"] == CC:
        return (
            (
                spec["tag"],
                spec["channel"],
                spec["type"],
                spec["controller"],
            ),
            spec["value"],
        )
    elif spec["type"] == NRPN:
        return (
            (
                spec["tag"],
                spec["channel"],
                spec["type"],
                spec["msb"],
                spec["lsb"],
            ),
            spec["value"],
        )
    raise NotImplementedError


def tuple_val_to_spec(tup, val):
    if tup[2] == CC:
        return {
            "tag": tup[0],
            "channel": tup[1],
            "type": tup[2],
            "controller": tup[3],
            "value": val,
        }
    elif tup[2] == NRPN:
        return {
            "tag": tup[0],
            "channel": tup[1],
            "type": tup[2],
            "msb": tup[3],
            "lsb": tup[4],
            "value": val,
        }
    raise NotImplementedError


def load_grid(grid):
    return {(p["x"], p["y"]): p["color"] for p in grid}


def dump_grid(grid):
    return [
        {"x": k[0], "y": k[1], "color": v}
        for k, v in grid.items()
        if isinstance(k, tuple)
    ]
