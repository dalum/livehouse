import os

from collections import defaultdict
from threading import Thread
from time import sleep, time

from ..miditypes import *
from ..drivers import Circuit
from ..services import GenericHandler

osc_waves = ('sin ', 'tri ', 'saw ', 'saw9', 'saw8', 'saw7', 'saw6', 'saw5', 'saw4', 'saw3', 'saw2', 'saw1', 'puls', 'sqr ', 'sint', 'anpu', 'asnc', 'trsw', 'dig1', 'dig2', 'dssq', 'div1', 'div2', 'div3', 'div4', 'div5', 'div6', 'ran1', 'ran2', 'ran3')
lfo_waves = ('sin ', 'tri ', 'saw ', 'sqr ', 'rand', 'time', 'pian', 'seq1', 'seq2', 'seq3', 'seq4', 'seq5', 'seq6', 'seq7', 'alt1', 'alt2', 'alt3', 'alt4', 'alt5', 'alt6', 'alt7', 'alt8', 'chro', 'cr16', 'majr', 'maj7', 'min7', 'arp1', 'arp2', 'dim ', 'decm', 'min3', 'pedl', '4ths', '4x12', '1maj', '1min', '2511')
fltr_types = ('lo12', 'lo24', 'bp6 ', 'bp12', 'hi12', 'hi24')
dist_types = ('diod', 'valv', 'clip', 'xovr', 'rect', 'bitr', 'rate')
reverb_types = ('cham', 's rm', 'l rm', 's hl', 'l hl', 'g hl')
syncs = ('off ', 't/32', '1/32', 't/16', '1/16', 't/8 ', 'd/16', '1/8 ', 't/4 ', 'd/8 ', '1/4 ', '11/3', 'd/4 ', '1/2 ', '22/3', '3bts', '4bts', '51/3', '6bts', '8bts', '1023', '12bs', '1313', '16bs', '18bs', '1823', '20bs', '2113', '24bs', '28bs', '30bs', '32bs', '36bs', '42bs', '48bs', '64bs')
off_on = ('off ', 'on  ')
fade_modes = ('fin ', 'fout', 'gin ', 'gout')
ratios = ('1:1 ', '4:3 ', '3:4 ', '3:2 ', '2:3 ', '2:1 ', '1:2 ', '3:1 ', '1:3 ', '4:1 ', '1:4 ', '1:of', 'of:1')

mod_sources = ('dir ', 'mwhl', 'aftr', 'expr', 'vel ', 'key ', 'lfo1', 'lf1-', 'lfo2', 'lf2-', 'env1', 'env2', 'env3')
mod_destinations = ('12pi', '1pit', '2pit', '1vsy', '2vsy', '1pwd', '2pwd', 'osc1', 'osc2', 'nois', 'ring', 'driv', 'freq', 'res ', 'lfo1', 'lfo2', 'dec1', 'dec2')

macro_destinations = (
    '   -',
    'prta', 'post',
    '1int', '1pwd', '1vsy', '1den', '1dtn', '1sem', '1cen',
    '2int', '2pwd', '2vsy', '2den', '2dtn', '2sem', '2cen',
    'osc1', 'osc2', 'ring', 'nois',
    'freq', 'res ', 'driv', 'trak', '2mod',
    '1atk', '1dec', '1sus', '1rel',
    '2atk', '2dec', '2sus', '2rel',
    '3del', '3atk', '3dec', '3sus', '3rel',
    '1rat', '1syn', '1slw', '2rat', '2syn', '2slw',
    'dist', 'chor', 'crat', 'cfdb', 'cdpt', 'cdel',
    'm  1', 'm  2', 'm  3', 'm  4', 'm  5', 'm  6', 'm  7', 'm  8',
    'm  9', 'm 10', 'm 11', 'm 12', 'm 13', 'm 14', 'm 15', 'm 16',
    'm 17', 'm 18', 'm 19', 'm 20'
    )

sidechain_sources = ('   1', '   2', '   3', '   4', 'off ')

user_destinations = (
    '   -',
    'prta',
    '1int', '1pwd', '1vsy', '1den', '1dtn', '1sem', '1cen', '1pit',
    '2int', '2pwd', '2vsy', '2den', '2dtn', '2sem', '2cen', '2pit',
    'osc1', 'osc2', 'ring', 'nois',
    'freq', 'res ', 'driv', 'trak', '2mod',
    '1atk', '1dec', '1sus', '1rel',
    '2atk', '2dec', '2sus', '2rel',
    '3del', '3atk', '3dec', '3sus', '3rel',
    '1rat', '1syn', '1slw', '2rat', '2syn', '2slw',
    'dist', 'chor', 'crat', 'cfdb', 'cdpt', 'cdel',
    'm  1', 'm  2', 'm  3', 'm  4', 'm  5', 'm  6', 'm  7', 'm  8',
    'm  9', 'm 10', 'm 11', 'm 12', 'm 13', 'm 14', 'm 15', 'm 16',
    'm 17', 'm 18', 'm 19', 'm 20'
)

menu = (
    (('pg 1', 54), (
        (('osc1', 40), (
            (('wave', 40, ('patch', 36)), osc_waves, range(30), 2, False),
            (('intp', 40, ('patch', 37)), range(128), range(128), 0, True),
            (('pwid', 40, ('patch', 38)), range(-64, 64), range(128), 127, True),
            (('vsyn', 40, ('patch', 39)), range(128), range(128), 0, True),
            (('dens', 40, ('patch', 40)), range(128), range(128), 0, True),
            (('dtun', 40, ('patch', 41)), range(128), range(128), 0, True),
            (('semi', 40, ('patch', 42)), range(-64, 64), range(128), 64, True),
            (('cent', 40, ('patch', 43)), range(-64, 64), range(128), 64, True),
            (('bend', 40, ('patch', 44)), range(-64, 64), range(128), 64, False),
        )),
        (('osc2', 42), (
            (('wave', 42, ('patch', 45)), osc_waves, range(30), 2, False),
            (('intp', 42, ('patch', 46)), range(128), range(128), 0, True),
            (('pwid', 42, ('patch', 47)), range(-64, 64), range(128), 127, True),
            (('vsyn', 42, ('patch', 48)), range(128), range(128), 0, True),
            (('dens', 42, ('patch', 49)), range(128), range(128), 0, True),
            (('dtun', 42, ('patch', 50)), range(128), range(128), 0, True),
            (('semi', 42, ('patch', 51)), range(-64, 64), range(128), 64, True),
            (('cent', 42, ('patch', 52)), range(-64, 64), range(128), 64, True),
            (('bend', 42, ('patch', 53)), range(-64, 64), range(128), 64, False),
        )),
        (('mix ', 32), (
            (('osc1', 32, ('patch', 54)), range(128), range(128), 127, True),
            (('osc2', 32, ('patch', 55)), range(128), range(128), 0, True),
            (('ring', 32, ('patch', 56)), range(128), range(128), 0, True),
            (('nois', 32, ('patch', 57)), range(128), range(128), 0, True),
            (('pre ', 32, ('patch', 58)), range(-12, 19), range(52, 83), 64, False),
            (('post', 32, ('patch', 59)), range(-12, 19), range(52, 83), 64, True),
        )),
        (('fltr', 8), (
            (('freq', 8, ('patch', 64)), range(128), range(128), 127, True),
            (('res ', 8, ('patch', 66)), range(128), range(128), 0, True),
            (('qnor', 8, ('patch', 67)), range(128), range(128), 64, False),
            (('trak', 8, ('patch', 65)), range(128), range(128), 127, True),
            (('type', 8, ('patch', 63)), fltr_types, range(6), 1, False),
            (('driv', 8, ('patch', 61)), range(128), range(128), 0, True),
            (('dtyp', 8, ('patch', 62)), dist_types, range(7), 0, False),
            (('rout', 8, ('patch', 60)), ('norm', 'osc1', 'os12'), range(3), 0, False),
            (('2mod', 8, ('patch', 68)), range(-64, 64), range(128), 64, True),

        )),
        (('voic', 56), (
            (('mode', 56, ('patch', 32)), ('mono', 'm ag', 'poly'), range(3), 2, False),
            (('prta', 56, ('patch', 33)), range(128), range(128), 0, True),
            (('glid', 56, ('patch', 34)), range(-12, 13), range(52, 77), 64, False),
            (('oct ', 56, ('patch', 35)), range(-6, 6), range(58, 70), 64, False),
        )),
    )),
    (('pg 2', 54), (
        (('env1', 96), (
            (('attk', 96, ('patch', 70)), range(128), range(128), 2, True),
            (('decy', 96, ('patch', 71)), range(128), range(128), 90, True),
            (('sust', 96, ('patch', 72)), range(128), range(128), 127, True),
            (('rels', 96, ('patch', 73)), range(128), range(128), 40, True),
            (('velo', 96, ('patch', 69)), range(-64, 64), range(128), 64, False),
        )),
        (('env2', 96), (
            (('attk', 96, ('patch', 75)), range(128), range(128), 2, True),
            (('decy', 96, ('patch', 76)), range(128), range(128), 75, True),
            (('sust', 96, ('patch', 77)), range(128), range(128), 35, True),
            (('rels', 96, ('patch', 78)), range(128), range(128), 45, True),
            (('velo', 96, ('patch', 74)), range(-64, 64), range(128), 64, False),
        )),
        (('env3', 96), (
            (('attk', 96, ('patch', 80)), range(128), range(128), 10, True),
            (('decy', 96, ('patch', 81)), range(128), range(128), 70, True),
            (('sust', 96, ('patch', 82)), range(128), range(128), 64, True),
            (('rels', 96, ('patch', 83)), range(128), range(128), 40, True),
            (('dela', 96, ('patch', 79)), range(128), range(128), 0, True),
        )),
        (('lfo1', 12), (
            (('rate', 12, ('patch', 89)), range(128), range(128), 68, True),
            (('sync', 12, ('patch', 90)), syncs, range(36), 0, True),
            (('wave', 12, ('patch', 84)), lfo_waves, range(38), 0, False),
            (('phas', 12, ('patch', 85)), range(0, 360, 3), range(120), 0, False),
            (('slew', 12, ('patch', 86)), range(128), range(128), 0, True),
            (('dela', 12, ('patch', 87)), range(128), range(128), 0, False),
            (('dsyn', 12, ('patch', 88)), syncs, range(36), 0, False),
            (('one ', 12, ('patch', 91, 0, 1)), off_on, (12, 13), 12, False),
            (('key ', 12, ('patch', 91, 1, 1)), off_on, (14, 15), 14, False),
            (('comm', 12, ('patch', 91, 2, 1)), off_on, (16, 17), 16, False),
            (('dtrg', 12, ('patch', 91, 3, 1)), off_on, (18, 19), 18, False),
            (('fade', 12, ('patch', 91, 4, 2)), fade_modes, range(4), 0, False),
        )),
        (('lfo2', 12), (
            (('rate', 12, ('patch', 97)), range(128), range(128), 68, True),
            (('sync', 12, ('patch', 98)), syncs, range(36), 0, True),
            (('wave', 12, ('patch', 92)), lfo_waves, range(38), 0, False),
            (('phas', 12, ('patch', 93)), range(0, 360, 3), range(120), 0, False),
            (('slew', 12, ('patch', 94)), range(128), range(128), 0, True),
            (('dela', 12, ('patch', 95)), range(128), range(128), 0, False),
            (('dsyn', 12, ('patch', 96)), syncs, range(36), 0, False),
            (('one ', 12, ('patch', 99, 0, 1)), off_on, (22, 23), 22, False),
            (('key ', 12, ('patch', 99, 1, 1)), off_on, (24, 25), 24, False),
            (('comm', 12, ('patch', 99, 2, 1)), off_on, (26, 27), 26, False),
            (('dtrg', 12, ('patch', 99, 3, 1)), off_on, (28, 29), 28, False),
            (('fade', 12, ('patch', 99, 4, 2)), fade_modes, range(4, 8), 4, False),
        )),
        (('dist', 48), (
            (('lvl ', 48, ('patch', 100)), range(128), range(128), 0, True),
            (('type', 48, ('patch', 116)), dist_types, range(7), 0, False),
            (('comp', 48, ('patch', 117)), range(128), range(128), 100, False),
        )),
        (('chor', 48), (
            (('lvl ', 48, ('patch', 102)), range(128), range(128), 0, True),
            (('type', 48, ('patch', 118)), ('phas', 'chor'), (0, 1), 1, False),
            (('rate', 48, ('patch', 119)), range(128), range(128), 84, True),
            (('sync', 48, ('patch', 120)), syncs, range(36), 0, False),
            (('fdbk', 48, ('patch', 121)), range(-64, 64), range(128), 74, True),
            (('dpth', 48, ('patch', 122)), range(128), range(128), 64, True),
            (('dela', 48, ('patch', 123)), range(128), range(128), 64, True),
        )),
        (('eq  ', 48), (
            (('bfrq', 48, ('patch', 105)), range(128), range(128), 64, False),
            (('blvl', 48, ('patch', 106)), range(-64, 64), range(128), 64, False),
            (('mfrq', 48, ('patch', 107)), range(128), range(128), 64, False),
            (('mlvl', 48, ('patch', 108)), range(-64, 64), range(128), 64, False),
            (('tfrq', 48, ('patch', 109)), range(128), range(128), 125, False),
            (('tlvl', 48, ('patch', 110)), range(-64, 64), range(128), 64, False),
        )),
    )),

    (('mod ', 64), (*(
        (('%2d%2d' % (4*i+1, 4*(i+1)), 64), (
            (('src1', 64, ('patch', 124+16*i)), mod_sources, range(13), 0, False),
            (('src2', 64, ('patch', 125+16*i)), mod_sources, range(13), 0, False),
            (('dpth', 64, ('patch', 126+16*i)), range(-64, 64), range(128), 64, True),
            (('dest', 64, ('patch', 127+16*i)), mod_destinations, range(18), 0, False),
            (('src1', 66, ('patch', 128+16*i)), mod_sources, range(13), 0, False),
            (('src2', 66, ('patch', 129+16*i)), mod_sources, range(13), 0, False),
            (('dpth', 66, ('patch', 130+16*i)), range(-64, 64), range(128), 64, True),
            (('dest', 66, ('patch', 131+16*i)), mod_destinations, range(18), 0, False),
            (('src1', 74, ('patch', 132+16*i)), mod_sources, range(13), 0, False),
            (('src2', 74, ('patch', 133+16*i)), mod_sources, range(13), 0, False),
            (('dpth', 74, ('patch', 134+16*i)), range(-64, 64), range(128), 64, True),
            (('dest', 74, ('patch', 135+16*i)), mod_destinations, range(18), 0, False),
            (('src1', 77, ('patch', 136+16*i)), mod_sources, range(13), 0, False),
            (('src2', 77, ('patch', 137+16*i)), mod_sources, range(13), 0, False),
            (('dpth', 77, ('patch', 138+16*i)), range(-64, 64), range(128), 64, True),
            (('dest', 77, ('patch', 139+16*i)), mod_destinations, range(18), 0, False),
            )) for i in range(5)),
    )),

    (('mac ', 84), (*(
        (('mac%d' % (i+1), 84), (
            (('dest', 84, ('patch', 205+17*i)), macro_destinations, range(71), 0, False),
            (('bgin', 84, ('patch', 206+17*i)), range(128), range(128), 0, False),
            (('end ', 84, ('patch', 207+17*i)), range(128), range(128), 127, False),
            (('dpth', 84, ('patch', 208+17*i)), range(-64, 64), range(128), 64, False),
            (('dest', 95, ('patch', 209+17*i)), macro_destinations, range(71), 0, False),
            (('bgin', 95, ('patch', 210+17*i)), range(128), range(128), 0, False),
            (('end ', 95, ('patch', 211+17*i)), range(128), range(128), 127, False),
            (('dpth', 95, ('patch', 212+17*i)), range(-64, 64), range(128), 64, False),
            (('dest', 109, ('patch', 213+17*i)), macro_destinations, range(71), 0, False),
            (('bgin', 109, ('patch', 214+17*i)), range(128), range(128), 0, False),
            (('end ', 109, ('patch', 215+17*i)), range(128), range(128), 127, False),
            (('dpth', 109, ('patch', 216+17*i)), range(-64, 64), range(128), 64, False),
            (('dest', 94, ('patch', 217+17*i)), macro_destinations, range(71), 0, False),
            (('bgin', 94, ('patch', 218+17*i)), range(128), range(128), 0, False),
            (('end ', 94, ('patch', 219+17*i)), range(128), range(128), 127, False),
            (('dpth', 94, ('patch', 220+17*i)), range(-64, 64), range(128), 64, False),
        )) for i in range(8)),
    )),

    (('usr ', 42), (*(
        (('usr%d' % (i+1), 42), (
            (('dest', 42, ('user', 16*i)), user_destinations, range(72), 0, False),
            (('bgin', 42, ('user', 16*i+2)), range(128), range(128), 0, False),
            (('end ', 42, ('user', 16*i+3)), range(128), range(128), 127, False),
            (('dpth', 42, ('user', 16*i+1)), range(-128, 128), range(-128, 128), 0, False, False),
            (('dest', 44, ('user', 16*i+4)), user_destinations, range(72), 0, False),
            (('bgin', 44, ('user', 16*i+6)), range(128), range(128), 0, False),
            (('end ', 44, ('user', 16*i+7)), range(128), range(128), 127, False),
            (('dpth', 44, ('user', 16*i+5)), range(-128, 128), range(-128, 128), 0, False, False),
            (('dest', 50, ('user', 16*i+8)), user_destinations, range(72), 0, False),
            (('bgin', 50, ('user', 16*i+10)), range(128), range(128), 0, False),
            (('end ', 50, ('user', 16*i+11)), range(128), range(128), 127, False),
            (('dpth', 50, ('user', 16*i+9)), range(-128, 128), range(-128, 128), 0, False, False),
            (('dest', 52, ('user', 16*i+12)), user_destinations, range(72), 0, False),
            (('bgin', 52, ('user', 16*i+14)), range(128), range(128), 0, False),
            (('end ', 52, ('user', 16*i+15)), range(128), range(128), 127, False),
            (('dpth', 52, ('user', 16*i+13)), range(-128, 128), range(-128, 128), 0, False),
        )) for i in range(8)),
    )),
    (('sess', 57), (
        (('rev ', 57), (
            (('type', 57, ('session', 6)), reverb_types, range(6), 2, False),
            (('dec ', 57, ('session', 7)), range(128), range(128), 64, False),
            (('damp', 57, ('session', 8)), range(128), range(128), 64, False),
            (('syn1', 53, ('session', 0)), range(128), range(128), 0, False),
            (('syn2', 25, ('session', 1)), range(128), range(128), 0, False),
            (('drm1', 14, ('session', 2)), range(128), range(128), 0, False),
            (('drm2', 14, ('session', 3)), range(128), range(128), 0, False),
            (('drm3', 14, ('session', 4)), range(128), range(128), 0, False),
            (('drm4', 14, ('session', 5)), range(128), range(128), 0, False),
        )),
        (('del ', 96), (
            (('time', 96, ('session', 15)), range(128), range(128), 64, False),
            (('sync', 96, ('session', 16)), syncs, range(36), 20, False),
            (('fdbk', 96, ('session', 17)), range(128), range(128), 64, False),
            (('wdth', 96, ('session', 18)), range(128), range(128), 127, False),
            (('l-r ', 96, ('session', 19)), ratios, range(13), 4, False),
            (('slew', 96, ('session', 20)), range(128), range(128), 5, False),
            (('syn1', 53, ('session', 9)), range(128), range(128), 0, False),
            (('syn2', 25, ('session', 10)), range(128), range(128), 0, False),
            (('drm1', 14, ('session', 11)), range(128), range(128), 0, False),
            (('drm2', 14, ('session', 12)), range(128), range(128), 0, False),
            (('drm3', 14, ('session', 13)), range(128), range(128), 0, False),
            (('drm4', 14, ('session', 14)), range(128), range(128), 0, False),
        )),
        (('fltr', 8), (
            (('freq', 8, ('session', 21)), range(128), range(128), 64, False),
            (('res ', 8, ('session', 22)), range(128), range(128), 30, False),
        )),
        (('s-c1', 55), (
            (('src ', 55, ('session', 23)), sidechain_sources, range(5), 0, False),
            (('atk ', 55, ('session', 24)), range(128), range(128), 0, False),
            (('hold', 55, ('session', 25)), range(128), range(128), 50, False),
            (('dec ', 55, ('session', 26)), range(128), range(128), 70, False),
            (('dpth', 55, ('session', 27)), range(128), range(128), 0, False),
        )),
        (('s-c2', 27), (
            (('src ', 27, ('session', 28)), sidechain_sources, range(5), 0, False),
            (('atk ', 27, ('session', 29)), range(128), range(128), 0, False),
            (('hold', 27, ('session', 30)), range(128), range(128), 50, False),
            (('dec ', 27, ('session', 31)), range(128), range(128), 70, False),
            (('dpth', 27, ('session', 32)), range(128), range(128), 0, False),
        )),
        (('drum', 14), (
            (('1smp', 14, ('session', 38)), range(64), range(64), 0, False),
            (('2smp', 14, ('session', 45)), range(64), range(64), 16, False),
            (('3smp', 14, ('session', 52)), range(64), range(64), 32, False),
            (('4smp', 14, ('session', 59)), range(64), range(64), 36, False),
        )),
    )),
)

def set_value(circuit, idx, addr, value):
    addr_t, *addr_ptr = addr
    if addr_t == 'session':
        return circuit.set_session_value(addr_ptr, value, emulate=True)
    elif addr_t == 'user':
        return circuit.set_user_value(idx, addr_ptr, value)
    elif addr_t == 'patch':
        return circuit.set_patch_value(idx, addr_ptr, value, emulate=True)
    else:
        raise ValueError('invalid address type: %s' % addr_t)

def get_value(circuit, idx, addr, default=None):
    if circuit is None:
        return default

    addr_t, *addr_ptr = addr
    if addr_t == 'session':
        return circuit.get_session_value(addr_ptr, default=default)
    elif addr_t == 'user':
        return circuit.get_user_value(idx, addr_ptr, default=default)
    elif addr_t == 'patch':
        return circuit.get_patch_value(idx, addr_ptr, default=default)
    else:
        raise ValueError('invalid address type: %s' % addr_t)


smart_menu = (
    ((0, 0), (
        (0, 0, 0), # osc 1 wave
        (0, 0, 2), # osc 1 pwid
        (0, 0, 3), # osc 1 vsyn
        (0, 0, 4), # osc 1 dens
        (0, 0, 6), # osc 1 semi
        (0, 0, 5), # osc 1 dtun
        (0, 0, 7), # osc 1 cent
    )),
    ((0, 1), (
        (0, 1, 0), # osc 2 wave
        (0, 1, 2), # osc 2 pwid
        (0, 1, 3), # osc 2 vsyn
        (0, 1, 4), # osc 2 dens
        (0, 1, 6), # osc 2 semi
        (0, 1, 5), # osc 2 dtun
        (0, 1, 7), # osc 2 cent
    )),
    ((0, 2), (
        (0, 2, 0), # osc 1 volu
        (0, 4, 0), # voic mode
        (0, 2, 1), # osc 2 volu
        (0, 4, 1), # voic prta
        (0, 2, 4), # mix  pre
        (0, 4, 2), # voic glid
        None,
    )),

    ((0, 3), (
        (0, 3, 0), # filt freq
        (0, 3, 4), # filt type
        (0, 3, 5), # filt driv
        (0, 3, 6), # filt dtyp
        (0, 3, 1), # filt reso
        (0, 3, 3), # filt trak
        (0, 3, 8), # filt 2mod
    )),
    ((0, 4), (
        (0, 3, 2), # filt qnor
        None,
        (0, 3, 7), # filt rout
        None,
        None,
        None,
        None,
    )),

    ((1, 0), (
        (1, 0, 0), # env1 attk
        (1, 0, 4), # env1 velo
        (1, 0, 1), # env1 decy
        None,
        (1, 0, 2), # env1 sust
        None,
        (1, 0, 3), # env1 rels
    )),
    ((1, 1), (
        (1, 1, 0), # env2 attk
        (1, 1, 4), # env2 velo
        (1, 1, 1), # env2 decy
        None,
        (1, 1, 2), # env2 sust
        None,
        (1, 1, 3), # env2 rels
    )),
    ((1, 2), (
        (1, 2, 0), # env3 attk
        (1, 2, 4), # env3 dela
        (1, 2, 1), # env3 decy
        None,
        (1, 2, 2), # env3 sust
        None,
        (1, 2, 3), # env3 rels
    )),

    ((1, 3), (
        (1, 3, 0), # lfo1 rate
        (1, 3, 1), # lfo1 sync
        (1, 3, 2), # lfo1 wave
        (1, 3, 3), # lfo1 phas
        (1, 3, 4), # lfo1 slew
        (1, 3, 6), # lfo1 dsyn
        (1, 3, 5), # lfo1 dela
    )),
    ((1, 4), (
        (1, 4, 0), # lfo2 rate
        (1, 4, 1), # lfo2 sync
        (1, 4, 2), # lfo2 wave
        (1, 4, 3), # lfo2 phas
        (1, 4, 4), # lfo2 slew
        (1, 4, 6), # lfo2 dsyn
        (1, 4, 5), # lfo2 dela
    )),
    ((1, 5), (
        (1, 3, 8), # lfo1 key
        (1, 4, 8), # lfo2 key
        (1, 3, 9), # lfo1 comm
        (1, 4, 9), # lfo2 comm
        None,
        None,
        None,
    )),

    ((2, 0), (
        (1, 5, 0), # dist lvl
        (0, 2, 2), # mix  ring
        (1, 5, 1), # dist type
        (0, 2, 3), # mix  nois
        (1, 5, 2), # dist comp
        None,
        None,
    )),
    ((2, 1), (
        (1, 6, 0), # chor lvl
        (1, 6, 2), # chor rate
        (1, 6, 1), # chor type
        (1, 6, 3), # chor sync
        (1, 6, 5), # chor dpth
        (1, 6, 6), # chor dela
        (1, 6, 4), # chor fdbk
    )),

    ((2, 2), (
        (1, 7, 1), # eq   blvl
        (1, 7, 0), # eq   bfrq
        (1, 7, 3), # eq   mlvl
        (1, 7, 2), # eq   mfrq
        (1, 7, 5), # eq   tlvl
        (1, 7, 4), # eq   tfrq
        (0, 2, 5), # mix  post
    )),

    ((3, 0), (
        (2, 0, 2), # mod  dpth
        (2, 0, 6), # mod  dpth
        (2, 0, 10), # mod  dpth
        (2, 0, 14), # mod  dpth
        (2, 1, 2), # mod  dpth
        (2, 1, 6), # mod  dpth
        (2, 1, 10), # mod  dpth
    )),
    ((3, 1), (
        (2, 1, 14), # mod  dpth
        (2, 2, 2), # mod  dpth
        (2, 2, 6), # mod  dpth
        (2, 2, 10), # mod  dpth
        (2, 2, 14), # mod  dpth
        (2, 3, 2), # mod  dpth
        (2, 3, 6), # mod  dpth
    )),
    ((3, 2), (
        (2, 3, 10), # mod  dpth
        (2, 3, 14), # mod  dpth
        (2, 4, 2), # mod  dpth
        (2, 4, 6), # mod  dpth
        (2, 4, 10), # mod  dpth
        (2, 4, 14), # mod  dpth
        None,
    )),

    ((4, 0), (
        (5, 0, 0), # sess rev  type
        (5, 0, 3), # sess rev  syn1
        (5, 0, 1), # sess rev  dec
        (5, 0, 4), # sess rev  syn2
        (5, 0, 2), # sess rev  damp
        None,
        None,
        None,
    )),
    ((4, 1), (
        (5, 1, 0), # sess del  time
        (5, 1, 6), # sess del  syn1
        (5, 1, 1), # sess del  sync
        (5, 1, 7), # sess del  syn2
        (5, 1, 2), # sess del  fdbk
        None,
        (5, 1, 3), # sess del  wdth
        (5, 1, 4), # sess del  l-r
    )),
)


smart_menu_pages = {}
smart_menu_values = {}

for (idx, (item, _)) in enumerate(smart_menu):
    length = len(smart_menu)
    bounds = 128 * idx // length, 128 * (idx + 1) // length

    smart_menu_values[idx] = (bounds[0] + bounds[1]) // 2

    for i in range(*bounds):
        smart_menu_pages[i] = idx


class CircuitProgrammerSmartMenu(GenericHandler):
    def register(self, obj):
        self.handlers[obj.tag] = CircuitProgrammerSmartMenuInstance(self.kernel, active=True)
        self.handlers[obj.tag].register(obj)

    def deregister(self, obj):
        if obj.tag in self.handlers:
            del self.handlers[obj.tag]

    def request_state(self, obj):
        handler = self.handlers.get(obj.tag, None)
        if handler is not None and handler.active:
            handler.request_state(obj)

    def handle(self, obj, *packet):
        handler = self.handlers.get(obj.tag, None)
        if handler is not None and handler.active and handler.handle(obj, *packet):
            return True

        return False


class CircuitProgrammerSmartMenuInstance(GenericHandler):
    def initialize(self):
        self.circuit = None
        self.page = [0, 0]
        self.previous_page = [0, 0]
        self.active_knob = [None, None]
        self.knob_status = [[0] * 8, [0] * 8]
        self.showing_pages = False
        self.showing_value = False
        self.show_cooldown = 0.5
        self.last_page_show = time()
        self.last_value_show = time()
        t = Thread(target=self.tick, args=())
        t.daemon = True
        t.start()

    def register(self, obj):
        self.circuit = obj

    def tick(self):
        previous_knob_status = [[0] * 8, [0] * 8]

        while True:
            sleep(0.05)

            if self.circuit is not None:
                force_show = False

                if self.showing_pages and time() - self.last_page_show > self.show_cooldown:
                    self.request_state(self.circuit)
                    self.showing_pages = False
                    force_show = True
                elif self.showing_value and time() - self.last_value_show > self.show_cooldown:
                    self.request_state(self.circuit)
                    self.showing_value = False
                    self.active_knob[0] = None
                    self.active_knob[1] = None
                    force_show = True

                for idx in range(2):
                    for knob, value in enumerate(self.knob_status[idx]):
                        if self.active_knob[idx] == knob:
                            continue
                        if force_show or previous_knob_status[idx][knob] != value:
                            if value is None:
                                value = 0
                                self.knob_status[idx][knob] = 0
                            self.circuit.cc(idx, 80 + knob, value)
                            previous_knob_status[idx][knob] = value

    def show_pages(self, obj):
        for idx, page in enumerate(self.page):
            if self.showing_pages and page == self.previous_page[idx]:
                continue

            self.previous_page[idx] = page

            menu, submenu = smart_menu[page][0]

            menu_knob = 2 * (menu % 4)
            menu_value = 127 - 96 * (menu // 4)

            submenu_knob = 1 + 2 * (submenu % 3)
            submenu_value = 127 - 96 * (submenu // 3)

            for knob in range(7):
                if knob == menu_knob:
                    self.knob_status[idx][knob] = menu_value
                elif knob == submenu_knob:
                    self.knob_status[idx][knob] = submenu_value
                else:
                    self.knob_status[idx][knob] = 0

        self.showing_pages = True
        self.last_page_show = time()

    def show_value(self, obj, idx, knob, value, norm_value, param_value):
        self.showing_value = True
        self.last_value_show = time()
        self.active_knob[idx] = knob

        if knob in (0, 2, 4, 6):
            knobs = (1, 3, 5, 7)
        else:
            knobs = (0, 2, 4, 6)

        nknobs = len(knobs)
        for i in range(nknobs):
            if norm_value % nknobs == i:
                self.knob_status[idx][knobs[i]] = 63 + 64 * (param_value % (4 * nknobs) == 0)
            else:
                self.knob_status[idx][knobs[i]] = 0

    def request_state(self, obj):
        for idx, page in enumerate(self.page):
            self.knob_status[idx][7] = self.page_to_value(page)

            for knob in range(7):
                selector = smart_menu[page][1][knob]
                if selector is None:
                    self.knob_status[idx][knob] = None
                    continue
                addr = menu[selector[0]][1][selector[1]][1][selector[2]][0][2]
                bounds = menu[selector[0]][1][selector[1]][1][selector[2]][2]
                value = get_value(obj, idx, addr)
                value = 128 * (value - min(bounds)) // (1 + max(bounds) - min(bounds))
                self.knob_status[idx][knob] = value

    def value_to_page(self, value):
        return smart_menu_pages[value]

    def page_to_value(self, page):
        return smart_menu_values[page]

    def handle(self, obj, channel, msg_t, *data):
        if msg_t == CC:
            idx = obj.channels.index(channel)
            cc, value = data
            # Handle macros
            if idx < 2 and cc in range(80, 88):
                knob = cc - 80

                # Control handle.
                if knob == 7:
                    prev_page = self.page[idx]
                    self.page[idx] = self.value_to_page(value)
                    self.show_pages(self.circuit)
                    return True

                if self.showing_pages:
                    return True

                selector = smart_menu[self.page[idx]][1][knob]
                if selector is None:
                    self.knob_status[idx][knob] = None
                    return True
                addr = menu[selector[0]][1][selector[1]][1][selector[2]][0][2]
                bounds = menu[selector[0]][1][selector[1]][1][selector[2]][2]
                norm_value = value * (1 + max(bounds) - min(bounds)) // 128
                param_value = min(bounds) + norm_value

                self.show_value(obj, idx, knob, value, norm_value, param_value)

                return set_value(obj, idx, addr, param_value)

        return False
