import os
from collections import defaultdict
from time import sleep

from ..miditypes import *
from ..drivers import LaunchpadPro, Circuit
from .launchpad_pro_patch_overlay import LaunchpadProPatchOverlay
from .launchpad_pro_note_overlay import LaunchpadProNoteOverlay

from .launchpad_pro_overlay import LaunchpadProOverlay

# fmt: off

osc_waves = ('sin ', 'tri ', 'saw ', 'saw9', 'saw8', 'saw7', 'saw6', 'saw5', 'saw4', 'saw3', 'saw2', 'saw1', 'puls', 'sqr ', 'sint', 'anpu', 'asnc', 'trsw', 'dig1', 'dig2', 'dssq', 'div1', 'div2', 'div3', 'div4', 'div5', 'div6', 'ran1', 'ran2', 'ran3')
lfo_waves = ('sin ', 'tri ', 'saw ', 'sqr ', 'rand', 'time', 'pian', 'seq1', 'seq2', 'seq3', 'seq4', 'seq5', 'seq6', 'seq7', 'alt1', 'alt2', 'alt3', 'alt4', 'alt5', 'alt6', 'alt7', 'alt8', 'chro', 'cr16', 'majr', 'maj7', 'min7', 'arp1', 'arp2', 'dim ', 'decm', 'min3', 'pedl', '4ths', '4x12', '1maj', '1min', '2511')
fltr_types = ('lo12', 'lo24', 'bp6 ', 'bp12', 'hi12', 'hi24')
dist_types = ('diod', 'valv', 'clip', 'xovr', 'rect', 'bitr', 'rate')
reverb_types = ('cham', 's rm', 'l rm', 's hl', 'l hl', 'g hl')
syncs = ('off ', 't/32', '1/32', 't/16', '1/16', 't/8 ', 'd/16', '1/8 ', 't/4 ', 'd/8 ', '1/4 ', '11/3', 'd/4 ', '1/2 ', '22/3', '3bts', '4bts', '51/3', '6bts', '8bts', '1023', '12bs', '1313', '16bs', '18bs', '1823', '20bs', '2113', '24bs', '28bs', '30bs', '32bs', '36bs', '42bs', '48bs', '64bs')
off_on = ('off ', 'on  ')
fade_modes = ('fin ', 'fout', 'gin ', 'gout')
ratios = ('1:1 ', '4:3 ', '3:4 ', '3:2 ', '2:3 ', '2:1 ', '1:2 ', '3:1 ', '1:3 ', '4:1 ', '1:4 ', '1:of', 'of:1')

mod_sources = ('dir ', 'mwhl', 'aftr', 'expr', 'vel ', 'key ', 'lfo1', 'lf1-', 'lfo2', 'lf2-', 'env1', 'env2', 'env3')
mod_destinations = ('12pi', '1pit', '2pit', '1vsy', '2vsy', '1pwd', '2pwd', 'osc1', 'osc2', 'nois', 'ring', 'driv', 'freq', 'res ', 'lfo1', 'lfo2', 'dec1', 'dec2')

macro_destinations = (
    '   -', # 0
    'prta', 'post', # 2
    '1int', '1pwd', '1vsy', '1den', '1dtn', '1sem', '1cen', # 9
    '2int', '2pwd', '2vsy', '2den', '2dtn', '2sem', '2cen', # 16
    'osc1', 'osc2', 'ring', 'nois', # 20
    'freq', 'res ', 'driv', 'trak', '2mod', # 25
    '1atk', '1dec', '1sus', '1rel', # 29
    '2atk', '2dec', '2sus', '2rel', # 33
    '3del', '3atk', '3dec', '3sus', '3rel', # 38
    '1rat', '1syn', '1slw', '2rat', '2syn', '2slw', # 44
    'dist', 'chor', 'crat', 'cfdb', 'cdpt', 'cdel', # 50
    'm  1', 'm  2', 'm  3', 'm  4', 'm  5', 'm  6', 'm  7', 'm  8', # 58
    'm  9', 'm 10', 'm 11', 'm 12', 'm 13', 'm 14', 'm 15', 'm 16', # 64
    'm 17', 'm 18', 'm 19', 'm 20' # 70
    )

sidechain_sources = ('   1', '   2', '   3', '   4', 'off ')

user_destinations = (
    '   -', # 0
    'prta', # 1
    '1int', '1pwd', '1vsy', '1den', '1dtn', '1sem', '1cen', '1pit', # 9
    '2int', '2pwd', '2vsy', '2den', '2dtn', '2sem', '2cen', '2pit', # 17
    'osc1', 'osc2', 'ring', 'nois', # 21
    'freq', 'res ', 'driv', 'trak', '2mod', # 26
    '1atk', '1dec', '1sus', '1rel', # 30
    '2atk', '2dec', '2sus', '2rel', # 34
    '3del', '3atk', '3dec', '3sus', '3rel', # 39
    '1rat', '1syn', '1slw', '2rat', '2syn', '2slw', # 45
    'dist', 'chor', 'crat', 'cfdb', 'cdpt', 'cdel', # 51
    'rev1', 'rev2', 'rvdc', # 54
    'del1', 'del2', 'dltm', # 57
)

menu = (
    (('pg 1', 54), (
        (('osc1', 40), (
            (('wave', 40, ('patch', 36)), osc_waves, range(30), 2, False),
            (('intp', 40, ('patch', 37)), range(128), range(128), 0, True),
            (('pwid', 40, ('patch', 38)), range(-64, 64), range(128), 127, True),
            (('vsyn', 40, ('patch', 39)), range(128), range(128), 0, True),
            (('dens', 40, ('patch', 40)), range(128), range(128), 0, True),
            (('dtun', 40, ('patch', 41)), range(128), range(128), 0, True),
            (('semi', 40, ('patch', 42)), range(-64, 64), range(128), 64, True),
            (('cent', 40, ('patch', 43)), range(-64, 64), range(128), 64, True),
            (('bend', 40, ('patch', 44)), range(-64, 64), range(128), 64, False),
        )),
        (('osc2', 42), (
            (('wave', 42, ('patch', 45)), osc_waves, range(30), 2, False),
            (('intp', 42, ('patch', 46)), range(128), range(128), 0, True),
            (('pwid', 42, ('patch', 47)), range(-64, 64), range(128), 127, True),
            (('vsyn', 42, ('patch', 48)), range(128), range(128), 0, True),
            (('dens', 42, ('patch', 49)), range(128), range(128), 0, True),
            (('dtun', 42, ('patch', 50)), range(128), range(128), 0, True),
            (('semi', 42, ('patch', 51)), range(-64, 64), range(128), 64, True),
            (('cent', 42, ('patch', 52)), range(-64, 64), range(128), 64, True),
            (('bend', 42, ('patch', 53)), range(-64, 64), range(128), 64, False),
        )),
        (('mix ', 32), (
            (('osc1', 32, ('patch', 54)), range(128), range(128), 127, True),
            (('osc2', 32, ('patch', 55)), range(128), range(128), 0, True),
            (('ring', 32, ('patch', 56)), range(128), range(128), 0, True),
            (('nois', 32, ('patch', 57)), range(128), range(128), 0, True),
            (('pre ', 32, ('patch', 58)), range(-12, 19), range(52, 83), 64, False),
            (('post', 32, ('patch', 59)), range(-12, 19), range(52, 83), 64, True),
        )),
        (('fltr', 8), (
            (('freq', 8, ('patch', 64)), range(128), range(128), 127, True),
            (('res ', 8, ('patch', 66)), range(128), range(128), 0, True),
            (('qnor', 8, ('patch', 67)), range(128), range(128), 64, False),
            (('trak', 8, ('patch', 65)), range(128), range(128), 127, True),
            (('type', 8, ('patch', 63)), fltr_types, range(6), 1, False),
            (('driv', 8, ('patch', 61)), range(128), range(128), 0, True),
            (('dtyp', 8, ('patch', 62)), dist_types, range(7), 0, False),
            (('rout', 8, ('patch', 60)), ('norm', 'osc1', 'os12'), range(3), 0, False),
            (('2mod', 8, ('patch', 68)), range(-64, 64), range(128), 64, True),

        )),
        (('voic', 56), (
            (('mode', 56, ('patch', 32)), ('mono', 'm ag', 'poly'), range(3), 2, False),
            (('prta', 56, ('patch', 33)), range(128), range(128), 0, True),
            (('glid', 56, ('patch', 34)), range(-12, 13), range(52, 77), 64, False),
            (('oct ', 56, ('patch', 35)), range(-6, 6), range(58, 70), 64, False),
        )),
    )),
    (('pg 2', 54), (
        (('env1', 96), (
            (('attk', 96, ('patch', 70)), range(128), range(128), 2, True),
            (('decy', 96, ('patch', 71)), range(128), range(128), 90, True),
            (('sust', 96, ('patch', 72)), range(128), range(128), 127, True),
            (('rels', 96, ('patch', 73)), range(128), range(128), 40, True),
            (('velo', 96, ('patch', 69)), range(-64, 64), range(128), 64, False),
        )),
        (('env2', 96), (
            (('attk', 96, ('patch', 75)), range(128), range(128), 2, True),
            (('decy', 96, ('patch', 76)), range(128), range(128), 75, True),
            (('sust', 96, ('patch', 77)), range(128), range(128), 35, True),
            (('rels', 96, ('patch', 78)), range(128), range(128), 45, True),
            (('velo', 96, ('patch', 74)), range(-64, 64), range(128), 64, False),
        )),
        (('env3', 96), (
            (('attk', 96, ('patch', 80)), range(128), range(128), 10, True),
            (('decy', 96, ('patch', 81)), range(128), range(128), 70, True),
            (('sust', 96, ('patch', 82)), range(128), range(128), 64, True),
            (('rels', 96, ('patch', 83)), range(128), range(128), 40, True),
            (('dela', 96, ('patch', 79)), range(128), range(128), 0, True),
        )),
        (('lfo1', 12), (
            (('rate', 12, ('patch', 89)), range(128), range(128), 68, True),
            (('sync', 12, ('patch', 90)), syncs, range(36), 0, True),
            (('wave', 12, ('patch', 84)), lfo_waves, range(38), 0, False),
            (('phas', 12, ('patch', 85)), range(0, 360, 3), range(120), 0, False),
            (('slew', 12, ('patch', 86)), range(128), range(128), 0, True),
            (('dela', 12, ('patch', 87)), range(128), range(128), 0, False),
            (('dsyn', 12, ('patch', 88)), syncs, range(36), 0, False),
            (('one ', 12, ('patch', 91, 0, 1)), off_on, (12, 13), 12, False),
            (('key ', 12, ('patch', 91, 1, 1)), off_on, (14, 15), 14, False),
            (('comm', 12, ('patch', 91, 2, 1)), off_on, (16, 17), 16, False),
            (('dtrg', 12, ('patch', 91, 3, 1)), off_on, (18, 19), 18, False),
            (('fade', 12, ('patch', 91, 4, 2)), fade_modes, range(4), 0, False),
        )),
        (('lfo2', 12), (
            (('rate', 12, ('patch', 97)), range(128), range(128), 68, True),
            (('sync', 12, ('patch', 98)), syncs, range(36), 0, True),
            (('wave', 12, ('patch', 92)), lfo_waves, range(38), 0, False),
            (('phas', 12, ('patch', 93)), range(0, 360, 3), range(120), 0, False),
            (('slew', 12, ('patch', 94)), range(128), range(128), 0, True),
            (('dela', 12, ('patch', 95)), range(128), range(128), 0, False),
            (('dsyn', 12, ('patch', 96)), syncs, range(36), 0, False),
            (('one ', 12, ('patch', 99, 0, 1)), off_on, (22, 23), 22, False),
            (('key ', 12, ('patch', 99, 1, 1)), off_on, (24, 25), 24, False),
            (('comm', 12, ('patch', 99, 2, 1)), off_on, (26, 27), 26, False),
            (('dtrg', 12, ('patch', 99, 3, 1)), off_on, (28, 29), 28, False),
            (('fade', 12, ('patch', 99, 4, 2)), fade_modes, range(4, 8), 4, False),
        )),
        (('dist', 48), (
            (('lvl ', 48, ('patch', 100)), range(128), range(128), 0, True),
            (('type', 48, ('patch', 116)), dist_types, range(7), 0, False),
            (('comp', 48, ('patch', 117)), range(128), range(128), 100, False),
        )),
        (('chor', 48), (
            (('lvl ', 48, ('patch', 102)), range(128), range(128), 0, True),
            (('type', 48, ('patch', 118)), ('phas', 'chor'), (0, 1), 1, False),
            (('rate', 48, ('patch', 119)), range(128), range(128), 84, True),
            (('sync', 48, ('patch', 120)), syncs, range(36), 0, False),
            (('fdbk', 48, ('patch', 121)), range(-64, 64), range(128), 74, True),
            (('dpth', 48, ('patch', 122)), range(128), range(128), 64, True),
            (('dela', 48, ('patch', 123)), range(128), range(128), 64, True),
        )),
        (('eq  ', 48), (
            (('bfrq', 48, ('patch', 105)), range(128), range(128), 64, False),
            (('blvl', 48, ('patch', 106)), range(-64, 64), range(128), 64, False),
            (('mfrq', 48, ('patch', 107)), range(128), range(128), 64, False),
            (('mlvl', 48, ('patch', 108)), range(-64, 64), range(128), 64, False),
            (('tfrq', 48, ('patch', 109)), range(128), range(128), 125, False),
            (('tlvl', 48, ('patch', 110)), range(-64, 64), range(128), 64, False),
        )),
    )),

    (('mod ', 64), (*(
        (('%2d%2d' % (4*i+1, 4*(i+1)), 64), (
            (('src1', 64, ('patch', 124+16*i)), mod_sources, range(13), 0, False),
            (('src2', 64, ('patch', 125+16*i)), mod_sources, range(13), 0, False),
            (('dpth', 64, ('patch', 126+16*i)), range(-64, 64), range(128), 64, True),
            (('dest', 64, ('patch', 127+16*i)), mod_destinations, range(18), 0, False),
            (('src1', 66, ('patch', 128+16*i)), mod_sources, range(13), 0, False),
            (('src2', 66, ('patch', 129+16*i)), mod_sources, range(13), 0, False),
            (('dpth', 66, ('patch', 130+16*i)), range(-64, 64), range(128), 64, True),
            (('dest', 66, ('patch', 131+16*i)), mod_destinations, range(18), 0, False),
            (('src1', 74, ('patch', 132+16*i)), mod_sources, range(13), 0, False),
            (('src2', 74, ('patch', 133+16*i)), mod_sources, range(13), 0, False),
            (('dpth', 74, ('patch', 134+16*i)), range(-64, 64), range(128), 64, True),
            (('dest', 74, ('patch', 135+16*i)), mod_destinations, range(18), 0, False),
            (('src1', 77, ('patch', 136+16*i)), mod_sources, range(13), 0, False),
            (('src2', 77, ('patch', 137+16*i)), mod_sources, range(13), 0, False),
            (('dpth', 77, ('patch', 138+16*i)), range(-64, 64), range(128), 64, True),
            (('dest', 77, ('patch', 139+16*i)), mod_destinations, range(18), 0, False),
            )) for i in range(5)),
    )),

    (('mac ', 84), (*(
        (('mac%d' % (i+1), 84), (
            (('dest', 84, ('patch', 205+17*i)), macro_destinations, range(71), 0, False),
            (('bgin', 84, ('patch', 206+17*i)), range(128), range(128), 0, False),
            (('end ', 84, ('patch', 207+17*i)), range(128), range(128), 127, False),
            (('dpth', 84, ('patch', 208+17*i)), range(-64, 64), range(128), 64, False),
            (('dest', 95, ('patch', 209+17*i)), macro_destinations, range(71), 0, False),
            (('bgin', 95, ('patch', 210+17*i)), range(128), range(128), 0, False),
            (('end ', 95, ('patch', 211+17*i)), range(128), range(128), 127, False),
            (('dpth', 95, ('patch', 212+17*i)), range(-64, 64), range(128), 64, False),
            (('dest', 109, ('patch', 213+17*i)), macro_destinations, range(71), 0, False),
            (('bgin', 109, ('patch', 214+17*i)), range(128), range(128), 0, False),
            (('end ', 109, ('patch', 215+17*i)), range(128), range(128), 127, False),
            (('dpth', 109, ('patch', 216+17*i)), range(-64, 64), range(128), 64, False),
            (('dest', 94, ('patch', 217+17*i)), macro_destinations, range(71), 0, False),
            (('bgin', 94, ('patch', 218+17*i)), range(128), range(128), 0, False),
            (('end ', 94, ('patch', 219+17*i)), range(128), range(128), 127, False),
            (('dpth', 94, ('patch', 220+17*i)), range(-64, 64), range(128), 64, False),
        )) for i in range(8)),
    )),

    (('usr ', 42), (*(
        (('usr%d' % (i+1), 42), (
            (('dest', 42, ('user', 16*i)), user_destinations, range(len(user_destinations)), 0, False),
            (('bgin', 42, ('user', 16*i+2)), range(128), range(128), 0, False),
            (('end ', 42, ('user', 16*i+3)), range(128), range(128), 127, False),
            (('dpth', 42, ('user', 16*i+1)), range(-128, 128), range(-128, 128), 0, False, False),
            (('dest', 44, ('user', 16*i+4)), user_destinations, range(len(user_destinations)), 0, False),
            (('bgin', 44, ('user', 16*i+6)), range(128), range(128), 0, False),
            (('end ', 44, ('user', 16*i+7)), range(128), range(128), 127, False),
            (('dpth', 44, ('user', 16*i+5)), range(-128, 128), range(-128, 128), 0, False, False),
            (('dest', 50, ('user', 16*i+8)), user_destinations, range(len(user_destinations)), 0, False),
            (('bgin', 50, ('user', 16*i+10)), range(128), range(128), 0, False),
            (('end ', 50, ('user', 16*i+11)), range(128), range(128), 127, False),
            (('dpth', 50, ('user', 16*i+9)), range(-128, 128), range(-128, 128), 0, False, False),
            (('dest', 52, ('user', 16*i+12)), user_destinations, range(len(user_destinations)), 0, False),
            (('bgin', 52, ('user', 16*i+14)), range(128), range(128), 0, False),
            (('end ', 52, ('user', 16*i+15)), range(128), range(128), 127, False),
            (('dpth', 52, ('user', 16*i+13)), range(-128, 128), range(-128, 128), 0, False),
        )) for i in range(8)),
    )),

    (('sess', 57), (
        (('rev ', 57), (
            (('type', 57, ('session', 6)), reverb_types, range(6), 2, False),
            (('dec ', 57, ('session', 7)), range(128), range(128), 64, False),
            (('damp', 57, ('session', 8)), range(128), range(128), 64, False),
            (('syn1', 53, ('session', 0)), range(128), range(128), 0, False),
            (('syn2', 25, ('session', 1)), range(128), range(128), 0, False),
            (('drm1', 14, ('session', 2)), range(128), range(128), 0, False),
            (('drm2', 14, ('session', 3)), range(128), range(128), 0, False),
            (('drm3', 14, ('session', 4)), range(128), range(128), 0, False),
            (('drm4', 14, ('session', 5)), range(128), range(128), 0, False),
        )),
        (('del ', 96), (
            (('time', 96, ('session', 15)), range(128), range(128), 64, False),
            (('sync', 96, ('session', 16)), syncs, range(36), 20, False),
            (('fdbk', 96, ('session', 17)), range(128), range(128), 64, False),
            (('wdth', 96, ('session', 18)), range(128), range(128), 127, False),
            (('l-r ', 96, ('session', 19)), ratios, range(13), 4, False),
            (('slew', 96, ('session', 20)), range(128), range(128), 5, False),
            (('syn1', 53, ('session', 9)), range(128), range(128), 0, False),
            (('syn2', 25, ('session', 10)), range(128), range(128), 0, False),
            (('drm1', 14, ('session', 11)), range(128), range(128), 0, False),
            (('drm2', 14, ('session', 12)), range(128), range(128), 0, False),
            (('drm3', 14, ('session', 13)), range(128), range(128), 0, False),
            (('drm4', 14, ('session', 14)), range(128), range(128), 0, False),
        )),
        (('fltr', 8), (
            (('freq', 8, ('session', 21)), range(128), range(128), 64, False),
            (('res ', 8, ('session', 22)), range(128), range(128), 30, False),
        )),
        (('s-c1', 55), (
            (('src ', 55, ('session', 23)), sidechain_sources, range(5), 0, False),
            (('atk ', 55, ('session', 24)), range(128), range(128), 0, False),
            (('hold', 55, ('session', 25)), range(128), range(128), 50, False),
            (('dec ', 55, ('session', 26)), range(128), range(128), 70, False),
            (('dpth', 55, ('session', 27)), range(128), range(128), 0, False),
        )),
        (('s-c2', 27), (
            (('src ', 27, ('session', 28)), sidechain_sources, range(5), 0, False),
            (('atk ', 27, ('session', 29)), range(128), range(128), 0, False),
            (('hold', 27, ('session', 30)), range(128), range(128), 50, False),
            (('dec ', 27, ('session', 31)), range(128), range(128), 70, False),
            (('dpth', 27, ('session', 32)), range(128), range(128), 0, False),
        )),
        (('drum', 14), (
            (('1smp', 14, ('session', 38)), range(64), range(64), 0, False),
            (('2smp', 14, ('session', 45)), range(64), range(64), 16, False),
            (('3smp', 14, ('session', 52)), range(64), range(64), 32, False),
            (('4smp', 14, ('session', 59)), range(64), range(64), 36, False),
        )),
    )),
)


# fmt: on


def set_value(circuit, idx, addr, value, *, temporary=False):
    addr_t, *addr_ptr = addr
    if addr_t == "session":
        return circuit.set_session_value(
            addr_ptr, value, emulate=True, temporary=temporary
        )
    elif addr_t == "user":
        return circuit.set_user_value(idx, addr_ptr, value)
    elif addr_t == "patch":
        return circuit.set_patch_value(
            idx, addr_ptr, value, emulate=True, temporary=temporary
        )
    else:
        raise ValueError("invalid address type: %s" % addr_t)


def get_value(circuit: Circuit, idx: int, addr: tuple, default=None):
    if circuit is None:
        return default

    addr_t, *addr_ptr = addr
    if addr_t == "session":
        return circuit.get_session_value(addr_ptr, default=default)
    elif addr_t == "user":
        return circuit.get_user_value(idx, addr_ptr, default=default)
    elif addr_t == "patch":
        return circuit.get_patch_value(idx, addr_ptr, default=default)
    else:
        raise ValueError("invalid address type: %s" % addr_t)


class LaunchpadProCircuitProgrammer(LaunchpadProOverlay):
    active_color = 119
    synth_colors = (53, 25)

    channel = 0

    velocity_levels = (1, 1, 1, 1, 1, 1, 1, 1)

    # menu:        ('title', color) (submenus...)
    # submenu:     ('title', color) (subsubmenus...)
    # subsubmenu:  ('title', color, addr, controller) (values) (range) (default)

    button_mode_map = {
        "circuit select": "device",
    }

    def initialize(self):
        self.store_mode = False
        self.confirm_patch_store_idx = None

        self.handlers = {"patch": LaunchpadProPatchOverlay(self.kernel)}

        self.base_state = {(x, y): 0 for x in range(8) for y in range(8)}

        self.selector = (0, 0, 0)
        self.selector_cache = {}

        self.submenu = menu[self.selector[0]][1]
        self.subsubmenu = self.submenu[self.selector[1]][1]
        self.addr = self.subsubmenu[self.selector[2]][0][2]

        self.circuit = None
        self.launchpad = None
        self.set_synth(0)

    def enable(self, launchpad):
        self.active = True
        self.launchpad = launchpad

        self.launchpad.push_display(self.base_state)

        self.launchpad.set_base_state("user", self.launchpad.active_button_color)
        self.launchpad.refresh_button("user")
        self.show_menu()
        self.show_status()

    def disable(self):
        self.launchpad.pop_display()

        self.active = False

    def patch_dump(self, circuit, idx):
        circuit.midi_out.send_message((240, 0, 32, 41, 1, 96, 64, idx, 247))

    def patch_dump_response(self, data):
        self.circuit.synth_data[str(data[6])] = data[8:-1]

    def patch_store(self, idx):
        if self.circuit is None:
            return

        data = self.circuit.synth_data[str(self.synth_selector)]
        self.circuit.midi_out.send_message(
            (240, 0, 32, 41, 1, 96, 1, idx, 0, *data, 247)
        )

    def set_circuit(self, obj):
        self.circuit = obj
        print("[LAUNCHPAD PRO CIRCUIT PROGRAMMER]: Selected: %s" % obj.tag)

    def set_synth(self, idx):
        if idx < 0:
            self.synth_selector = -1
        elif idx < 2:
            self.synth_selector = idx
            self.show_status()
        else:
            pass
            # raise ValueError('invalid synth selector index: %s' % idx)

    def prev_synth(self):
        self.set_synth(max(0, self.synth_selector - 1))

    def next_synth(self):
        self.set_synth(min(1, self.synth_selector + 1))

    def set_mode(self, mode):
        color = self.launchpad.active_button_color

        if mode in self.button_mode_map:
            self.launchpad.set_button(self.button_mode_map[mode], color)
            return True

        return False

    def unset_mode(self, mode):
        if mode in self.button_mode_map:
            self.launchpad.unset_button(self.button_mode_map[mode])
            return True

        return False

    def handle(self, obj, *packet):
        if isinstance(obj, LaunchpadPro):
            return self.launchpad_handle(obj, *packet)
        elif isinstance(obj, Circuit):
            return self.circuit_handle(obj, *packet)

    def circuit_handle(self, obj, channel, msg_t, *data):
        if msg_t == SYSEX:
            self.patch_dump_response(data)
            return True

        if msg_t == NOTE_ON and "circuit select" in self.kernel.modes:
            self.set_circuit(obj)
            idx = obj.channels.index(channel)
            self.set_synth(idx)
            return True

        if msg_t == PROGRAM_CHANGE:
            self.patch_dump(obj, obj.channels.index(channel))
            return True

        return False

    def launchpad_handle(self, obj, *packet):
        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler.handle(obj, *packet):
                return True

        if self.launchpad.is_button_on(packet, "user"):
            self.disable()
            return True

        if self.launchpad.is_button_on(packet, "device"):
            self.kernel.push_mode("circuit select")
        if self.launchpad.is_button_off(packet, "device"):
            self.kernel.pop_mode("circuit select")

        if self.circuit is None:
            return True

        if "shift" not in self.kernel.modes and self.launchpad.is_button_on(
            packet, "down"
        ):
            self.handlers["patch"].enable(self, save=False)
            return True
        if "shift" not in self.kernel.modes and self.launchpad.is_button_on(
            packet, "up"
        ):
            self.handlers["patch"].enable(self, save=True)
            return True

        if "shift" in self.kernel.modes and self.launchpad.is_button_on(packet, "up"):
            if not self.store_mode:
                self.launchpad.flush_display(52 - self.synth_selector * 24)
                self.launchpad.set_button("up", self.launchpad.active_button_color)
                self.store_mode = True
            else:
                self.confirm_patch_store_idx = None
                self.store_mode = False
                self.launchpad.unset_button("up")
                self.launchpad.flush_display()
                self.show_menu()
                self.show_status()
            return True

        if self.launchpad.is_button_on(packet, "left"):
            self.prev_synth()
        if self.launchpad.is_button_on(packet, "right"):
            self.next_synth()

        channel, msg_t, *data = packet

        if msg_t == USER:
            x, y, vel = data

            if self.store_mode:
                if vel == 0:
                    return True

                idx = (7 - y) * 8 + x

                if idx == self.confirm_patch_store_idx:
                    self.confirm_patch_store_idx = None
                    self.patch_store(idx)
                    self.store_mode = False
                    self.launchpad.unset_button("up")
                    self.launchpad.flush_display()
                    self.show_menu()
                    self.show_status()
                else:
                    self.launchpad.flush_display(52 - self.synth_selector * 24)
                    self.confirm_patch_store_idx = idx
                    self.launchpad.set_pad(x, y, self.active_color)
                return True

            if vel == 0:
                if 0 <= y < 4:
                    self.show_status()
                return True

            velocity_level = vel // 16
            update_flag = False

            if y == 0 and x < len(menu):
                key = (x,)
                if key not in self.selector_cache:
                    self.selector_cache[key] = (*key, 0, 0)
                self.selector = self.selector_cache[key]
                update_flag = True
            elif y == 1 and x < len(self.submenu):
                key = (self.selector[0], x)
                if key not in self.selector_cache:
                    self.selector_cache[key] = (*key, 0)
                self.selector_cache[key[0:1]] = (*key, 0)
                self.selector = self.selector_cache[key]
                update_flag = True
            elif 2 <= y < 4 and (x + 8 * (y - 2)) < len(self.subsubmenu):
                key = (self.selector[0], self.selector[1], x + 8 * (y - 2))
                self.selector_cache[key[0:1]] = key
                self.selector_cache[key[0:2]] = key
                self.selector = key
                update_flag = True
            elif y >= 4:
                bounds = self.subsubmenu[self.selector[2]][2]
                default = self.subsubmenu[self.selector[2]][3]
                delta = (
                    (1 + 24 * (y == 7))
                    * (2 * (x > 3) - 1)
                    * self.velocity_levels[velocity_level]
                )

                value = (
                    get_value(
                        self.circuit, self.synth_selector, self.addr, default=default
                    )
                    + delta
                )
                if value is None:
                    return True

                value = max(min(value, max(bounds)), min(bounds))
                set_value(self.circuit, self.synth_selector, self.addr, value)
                self.show_status()

            if update_flag:
                self.submenu = menu[self.selector[0]][1]
                self.subsubmenu = self.submenu[self.selector[1]][1]
                self.addr = self.subsubmenu[self.selector[2]][0][2]
                self.show_title(min(y, 2))
                self.show_menu()

        return True

    def show_menu(self):
        for idx in range(8):
            if idx == self.selector[0]:
                color = self.active_color
            elif idx < len(menu):
                color = menu[idx][0][1]
            else:
                color = 0
            self.launchpad.set_pad(idx, 0, color)

        for idx in range(8):
            if idx == self.selector[1]:
                color = self.active_color
            elif idx < len(self.submenu):
                color = self.submenu[idx][0][1]
            else:
                color = 0
            self.launchpad.set_pad(idx, 1, color)

        for idx in range(16):
            if idx == self.selector[2]:
                color = self.active_color
                # Can assign as macro
                if self.subsubmenu[self.selector[2]][4]:
                    self.launchpad.set_base_state("shift", 11)
                else:
                    self.launchpad.unset_base_state("shift")
                self.launchpad.refresh_button("shift")

            elif idx < len(self.subsubmenu):
                color = self.subsubmenu[idx][0][1]
            else:
                color = 0
            x, y = idx % 8, 2 + idx // 8
            self.launchpad.set_pad(x, y, color)

    def show_title(self, level=0):
        if level == 0:
            self.show_string(menu[self.selector[0]][0][0])
        elif level == 1:
            submenu = menu[self.selector[0]][1]
            self.show_string(submenu[self.selector[1]][0][0])
        elif level == 2:
            submenu = menu[self.selector[0]][1]
            subsubmenu = submenu[self.selector[1]][1]
            self.show_string(subsubmenu[self.selector[2]][0][0])

    def show_status(self):
        if self.launchpad is None:
            return

        values = self.subsubmenu[self.selector[2]][1]
        bounds = self.subsubmenu[self.selector[2]][2]
        default = self.subsubmenu[self.selector[2]][3]
        if self.circuit is None:
            self.show_string("----")
            return

        synth_color = self.synth_colors[self.synth_selector]
        self.launchpad.set_button("left", synth_color)
        self.launchpad.set_button("right", synth_color)

        patch_data = self.circuit.synth_data[str(self.synth_selector)]
        if len(patch_data) == 0:
            self.show_string("----")
        else:
            val = get_value(
                self.circuit, self.synth_selector, self.addr, default=default
            )
            if val is None:
                return
            self.show_string(str(values[val - min(bounds)]).rjust(4))


# fmt: off


smart_menu_macro_dests = (
    (3, 0, 0),
    (3, 0, 4),
    (3, 0, 8),
    (3, 0, 12),
    (3, 1, 0),
    (3, 1, 4),
    (3, 1, 8),
    (3, 1, 12),
    (3, 2, 0),
    (3, 2, 4),
    (3, 2, 8),
    (3, 2, 12),
    (3, 3, 0),
    (3, 3, 4),
    (3, 3, 8),
    (3, 3, 12),
    (3, 4, 0),
    (3, 4, 4),
    (3, 4, 8),
    (3, 4, 12),
    (3, 5, 0),
    (3, 5, 4),
    (3, 5, 8),
    (3, 5, 12),
    (3, 6, 0),
    (3, 6, 4),
    (3, 6, 8),
    (3, 6, 12),
    (3, 7, 0),
    (3, 7, 4),
    (3, 7, 8),
    (3, 7, 12),
)

smart_menu = (
    (None, 0, (None, None, None, None, None, None, None, None)),
    ("1", 40, (
        (0, 0, 0), # osc 1 wave
        (0, 0, 1), # osc 1 intp
        (0, 0, 2), # osc 1 pwid
        (0, 0, 3), # osc 1 vsyn
        (0, 0, 4), # osc 1 dens
        (0, 0, 5), # osc 1 dtun
        (0, 0, 6), # osc 1 semi
        (0, 0, 7), # osc 1 cent
    )),
    ("1", 43, (
        (0, 1, 0), # osc 2 wave
        (0, 1, 1), # osc 2 intp
        (0, 1, 2), # osc 2 pwid
        (0, 1, 3), # osc 2 vsyn
        (0, 1, 4), # osc 2 dens
        (0, 1, 5), # osc 2 dtun
        (0, 1, 6), # osc 2 semi
        (0, 1, 7), # osc 2 cent
    )),
    ("1", 32, (
        (0, 2, 0), # mix  osc1
        None,
        (0, 2, 1), # mix  osc2
        None,
        (0, 2, 2), # mix  ring
        None,
        (0, 2, 3), # mix  nois
        None,
    )),

    ("2", 8, (
        (0, 3, 0), # fltr freq
        (0, 3, 1), # fltr reso
        (0, 3, 2), # fltr qnor
        (0, 3, 3), # fltr trak
        (0, 3, 4), # fltr type
        (0, 3, 5), # fltr driv
        (0, 3, 6), # fltr dtyp
        (0, 3, 8), # fltr 2mod
    )),

    ("3", 96, (
        (1, 0, 0), # env1 attk
        (1, 1, 0), # env2 attk
        (1, 0, 1), # env1 decy
        (1, 1, 1), # env2 decy
        (1, 0, 2), # env1 sust
        (1, 1, 2), # env2 sust
        (1, 0, 3), # env1 rels
        (1, 1, 3), # env2 rels
    )),
    ("3", 97, (
        (1, 2, 0), # env3 attk
        (1, 0, 4), # env1 velo
        (1, 2, 1), # env3 decy
        (1, 1, 4), # env2 velo
        (1, 2, 2), # env3 sust
        (1, 2, 4), # env3 dela
        (1, 2, 3), # env3 rels
        None
    )),

    ("4", 12, (
        (1, 3, 0), # lfo1 rate
        (1, 3, 1), # lfo1 sync
        (1, 3, 2), # lfo1 wave
        (1, 3, 3), # lfo1 phas
        (1, 3, 4), # lfo1 slew
        (1, 3, 6), # lfo1 dsyn
        (1, 3, 8), # lfo1 key
        (1, 3, 9), # lfo1 comm
    )),
    ("4", 15, (
        (1, 4, 0), # lfo2 rate
        (1, 4, 1), # lfo2 sync
        (1, 4, 2), # lfo2 wave
        (1, 4, 3), # lfo2 phas
        (1, 4, 4), # lfo2 slew
        (1, 4, 6), # lfo2 dsyn
        (1, 4, 8), # lfo2 key
        (1, 4, 9), # lfo2 comm
    )),

    ("5", 48, (
        (1, 5, 0), # dist lvl
        None,
        (1, 5, 1), # dist type
        None,
        (1, 5, 2), # dist comp
        None,
        None,
        None,
    )),
    ("5", 51, (
        (1, 6, 0), # chor lvl
        (1, 6, 1), # chor type
        (1, 6, 2), # chor rate
        (1, 6, 3), # chor sync
        (1, 6, 4), # chor fdbk
        (1, 6, 5), # chor dpth
        (1, 6, 6), # chor dela
        None,
    )),

    ("6", 32, (
        (1, 7, 0), # eq   bfrq
        (1, 7, 1), # eq   blvl
        (1, 7, 2), # eq   mfrq
        (1, 7, 3), # eq   mlvl
        (1, 7, 4), # eq   tfrq
        (1, 7, 5), # eq   tlvl
        (0, 2, 4), # mix  pre
        (0, 2, 5), # mix  post
    )),
    ("6", 56, (
        (0, 4, 0), # voic mode
        None,
        (0, 4, 1), # voic prta
        None,
        (0, 4, 2), # voic glid
        None,
        None,
        None,
    )),

    ("7", 64, (
        (2, 0, 2), # mod  dpth
        (2, 0, 6), # mod  dpth
        (2, 0, 10), # mod  dpth
        (2, 0, 14), # mod  dpth
        (2, 1, 2), # mod  dpth
        (2, 1, 6), # mod  dpth
        (2, 1, 10), # mod  dpth
        (2, 1, 14), # mod  dpth
    )),
    ("7", 65, (
        (2, 2, 2), # mod  dpth
        (2, 2, 6), # mod  dpth
        (2, 2, 10), # mod  dpth
        (2, 2, 14), # mod  dpth
        (2, 3, 2), # mod  dpth
        (2, 3, 6), # mod  dpth
        (2, 3, 10), # mod  dpth
        (2, 3, 14), # mod  dpth
    )),
    ("7", 66, (
        (2, 4, 2), # mod  dpth
        None,
        (2, 4, 6), # mod  dpth
        None,
        (2, 4, 10), # mod  dpth
        None,
        (2, 4, 14), # mod  dpth
        None,
    )),

    ("8", 57, (
        (5, 0, 0), # sess rev  type
        (5, 0, 3), # sess rev  syn1
        (5, 0, 1), # sess rev  dec
        (5, 0, 4), # sess rev  syn2
        (5, 0, 2), # sess rev  damp
        None,
        None,
        None,
    )),
    ("8", 96, (
        (5, 1, 0), # sess del  time
        (5, 1, 6), # sess del  syn1
        (5, 1, 1), # sess del  sync
        (5, 1, 7), # sess del  syn2
        (5, 1, 2), # sess del  fdbk
        None,
        (5, 1, 3), # sess del  wdth
        (5, 1, 4), # sess del  l-r
    )),
    ("8", 97, (
        (5, 3, 1), # sess s-c1 atk
        (5, 4, 1), # sess s-c2 atk
        (5, 3, 2), # sess s-c1 hold
        (5, 4, 2), # sess s-c2 hold
        (5, 3, 3), # sess s-c1 dec
        (5, 4, 3), # sess s-c2 dec
        (5, 3, 4), # sess s-c1 dpth
        (5, 4, 4), # sess s-c2 dpth
    )),
)


# fmt: on


smart_menu_pages = {}
for (idx, (button, _, _)) in enumerate(smart_menu):
    if button not in smart_menu_pages:
        smart_menu_pages[button] = []
    smart_menu_pages[button].append(idx)


class LaunchpadProCircuitProgrammerSmartMenu(LaunchpadProOverlay):
    launchpad_buttons = ("1", "2", "3", "4", "5", "6", "7", "8")

    def initialize(self):
        self.page = 0
        self.circuits = set()
        self.launchpad = None
        self.restored = {}
        self.macro_state = set()

        self.kernel.register_callback("request state", self.request_state)

    ##################################################
    ## Request state
    ##################################################

    def request_state(self, obj):
        if isinstance(obj, Circuit):
            return self.circuit_request_state(obj)
        elif isinstance(obj, LaunchpadPro):
            return self.launchpad_request_state(obj)

    def circuit_request_state(self, obj: Circuit):
        self.circuits.add(obj)

        if self.page == 0:
            if obj in self.macro_state:
                self.circuit_restore_macro_state(obj)
            return

        if not obj in self.macro_state:
            self.circuit_save_macro_state(obj)

        for idx in range(2):
            for knob in range(8):
                selector = smart_menu[self.page][2][knob]
                if selector is None:
                    obj.cc(idx, 80 + knob, 0)
                    continue
                addr = menu[selector[0]][1][selector[1]][1][selector[2]][0][2]
                bounds = menu[selector[0]][1][selector[1]][1][selector[2]][2]
                value = get_value(obj, idx, addr, default=0)
                value = 127 * (value - min(bounds)) // (max(bounds) - min(bounds))
                obj.cc(idx, 80 + knob, value)

    def circuit_save_macro_state(self, obj: Circuit):
        self.macro_state.add(obj)
        for idx in range(2):
            for dest in range(32):
                selector = smart_menu_macro_dests[dest]
                addr = menu[selector[0]][1][selector[1]][1][selector[2]][0][2]
                set_value(obj, idx, addr, 0, temporary=True)

    def circuit_restore_macro_state(self, obj: Circuit):
        # Restore destination.
        for idx in range(2):
            for dest in range(32):
                selector = smart_menu_macro_dests[dest]
                addr = menu[selector[0]][1][selector[1]][1][selector[2]][0][2]
                set_value(obj, idx, addr, get_value(obj, idx, addr, default=0), temporary=True)

        # Restore depth.
        for idx in range(2):
            for knob in range(8):
                addr = ("patch", 204 + 17 * knob)
                obj.cc(idx, 80 + knob, get_value(obj, idx, addr, default=0))

        self.macro_state.remove(obj)

    def launchpad_request_state(self, obj: LaunchpadPro):
        self.launchpad = obj

        for button in self.launchpad_buttons:
            obj.unset_button(button)

        button = self.page_to_button(self.page)
        if button is not None:
            obj.set_button(button, self.page_to_color(self.page))

        for circuit in self.circuits:
            self.circuit_request_state(circuit)

    ##################################################
    ## Conversions
    ##################################################

    def page_to_button(self, page: int):
        return smart_menu[self.page][0]

    def button_to_page(self, button: str):
        pages = smart_menu_pages[button]
        if self.page in pages:
            idx = pages.index(self.page) + 1
            idx = idx % len(pages)
        else:
            idx = 0
        return smart_menu_pages[button][idx]

    def page_to_color(self, page: int):
        return smart_menu[self.page][1]

    ##################################################
    ## Handle
    ##################################################

    def handle(self, obj, *packet) -> bool:
        if isinstance(obj, LaunchpadPro):
            return self.launchpad_handle(obj, *packet)
        elif isinstance(obj, Circuit):
            return self.circuit_handle(obj, *packet)
        return False

    def circuit_handle(self, obj: Circuit, channel: int, msg_t: int, *data) -> bool:
        if self.page != 0 and msg_t == CC:
            idx = obj.channels.index(channel)
            cc, value = data
            # Handle macros
            if idx < 2 and cc in range(80, 88):
                knob = cc - 80

                selector = smart_menu[self.page][2][knob]
                if selector is None:
                    obj.cc(idx, cc, 0)
                    return True
                addr = menu[selector[0]][1][selector[1]][1][selector[2]][0][2]
                bounds = menu[selector[0]][1][selector[1]][1][selector[2]][2]
                value = min(bounds) + value * (max(bounds) - min(bounds)) // 127
                return set_value(obj, idx, addr, value)

        return False

    def launchpad_handle(self, obj: LaunchpadPro, *packet) -> bool:
        for button in self.launchpad_buttons:
            if self.launchpad.is_button_on(packet, button):
                if "shift" in self.kernel.modes:
                    self.page = 0
                else:
                    self.page = self.button_to_page(button)
                for circuit in self.circuits:
                    self.request_state(circuit)
                self.request_state(self.launchpad)
                return True

        return False
