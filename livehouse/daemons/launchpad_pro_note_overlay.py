from ..miditypes import *
from ..drivers import LaunchpadPro

from .launchpad_pro_overlay import LaunchpadProOverlay

from random import randint

class LaunchpadProNoteOverlay(LaunchpadProOverlay):
    def initialize(self):
        self.target_driver = None
        self.target_device = None
        self.base_state = {}
        self.notes = {}

    def enable(self, launchpad, save=False):
        self.active = True

        self.launchpad = launchpad
        self.launchpad.push_display(self.base_state)
        self.launchpad.set_button('note', self.launchpad.active_button_color)

        for name in ["1", "2", "3", "4", "5", "6", "7", "8"]:
            self.launchpad.set_base_state(name, -1, force=True)

        if len(self.notes) == 0:
            self.generate_default_notes()

        self.generate_color_scheme(launchpad.base_color)
        self.generate_layout()
        self.launchpad.refresh_display()

    def disable(self):
        self.launchpad.pop_display()

        self.active = False

    def handle(self, obj, *packet):
        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler.handle(obj, *packet):
                return True

        if isinstance(obj, LaunchpadPro):
            return self.launchpad_handle(obj, *packet)
        else:
            return self.target_handle(obj, *packet)

    def launchpad_handle(self, obj, *packet):
        if self.launchpad.is_button_on(packet, 'note'):
            self.disable()
            return True

        if self.launchpad.coloring_handle(packet):
            self.generate_color_scheme(self.launchpad.base_color)
            self.generate_layout()
            self.launchpad.refresh_display()
            return True

        if self.launchpad.is_button_on(packet, 'quantise'):
            if 'shift' in self.kernel.modes:
                self.generate_default_notes()
            else:
                self.randomize_notes()

            self.generate_layout()
            self.launchpad.refresh_display()
            return True

        if self.launchpad.is_button_on(packet, 'device'):
            self.kernel.push_mode('device select')
            self.launchpad.set_button('device', self.launchpad.active_button_color)
        if self.launchpad.is_button_off(packet, 'device'):
            self.kernel.pop_mode('device select')
            self.launchpad.unset_button('device')

        if self.launchpad.is_button_on(packet, 'up'):
            self.launchpad.move(0, -1)
            return True
        if self.launchpad.is_button_on(packet, 'down'):
            self.launchpad.move(0, +1)
            return True
        if self.launchpad.is_button_on(packet, 'left'):
            self.launchpad.move(+1, 0)
            return True
        if self.launchpad.is_button_on(packet, 'right'):
            self.launchpad.move(-1, 0)
            return True

        if self.target_device is None or self.target_driver is None:
            return True

        channel, msg_t, *data = packet

        if msg_t == USER:
            x, y, vel = data
            note = self.xy_to_note(x, y)
            if vel > 0:
                self.kernel.note_on(*self.target_device, note, vel)
                target_packet = (self.target_device[1], NOTE_ON, note, vel)
                self.target_driver.handle(*target_packet)
                color = (int(2 + vel // 2.05),) * 3
                self.launchpad.set_pad(x, y, color)
            else:
                self.kernel.note_off(*self.target_device, note, vel)
                target_packet = (self.target_device[1], NOTE_OFF, note, vel)
                self.target_driver.handle(*target_packet)
                self.launchpad.unset_pad(x, y)

        return True

    def target_handle(self, obj, channel, msg_t, *data):
        if 'device select' in self.kernel.modes:
            self.target_driver = obj
            self.target_device = (obj.tag, channel)
            return True
        return False

    def generate_default_notes(self):
        for x in range(-128, 128):
            for y in range(-128, 128):
                note = 36 + x + 5 * y
                if 0 <= note < 128:
                    self.notes[x, y] = note

    def randomize_notes(self):
        for (x, y), note in self.notes.items():
            x_ = x + randint(0, 1)
            if (x_, y) in self.notes:
                self.notes[x, y], self.notes[x_, y] = self.notes[x_, y], self.notes[x, y]

    def generate_color_scheme(self, color):
        idx = self.launchpad.color_table.index(color)
        self.colors = [
            0,
            self.launchpad.color_table[0 + (idx//4)*4],
            self.launchpad.color_table[1 + (idx//4)*4],
            self.launchpad.color_table[2 + (idx//4)*4],
            self.launchpad.color_table[3 + (idx//4)*4],
        ]
        self.octave_color_pairs = [
            (1, 3), # 00 - 11
            (1, 3), # 12 - 23
            (1, 3), # 24 - 35
            (1, 4), # 36 - 47
            (1, 4), # 48 - 59
            (1, 4), # 60 - 71
            (2, 4), # 72 - 83
            (2, 4), # 84 - 95
            (2, 4), # 96 - 107
            (3, 4), # 108 - 119
            (3, 4), # 120 - 127
        ]

    def xy_to_note(self, x, y):
        return self.notes[x, y]

    def note_to_color(self, note):
        octave = note // 12
        note_12 = note % 12
        if (note_12 == 0 or note_12 == 2 or note_12 == 4 or note_12 == 5 or
            note_12 == 7 or note_12 == 9 or note_12 == 11):
            return self.colors[self.octave_color_pairs[octave][1]]
        else:
            return self.colors[self.octave_color_pairs[octave][0]]

    def generate_layout(self):
        for key, note in self.notes.items():
            self.launchpad.set_base_state(key, self.note_to_color(note), force=True)
