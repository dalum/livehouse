from ..services import GenericHandler

class LaunchpadProOverlay(GenericHandler):
    letters = {
        'undef': '00110000',
        ' ': '00000000',
        '-': '00003300',
        '/': '03022030',
        '\\': '30200203',
        ':': '00300030',
        '1': '03030303',
        '2': '33022033',
        '3': '33131333',
        '4': '03233301',
        '5': '33200233',
        '6': '30313333',
        '7': '33030303',
        '8': '33331133',
        '9': '33331303',
        '0': '11000011',
        'a': '00013323',
        'b': '30303332',
        'c': '00333033',
        'd': '03033323',
        'e': '33303233',
        'f': '33303330',
        'g': '23330333',
        'h': '30303333',
        'i': '30003030',
        'j': '03030333',
        'k': '30313232',
        'l': '30303033',
        'm': '00333333',
        'n': '00103333',
        'o': '00003333',
        'p': '00333330',
        'q': '00333303',
        'r': '00003330',
        's': '33300333',
        't': '30333030',
        'u': '00112223',
        'v': '00113232',
        'w': '11223333',
        'x': '00330033',
        'y': '33330330',
        'z': '33033033',
    }

    palettes = {
        'white': (0, 1, 2, 3),
        'brown': (0, 11, 9, 8),
        'yellow': (0, 15, 13, 12),
        'green': (0, 19, 17, 16),
        'blue': (0, 39, 37, 36),
        'purple': (0, 51, 49, 48),
        'red': (0, 59, 57, 56),
    }

    def request_state(self, obj):
        pass

    def _set_mode(self, mode):
        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler._set_mode(mode):
                return True
        return self.set_mode(mode)

    def _unset_mode(self, mode):
        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler._unset_mode(mode):
                return True
        return self.unset_mode(mode)

    def set_mode(self, mode):
        return False

    def unset_mode(self, mode):
        return False

    def _set_status(self, status, value):
        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler._set_status(status, value):
                return True
        return self.set_status(status, value)

    def _clear_status(self, status):
        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler._clear_status(status):
                return True
        return self.clear_status(status)

    def set_status(self, status, value):
        return False

    def clear_status(self, status):
        return False

    def show_string(self, string, y=4, palette=('white', 'purple')):
        for (idx, letter) in enumerate(string):
            self.show_letter(letter, 2 * idx, y, palette[idx % 2])

    def show_letter(self, letter, x0, y0, palette='white'):
        for (idx, n) in enumerate(self.letters.get(letter, self.letters['undef'])):
            x, y = x0 + (idx % 2), y0 + 3 - idx // 2
            self.launchpad.set_pad(x, y, self.palettes[palette][int(n)])
