from ..miditypes import *
from ..drivers import LaunchpadPro

from .launchpad_pro_overlay import LaunchpadProOverlay

class LaunchpadProPatchOverlay(LaunchpadProOverlay):
    default_patch_color = 53

    def enable(self, parent, save=False):
        self.active = True
        self.confirm_idx = None

        self.parent = parent
        self.circuit = parent.circuit
        self.launchpad = parent.launchpad
        self.idx = parent.synth_selector
        self.save = save

        self.base_state = {
            self.launchpad.idx_to_xy(idx):
            patch.get("metadata", {}).get("launchpad color", self.default_patch_color)
            for (idx, patch) in self.circuit.bank.patches.items()
        }
        self.launchpad.push_display(self.base_state)

        if self.save:
            self.launchpad.set_button("up", self.launchpad.active_button_color)
        else:
            self.launchpad.set_button("down", self.launchpad.active_button_color)

    def disable(self):
        self.launchpad.pop_display()

        self.active = False

    def handle(self, obj, *packet):
        if self.launchpad.coloring_handle(*packet):
            return True

        if self.launchpad.is_button_on(packet, 'down'):
            if self.save:
                self.save = False
                self.launchpad.set_button("down", self.launchpad.active_button_color)
                self.launchpad.unset_button("up")
            else:
                self.disable()
            return True
        if self.launchpad.is_button_on(packet, 'up'):
            if not self.save:
                self.save = True
                self.launchpad.set_button("up", self.launchpad.active_button_color)
                self.launchpad.unset_button("down")
            else:
                self.disable()
            return True

        channel, msg_t, *data = packet

        if msg_t == USER:
            x, y, vel = data
            idx = self.launchpad.xy_to_idx(x, y, origin=(0, 0))
            if vel > 0:
                self.launchpad.set_pad(x, y, self.launchpad.active_color)
            else:
                self.launchpad.unset_pad(x, y)
                if self.confirm_idx is None:
                    self.confirm_idx = idx
                    self.launchpad.set_pad(x, y, self.launchpad.active_color)
                elif self.confirm_idx == idx:
                    self.confirm_idx = None
                    if self.save:
                        self.launchpad.set_pad(x, y, self.launchpad.base_color)
                        data = self.circuit.dump_patch(self.idx)
                        metadata = {"launchpad color": self.launchpad.base_color}
                        self.circuit.bank.set_patch(idx, data, metadata=metadata)
                    else:
                        data = self.circuit.bank.get_patch(idx)["data"]
                        if data is not None:
                            self.circuit.synth_data[self.idx] = data["synth"].copy()
                            self.circuit.user_data[self.idx] = data["user"].copy()
                            self.circuit.compile_user_functions()
                            self.circuit.send_synth_data(self.idx)
                    self.disable()
                else:
                    self.confirm_idx = None

        return True
