from ..miditypes import *
from ..drivers import LaunchpadPro

from .launchpad_pro_overlay import LaunchpadProOverlay


class LaunchpadProSendsOverlay(LaunchpadProOverlay):
    ignore_buttons = [
        "delete",
        "quantise",
        "double",
        "record",
        "record arm",
        "volume",
        "stop clip",
    ]

    def initialize(self):
        self.base_state = {}

    def enable(self, launchpad):
        if "listen" not in self.kernel.modes:
            return False

        self.launchpad = launchpad

        self.launchpad.coloring_mode = False
        self.launchpad.push_display(self.base_state)

        for name in self.ignore_buttons:
            self.launchpad.set_base_state(name, -1)
            self.launchpad.refresh_button(name)

        self.launchpad.set_base_state("sends", 1)
        self.launchpad.refresh_button("sends")

        self.active_note = 0

        self.show_sends()
        self.show_mods()

        self.active = True

    def disable(self):
        self.launchpad.pop_display()
        self.active = False

    ##################################################
    ## Handling
    ##################################################

    def unset_mode(self, mode):
        if mode == "listen":
            self.disable()

        return False

    def handle(self, obj, *packet):
        if self.launchpad.is_button_on(packet, "sends"):
            self.disable()
            return True

        for name in self.ignore_buttons:
            if self.launchpad.is_button_on(packet, name):
                return False
            if self.launchpad.is_button_off(packet, name):
                return False

        if len(self.kernel.actives) == 0:
            return True

        channel, msg_t, *data = packet

        if msg_t == USER:
            x, y, vel = data
            idx = self.launchpad.xy_to_idx(x, y)

            src = self.kernel.actives[-1]

            if idx < 16:
                if src in self.kernel.router.tables:
                    table = self.kernel.router.tables[src]
                    if idx < len(table):
                        (tag, channel, note, mod) = table[idx]
                        self.active_note = idx

                        if vel > 0:
                            if "delete" in self.kernel.modes:
                                self.kernel.router.bind(src, (tag, channel, note, mod))
                                self.kernel.note_off(tag, channel, note, 0, mod)
                                self.show_sends()
                                return True

                            self.kernel.note_on(tag, channel, note, vel, mod)
                            self.launchpad.set_pad(x, y, 24)
                            self.show_mods()
                        else:
                            self.kernel.note_off(tag, channel, note, 0, mod)
                            self.launchpad.set_pad(x, y, 27)
            elif idx < 32:
                idx = idx - 16
                if src in self.kernel.scene.tables:
                    table = list(self.kernel.scene.tables[src].items())
                    if idx < len(table):
                        dest, value = table[idx]

                        if vel > 0:
                            if "delete" in self.kernel.modes:
                                self.kernel.scene.unbind(src, dest)
                                self.show_sends()
                                return True

                            self.kernel._send(*dest, value)
                            self.launchpad.set_pad(x, y, 8)
                        else:
                            self.launchpad.set_pad(x, y, 11)
            elif idx < 40:
                if vel > 0:
                    if src in self.kernel.router.tables:
                        table = self.kernel.router.tables[src]
                        if self.active_note < len(table):
                            *pre, mod = table[self.active_note]

                            if idx == 32:
                                table[self.active_note] = (*pre, mod ^ LATCH)
                            elif idx == 33:
                                table[self.active_note] = (*pre, mod ^ LATCH_RELEASE)
                            elif idx == 34:
                                table[self.active_note] = (*pre, mod ^ CYCLE)
                            elif idx in (35, 36, 37, 38, 39):
                                mod = (mod & 0xF) ^ mod
                                if idx == 35:
                                    val = 0
                                if idx == 36:
                                    val = 3
                                elif idx == 37:
                                    val = 7
                                elif idx == 38:
                                    val = 11
                                elif idx == 39:
                                    val = 15
                                table[self.active_note] = (*pre, mod | val)

                            self.show_mods()

        return True

    def show_mods(self):
        src = self.kernel.actives[-1]
        if src in self.kernel.router.tables:
            table = self.kernel.router.tables[src]
            if self.active_note < len(table):
                tag, channel, note, mod = table[self.active_note]
            else:
                return False
        else:
            return False

        self.launchpad.set_pad(0, 3, 40 if mod & LATCH else 43)
        self.launchpad.set_pad(1, 3, 40 if mod & LATCH_RELEASE else 43)
        self.launchpad.set_pad(2, 3, 40 if mod & CYCLE else 43)
        self.launchpad.set_pad(3, 3, 56 if (mod & 0xF) == 0 else 59)
        self.launchpad.set_pad(4, 3, 56 if (mod & 0xF) == 3 else 59)
        self.launchpad.set_pad(5, 3, 56 if (mod & 0xF) == 7 else 59)
        self.launchpad.set_pad(6, 3, 56 if (mod & 0xF) == 11 else 59)
        self.launchpad.set_pad(7, 3, 56 if (mod & 0xF) == 15 else 59)

    def show_sends(self):
        for idx in range(32):
            self.launchpad.set_pad(*self.launchpad.idx_to_xy(idx), 0, force=True)

        if len(self.kernel.actives) == 0:
            self.show_string("----")
        else:
            src = self.kernel.actives[-1]
            if src in self.kernel.router.tables:
                table = self.kernel.router.tables[src]
                for idx, dest in enumerate(table):
                    x, y = self.launchpad.idx_to_xy(idx)
                    self.launchpad.set_pad(x, y, 27)

            if src in self.kernel.scene.tables:
                table = list(self.kernel.scene.tables[src].items())
                for idx, dest in enumerate(table):
                    x, y = self.launchpad.idx_to_xy(16 + idx)
                    self.launchpad.set_pad(x, y, 11)

            for idx in range(3):
                x, y = self.launchpad.idx_to_xy(32 + idx)
                self.launchpad.set_pad(x, y, 43)

            for idx in range(5):
                x, y = self.launchpad.idx_to_xy(35 + idx)
                self.launchpad.set_pad(x, y, 59)
