from ..converters import load_grid, dump_grid
from ..drivers import LaunchpadPro
from ..miditypes import *

from .launchpad_pro_overlay import LaunchpadProOverlay


class LaunchpadProSessionOverlay(LaunchpadProOverlay):
    default_session_color = 3
    color_table = (
        # ocean blue
        # 41,
        # indigo
        45,
        # purple
        49,
        # pink
        # 53,
        # magenta
        57,
        # red
        5,
        # orange
        9,
        # yellow
        13,
        # lime
        # 17,
        # light green
        21,
        # moss green
        # 25,
        # aqua green
        # 29,
        # aquamarine
        33,
        # sky blue
        # 37,
    )

    def enable(self, launchpad):
        self.active = True
        self.launchpad = launchpad

        self.launchpad.coloring_mode = False

        self.active_idx = self.kernel.global_state.get("idx")
        active_y = self.launchpad.idx_to_xy(self.active_idx, origin=(0, 0))[1]

        self.base_state = self.generate_grid()
        self.base_state["x0"] = 0
        self.base_state["y0"] = (8 * (active_y // 8)) % -64

        self.launchpad.push_display(self.base_state)
        self.launchpad.set_button("session", self.launchpad.active_button_color)

        self.show_active_session()
        self.show_page()

    def disable(self):
        if self.active:
            self.hide_active_session()
            self.launchpad.pop_display()

            if "save" in self.kernel.modes:
                self.kernel.pop_mode("save")

        self.active = False

    def set_mode(self, mode):
        if mode == "save":
            self.launchpad.set_button("up", self.launchpad.save_color)
            return True
        if mode == "delete":
            self.launchpad.set_button("delete", self.launchpad.delete_color)
            return True
        return False

    def unset_mode(self, mode):
        if mode == "save":
            self.launchpad.unset_button("up")
            return True
        if mode == "delete":
            self.launchpad.unset_button("delete")
            return True
        return False

    def handle(self, obj, *packet):
        if self.launchpad.is_button_on(packet, "session"):
            self.disable()
            return True

        if self.launchpad.coloring_handle(*packet):
            return True

        if self.launchpad.is_button_on(packet, "up"):
            self.kernel.toggle_mode("save")
            return True

        if self.launchpad.is_button_on(packet, "delete"):
            self.kernel.toggle_mode("delete")
            return True

        if self.launchpad.is_button_on(packet, "left"):
            self.hide_active_session()
            self.launchpad.move(y=-8, y_mod=-64)
            self.show_active_session()
            self.show_page()
            return True
        if self.launchpad.is_button_on(packet, "right"):
            self.hide_active_session()
            self.launchpad.move(y=+8, y_mod=-64)
            self.show_active_session()
            self.show_page()
            return True

        channel, msg_t, *data = packet

        if msg_t == USER:
            x, y, vel = data
            idx = self.launchpad.xy_to_idx(x, y, origin=(0, 0))
            if vel > 0:
                self.launchpad.set_pad(x, y, self.launchpad.active_color)
            else:
                self.launchpad.bank.set_global(
                    "session grid", dump_grid(self.base_state)
                )
                self.launchpad.bank.set_global("x0", self.base_state["x0"])
                self.launchpad.bank.set_global("y0", self.base_state["y0"])

                if "delete" in self.kernel.modes:
                    self.kernel.clear_session(idx)
                    self.launchpad.unset_base_state((x, y))
                elif "save" in self.kernel.modes:
                    self.kernel.save_session(idx)
                    self.disable()
                else:
                    self.disable()
                    self.kernel.load_session(idx)

                self.launchpad.unset_pad(x, y)
        return True

    def show_page(self):
        x0, y0 = self.launchpad.get_origin()
        color = self.color_table[y0 // 8 % 8]
        self.launchpad.set_button("left", color)
        self.launchpad.set_button("right", color)

    def show_active_session(self):
        x, y = self.launchpad.idx_to_xy(self.active_idx, origin=(0, 0))
        self.launchpad.set_pad(x, y, self.launchpad.active_color)

    def hide_active_session(self):
        x, y = self.launchpad.idx_to_xy(self.active_idx, origin=(0, 0))
        self.launchpad.unset_pad(x, y)

    def generate_grid(self):
        return {
            self.launchpad.idx_to_xy(idx, origin=(0, 0)): (
                self.launchpad.bank.get_from_session(idx, "color", 0)
            )
            for idx in range(512)
        }
