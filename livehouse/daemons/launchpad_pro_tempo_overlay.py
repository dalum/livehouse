from ..miditypes import *
from ..drivers import LaunchpadPro

from .launchpad_pro_overlay import LaunchpadProOverlay

class LaunchpadProTempoOverlay(LaunchpadProOverlay):
    beat_color = 3

    def initialize(self):
        self.base_state = {}
        self.kernel.clock.subscribe(self)

    def enable(self, launchpad):
        self.launchpad = launchpad

        self.launchpad.coloring_mode = False
        self.launchpad.push_display(self.base_state)

        self.launchpad.set_base_state('click', 1)
        self.launchpad.refresh_button('click')
        self.launchpad.set_base_state('1', 19)
        self.launchpad.refresh_button('1')

        self.show_tempo()
        self.show_play_button()

        self.active = True

    def disable(self):
        self.launchpad.pop_display()
        self.active = False

    ##################################################
    ## Beats
    ##################################################

    def beat(self):
        if self.active:
            self.launchpad.set_button('click', self.beat_color)

    def offbeat(self):
        if self.active:
            self.launchpad.unset_button('click')

    ##################################################
    ## Handling
    ##################################################

    def handle(self, obj, *packet):
        if self.launchpad.is_button_on(packet, 'click'):
            self.disable()
            return True

        if self.launchpad.is_button_on(packet, '1'):
            if self.kernel.clock.running:
                self.kernel.clock.time_stop()
            elif 'shift' in self.kernel.modes:
                self.kernel.clock.time_continue()
            else:
                self.kernel.clock.time_start()
            self.show_play_button()
            return True

        channel, msg_t, *data = packet

        if msg_t == USER:
            x, y, vel = data

            if vel > 0:
                if y > 3:
                    delta = (1 + 9*(y==7)) * (2 * (x > 3) - 1)
                    self.kernel.clock.set_tempo(self.kernel.clock.get_tempo() + delta)
                else:
                    self.kernel.clock.tap()
                self.show_tempo()

        return True

    def show_tempo(self):
        self.show_string(str(self.kernel.clock.get_tempo()).rjust(4))

    def show_play_button(self):
        if self.kernel.clock.running:
            self.launchpad.set_button('1', 17)
        else:
            self.launchpad.unset_button('1')
