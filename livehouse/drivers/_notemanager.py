from collections import Counter


class NoteManager:
    def __init__(self, device, **kwargs):
        self.device = device
        self.__dict__.update(kwargs)

        self.note_refs = Counter()
        self._latched = {}
        self.sustained_channels = set()

    def latched(self, channel: int):
        if channel not in self._latched:
            self._latched[channel] = set()
        return self._latched[channel]

    def reset(self):
        self.note_refs = Counter()
        self._latched = {}

    def latch(self, src: tuple):
        latched = self.latched(src[0])

        if src[0] not in self.sustained_channels and all(
            self.note_refs[src_] == 1 for src_ in latched
        ):
            for (channel, note) in latched:
                self.device.note_off(channel, note, 0)
            latched.clear()

        prerefs = self.note_refs[src]
        if src not in latched:
            self.note_refs[src] += 1
            latched.add(src)

        return prerefs

    def latch_release(self, channel: int):
        latched = self.latched(channel)

        for src in latched:
            channel, note = src
            self.device.note_off(channel, note, 0)

        latched.clear()

    def sustain(self, channel):
        self.sustained_channels.add(channel)

        latched = self.latched(channel)
        for src, count in self.note_refs.items():
            if count > 0 and channel == src[0]:
                self.latch(src)

    def sustain_release(self, channel):
        self.sustained_channels.discard(channel)
        self.latch_release(channel)
