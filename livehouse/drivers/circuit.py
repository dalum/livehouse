from ..bank import Bank
from .generic_driver import GenericDriver
from ..miditypes import *

from time import sleep

class Circuit(GenericDriver):
    name = "Circuit"

    locked = False
    controller_ignore = [13]
    save_on_patch = False
    reduced_session = False

    default_session_index = 0

    default_patch_data = [
        # patch name 0:15
        73, 110, 105, 116, 105, 97, 108, 32, 80, 97, 116, 99, 104, 32, 32, 32,
        # category 16
        0,
        # genre 17
        0,
        # reserved 18:31
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        # voice 32:35
        2, 0, 64, 64,
        # osc 1 36:44
        2, 127, 64, 0, 0, 0, 64, 64, 76,
        # osc 2 45:53
        2, 127, 64, 0, 0, 0, 64, 64, 76,
        # mixer 54:59
        127, 0, 0, 0, 64, 64,
        # filter 60:68
        0, 0, 0, 1, 127, 127, 0, 64, 64,
        # env1 69:73
        64, 2, 90, 127, 40,
        # env2 74:78
        64, 2, 75, 35, 45,
        # env3 79:83
        0, 10, 70, 64, 40,
        # lfo1 84:91
        0, 0, 0, 0, 0, 68, 0, 0,
        # lfo2 92:98
        0, 0, 0, 0, 0, 68, 0, 0,
        # FX 100:123
        0, 0, 0, 0, 0,
        64, 64, 64, 64, 125, 64,
        0, 0, 0, 0, 0,
        0, 100,
        1, 20, 0, 74, 64, 64,
        # mod matrix 124:203
        7, 0, 64, 1, # lf1- -> 1pi
        9, 0, 64, 2, # lf2- -> 2pi
        7, 0, 64, 3, # lf1- -> 1vsy
        9, 0, 64, 4, # lf2- -> 2vsy
        7, 0, 64, 12, # lf1- -> freq
        9, 0, 64, 16, # lf2- -> amp deca
        7, 0, 64, 11, # lf1- -> driv
        7, 0, 64, 13, # lf1- -> res

        12, 4, 64, 0, # env3*vel -> 12pi
        12, 4, 64, 9, # env3*vel -> nois
        12, 4, 64, 3, # env3*vel -> 1vsy
        12, 4, 64, 10, # env3*vel -> ring
        12, 4, 64, 4, # env3*vel -> 2vsy
        12, 4, 64, 13, # env3*vel -> res
        12, 4, 64, 11, # env3*vel -> driv
        12, 4, 64, 14, # env3*vel -> lfo1

        5, 0, 64, 1, # key -> 12pi
        5, 0, 64, 3, # key -> 1vsy
        5, 0, 64, 4, # key -> 2vsy
        5, 0, 64, 11, # key -> driv

        # macros 204:339
        0,
        26, 0, 127, 127, # 1atk
        30, 0, 127, 127, # 2atk
        *(127*(i % 4 == 2) + 64*(i % 4 == 3) for i in range(8)),

        0,
        39, 0, 127, 127, # 1rat
        *(127*(i % 4 == 2) + 64*(i % 4 == 3) for i in range(12)),

        0,
        29, 0, 127, 127, # 1rel
        33, 0, 127, 127, # 2rel
        *(127*(i % 4 == 2) + 64*(i % 4 == 3) for i in range(8)),

        0,
        5, 0, 127, 127, # 1vsy
        12, 0, 127, 127, # 2vsy
        *(127*(i % 4 == 2) + 64*(i % 4 == 3) for i in range(8)),

        0,
        45, 0, 127, 127, # dist
        *(127*(i % 4 == 2) + 64*(i % 4 == 3) for i in range(12)),

        0,
        21, 0, 127, 127, # freq
        *(127*(i % 4 == 2) + 64*(i % 4 == 3) for i in range(12)),

        0,
        22, 0, 127, 127, # res
        *(127*(i % 4 == 2) + 64*(i % 4 == 3) for i in range(12)),

        0,
        46, 0, 127, 127, # chor
        *(127*(i % 4 == 2) + 64*(i % 4 == 3) for i in range(12)),
    ]

    default_user_data = [*(127*(i % 4 == 3) for i in range(512))]

    default_session_data = [
        # reverb
        0, 0, 0, 0, 0, 0, 2, 64, 64,
        # delay
        0, 0, 0, 0, 0, 0, 64, 20, 64, 127, 4, 5,
        # filter
        64, 30,
        # sidechain
        0, 0, 50, 70, 0, 0, 0, 50, 70, 0,
        # mixer
        100, 100, 64, 64,
        # additional
        0,
        # drums
        0, 0, 64, 0, 0, 64, 64,
        16, 0, 64, 0, 0, 64, 64,
        32, 0, 64, 0, 0, 64, 64,
        36, 0, 64, 0, 0, 64, 64,
    ]

    reduced_session_msgs = (
        # reverb
        (3, CC, 88), (3, CC, 89), (3, CC, 90),
        (3, CC, 106), (3, CC, 109), (3, CC, 110),
        (3, NRPN, 1, 18), (3, NRPN, 1, 19), (3, NRPN, 1, 20),
        # delay
        (3, CC, 111), (3, CC, 112), (3, CC, 113), (3, CC, 114),
        (3, CC, 115), (3, CC, 116),
        (3, NRPN, 1, 6), (3, NRPN, 1, 7), (3, NRPN, 1, 8),
        (3, NRPN, 1, 9), (3, NRPN, 1, 10), (3, NRPN, 1, 11),
        # filter
        # (3, CC, 74), (3, CC, 71),
        None, None,
        # sidechain
        None, None, None, None, None,
        None, None, None, None, None,
        # mixer
        # (3, CC, 12), (3, CC, 14), (3, CC, 117), (3, CC, 118),
        None, None, None, None,
        # additional
        None,
        # drums
        None, None, None, None, None, None, None,
        None, None, None, None, None, None, None,
        None, None, None, None, None, None, None,
        None, None, None, None, None, None, None,
    )

    session_msgs = (
        # reverb
        (3, CC, 88), (3, CC, 89), (3, CC, 90),
        (3, CC, 106), (3, CC, 109), (3, CC, 110),
        (3, NRPN, 1, 18), (3, NRPN, 1, 19), (3, NRPN, 1, 20),
        # delay
        (3, CC, 111), (3, CC, 112), (3, CC, 113), (3, CC, 114),
        (3, CC, 115), (3, CC, 116),
        (3, NRPN, 1, 6), (3, NRPN, 1, 7), (3, NRPN, 1, 8),
        (3, NRPN, 1, 9), (3, NRPN, 1, 10), (3, NRPN, 1, 11),
        # filter
        # (3, CC, 74), (3, CC, 71),
        None, None,
        # sidechain
        (3, NRPN, 2, 55), (3, NRPN, 2, 56), (3, NRPN, 2, 57),
        (3, NRPN, 2, 58), (3, NRPN, 2, 59),
        (3, NRPN, 2, 65), (3, NRPN, 2, 66), (3, NRPN, 2, 67),
        (3, NRPN, 2, 68), (3, NRPN, 2, 69),
        # mixer
        # (3, CC, 12), (3, CC, 14), (3, CC, 117), (3, CC, 118),
        None, None, None, None,
        # additional
        # (3, NRPN, 1, 21),
        None,
        # drums
        # (2, CC, 8), (2, CC, 12), (2, CC, 14), (2, CC, 15), (2, CC, 16),
        # (2, CC, 17), (2, CC, 77),
        # (2, CC, 18), (2, CC, 23), (2, CC, 34), (2, CC, 40), (2, CC, 42),
        # (2, CC, 43), (2, CC, 78),
        # (2, CC, 44), (2, CC, 45), (2, CC, 46), (2, CC, 47), (2, CC, 48),
        # (2, CC, 49), (2, CC, 79),
        # (2, CC, 50), (2, CC, 53), (2, CC, 55), (2, CC, 57), (2, CC, 61),
        # (2, CC, 76), (2, CC, 80),
        None, None, None, None, None, None, None,
        None, None, None, None, None, None, None,
        None, None, None, None, None, None, None,
        None, None, None, None, None, None, None,
    )

    patch_addr_ptrs = (
        *((x,) for x in range(0, 91)), (91, 0, 1), (91, 1, 1), (91, 2, 1), (91, 3, 1), (91, 4, 2),
        *((x,) for x in range(92, 99)), (99, 0, 1), (99, 1, 1), (99, 2, 1), (99, 3, 1), (99, 4, 2),
        *((x,) for x in range(100, 339)),
    )

    patch_msgs = (
        # patch name 0:15
        None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None,
        # category 16
        None,
        # genre 17
        None,
        # reserved 18:31
        None, None, None, None, None, None, None, None, None, None, None, None, None, None,
        # voice 32:35
        (-1, CC, 3), (-1, CC, 5), (-1, CC, 9), (-1, CC, 13),
        # osc 1 36:44
        (-1, CC, 19), (-1, CC, 20), (-1, CC, 21), (-1, CC, 22), (-1, CC, 24),
        (-1, CC, 25), (-1, CC, 26), (-1, CC, 27), (-1, CC, 28),
        # osc 2 45:53
        (-1, CC, 29), (-1, CC, 30), (-1, CC, 31), (-1, CC, 33), (-1, CC, 35),
        (-1, CC, 36), (-1, CC, 37), (-1, CC, 39), (-1, CC, 40),
        # mixer 54:59
        (-1, CC, 51), (-1, CC, 52), (-1, CC, 54), (-1, CC, 56), (-1, CC, 58),
        (-1, CC, 59),
        # filter 60:68
        (-1, CC, 60), (-1, CC, 63), (-1, CC, 65), (-1, CC, 68), (-1, CC, 74),
        (-1, CC, 69), (-1, CC, 71), (-1, CC, 78), (-1, CC, 79),
        # env1 69:73
        (-1, CC, 108), (-1, CC, 73), (-1, CC, 75), (-1, CC, 70), (-1, CC, 72),
        # env2 74:78
        (-1, NRPN, 0, 0), (-1, NRPN, 0, 1), (-1, NRPN, 0, 2),
        (-1, NRPN, 0, 3), (-1, NRPN, 0, 4),
        # env3 79:83
        (-1, NRPN, 0, 14), (-1, NRPN, 0, 15), (-1, NRPN, 0, 16),
        (-1, NRPN, 0, 17), (-1, NRPN, 0, 18),
        # lfo1 84:91
        (-1, NRPN, 0, 70), (-1, NRPN, 0, 71), (-1, NRPN, 0, 72),
        (-1, NRPN, 0, 74), (-1, NRPN, 0, 75), (-1, NRPN, 0, 76),
        (-1, NRPN, 0, 77), [
            (-1, NRPN, 0, 122), (12, 13),
            (-1, NRPN, 0, 122), (14, 15),
            (-1, NRPN, 0, 122), (16, 17),
            (-1, NRPN, 0, 122), (18, 19),
            (-1, NRPN, 0, 123), range(4)
        ],
        # lfo2 92:98
        (-1, NRPN, 0, 79), (-1, NRPN, 0, 80), (-1, NRPN, 0, 81),
        (-1, NRPN, 0, 83), (-1, NRPN, 0, 84), (-1, NRPN, 0, 85),
        (-1, NRPN, 0, 86), [
            (-1, NRPN, 0, 122), (22, 23),
            (-1, NRPN, 0, 122), (24, 25),
            (-1, NRPN, 0, 122), (26, 27),
            (-1, NRPN, 0, 122), (28, 29),
            (-1, NRPN, 0, 123), range(4, 8)
        ],
        # FX 100:123
        (-1, CC, 91), None, (-1, CC, 93), None, None,
        (-1, NRPN, 0, 104), (-1, NRPN, 0, 105), (-1, NRPN, 0, 106),
        (-1, NRPN, 0, 107), (-1, NRPN, 0, 108), (-1, NRPN, 0, 109),
        None, None, None, None, None,
        (-1, NRPN, 1, 0), (-1, NRPN, 1, 1),
        (-1, NRPN, 1, 24), (-1, NRPN, 1, 25), (-1, NRPN, 1, 26),
        (-1, NRPN, 1, 27), (-1, NRPN, 1, 28), (-1, NRPN, 1, 29),
        # mod matrix 124:203
        *((-1, NRPN, 1, 83 + i) for i in range(128 - 83) if i % 5 != 2),
        *((-1, NRPN, 2, i) for i in range(55) if i % 5 != 2),
        # macros 204:339
        (-1, CC, 80), *((-1, NRPN, 3, i) for i in range(0, 16)),
        (-1, CC, 81), *((-1, NRPN, 3, i) for i in range(16, 32)),
        (-1, CC, 82), *((-1, NRPN, 3, i) for i in range(32, 48)),
        (-1, CC, 83), *((-1, NRPN, 3, i) for i in range(48, 64)),
        (-1, CC, 84), *((-1, NRPN, 3, i) for i in range(64, 80)),
        (-1, CC, 85), *((-1, NRPN, 3, i) for i in range(80, 96)),
        (-1, CC, 86), *((-1, NRPN, 3, i) for i in range(96, 112)),
        (-1, CC, 87), *((-1, NRPN, 3, i) for i in range(112, 128)),
    )

    user_destination_addrs = (
        (None, 0),
        ('patch', 33),
        ('patch', 37), ('patch', 38), ('patch', 39), ('patch', 40), ('patch', 41), ('patch', 42), ('patch', 43), ('pitch', 42, 43),
        ('patch', 46), ('patch', 47), ('patch', 48), ('patch', 49), ('patch', 50), ('patch', 51), ('patch', 52), ('pitch', 51, 52),
        ('patch', 54), ('patch', 55), ('patch', 56), ('patch', 57),
        ('patch', 64), ('patch', 66), ('patch', 61), ('patch', 65), ('patch', 68),
        ('patch', 70), ('patch', 71), ('patch', 72), ('patch', 73),
        ('patch', 75), ('patch', 76), ('patch', 77), ('patch', 78),
        ('patch', 79), ('patch', 80), ('patch', 81), ('patch', 82), ('patch', 83),
        ('patch', 89), ('patch', 90), ('patch', 86), ('patch', 97), ('patch', 98), ('patch', 94),
        ('patch', 100), ('patch', 102), ('patch', 119), ('patch', 121), ('patch', 122), ('patch', 123),
        ('session', 0), ('session', 1), ('session', 7),
        ('session', 9), ('session', 10), ('session', 15),
    )

    user_destination_msgs = (
        (None, 0),
        (CC, 5),
        (CC, 20), (CC, 21), (CC, 22), (CC, 24), (CC, 25), (CC, 26), (CC, 27), (26, 27),
        (CC, 30), (CC, 31), (CC, 33), (CC, 35), (CC, 36), (CC, 37), (CC, 39), (37, 39),
        (CC, 51), (CC, 52), (CC, 54), (CC, 56),
        (CC, 74), (CC, 71), (CC, 63), (CC, 69), (CC, 79),
        (CC, 73), (CC, 75), (CC, 70), (CC, 72),
        (NRPN, 0,1), (NRPN, 0,2), (NRPN, 0,3), (NRPN, 0,4),
        (NRPN, 0,14), (NRPN, 0,15), (NRPN, 0,16), (NRPN, 0,17), (NRPN, 0,18),
        # 1rat, 1syn, 1slw, 2rat, 2syn, 2slw
        (NRPN, 0,76), (NRPN, 0,77), (NRPN, 0,72), (NRPN, 0,85), (NRPN, 0,86), (NRPN, 0,81),
        # dist, chor, crat, cdfb, cdpt, cdel
        (CC, 91), (CC, 93), (NRPN, 1,25), (NRPN, 1,27), (NRPN, 1,28), (NRPN, 1,29),
        (CC, 88), (CC, 89), (NRPN, 1,19),
        (CC, 111), (CC, 112), (NRPN, 1,6),
    )

    def generate_tag(self, name):
        if self.usb and self.autodetect_tag:
            self.request_synth_data(0)
            for _ in range(10):
                if self.tag is None:
                    sleep(0.1)
                else:
                    break
            else:
                return False
        else:
            self.tag = name
        return True

    def initialize(self):
        if self.reduced_session:
            self.session_msgs = self.reduced_session_msgs

        self.msg_buffer = []
        self.user_funs = {}
        self.usb = self.__dict__.get("usb", True)
        self.autodetect_tag = self.__dict__.get("autodetect_tag", True)
        self.midi_in.ignore_types(sysex=False)
        self.user_data = {}
        self.synth_data = {}
        self.session_data = {}

    def enable(self):
        self.load_session(self.kernel.global_state.get("idx"))

    ##################################################
    ## Session
    ##################################################

    def load_session(self, session_idx):
        super().load_session(session_idx)

        self.user_data = self.bank.get("user data", {})
        if self.user_data is None:
            self.bank.set("user data", {})
            self.user_data = self.bank.get("user data", {})

        if "0" not in self.user_data:
            self.user_data["0"] = self.default_user_data.copy()
        if "1" not in self.user_data:
            self.user_data["1"] = self.default_user_data.copy()

        for idx in range(2):
            if len(self.user_data[str(idx)]) < 512:
                cache = self.default_user_data.copy()
                for i in range(len(self.user_data[str(idx)])):
                    cache[i] = self.user_data[str(idx)][i]
                self.user_data[str(idx)] = cache

        self.session_data = self.bank.get("session data", self.default_session_data.copy())

        self.synth_data = self.bank.get("synth data", {})
        if self.synth_data is None:
            self.bank.set("synth data", {})
            self.synth_data = self.bank.get("synth data", {})

        if "0" not in self.synth_data:
            self.synth_data["0"] = self.default_patch_data.copy()
        if "1" not in self.synth_data:
            self.synth_data["1"] = self.default_patch_data.copy()

        patch_name = list(map(ord, self.tag.ljust(16)))
        self.synth_data["0"][:16] = patch_name
        self.synth_data["1"][:16] = patch_name

        self.compile_user_functions()

        for msg, value in zip(self.session_msgs, self.session_data):
            if msg is None:
                continue

            channel_idx, *param = msg
            self.send(self.channels[channel_idx], *param, value)
            sleep(0.005)

        for idx in self.synth_data:
            sleep(0.1)
            self.send_synth_data(int(idx))

        self.kernel.callback('request state', self)

    ##################################################
    ## Messages
    ##################################################

    def user(self, controller, value):
        for fun in self.user_funs.get(controller, ()):
            fun(value)

    def _send(self, channel, msg_t, *data):
        if msg_t == CC:
            return self._cc(channel, *data)
        if msg_t == NRPN:
            return self._nrpn(channel, *data)

        return super().send(channel, msg_t, *data)

    def _cc(self, channel, controller, value):
        super().cc(channel, controller, value)

        try:
            idx = self.channels.index(channel)
            if idx < 2:
                addr_ptr = (self.patch_msgs.index((-1, CC, controller)),)
                self.set_patch_value(idx, addr_ptr, value, silent=True)
            else:
                addr_ptr = (self.session_msgs.index((idx, CC, controller)),)
                self.set_session_value(addr_ptr, value, silent=True)

            for (handler_name, handler) in self.handlers.items():
                if handler.active:
                    handler.request_state(self)

        except ValueError:
            pass

    def _nrpn(self, channel, param_msb, param_lsb, value_msb, value_lsb=None):
        super().nrpn(channel, param_msb, param_lsb, value_msb, value_lsb)

        try:
            idx = self.channels.index(channel)
            if idx < 2:
                addr_ptr = (self.patch_msgs.index((-1, NRPN, param_msb, param_lsb)),)
                self.set_patch_value(idx, addr_ptr, value_msb, silent=True)
            else:
                addr_ptr = (self.session_msgs.index((idx, NRPN, param_msb, param_lsb)),)
                self.set_session_value(addr_ptr, value_msb, silent=True)

            for (handler_name, handler) in self.handlers.items():
                if handler.active:
                    handler.request_state(self)

        except ValueError:
            pass

    def send_message(self, channel, msg_t, *data):
        super().send_message(channel, msg_t, *data)

    ##################################################
    ## User data
    ##################################################

    def compile_user_functions(self):
        slots = {}
        funs = {num: [] for num in range(16)}

        for synth_idx in range(2):
            channel = self.channels[synth_idx]
            user_data = self.user_data.get(str(synth_idx), None)
            synth_data = self.synth_data.get(str(synth_idx), None)
            if user_data is None or synth_data is None:
                continue

            for num in range(16):
                for offset in range(4):
                    idx = 16*num + 4*offset
                    if idx >= len(user_data):
                        continue
                    param = user_data[idx]
                    addr_t, *addr = self.user_destination_addrs[param]
                    if addr_t is None:
                        continue
                    depth = user_data[idx + 1]
                    v0 = user_data[idx + 2]
                    v1 = user_data[idx + 3]
                    if v0 == v1:
                        v1 += 1

                    user_dest = self.user_destination_msgs[param]

                    if addr_t == 'pitch':
                        slot_ids = []
                        ls = []
                        dests = []
                        for (a, d) in zip(addr, user_dest):
                            dest = (channel, CC, d)
                            dests.append(dest)
                            if dest not in slots:
                                slots[dest] = [synth_data[a]]
                            slot_ids.append(len(slots[dest]))
                            slots[dest].append(0)
                            ls.append(slots[dest])

                        def fun(value, ls=ls, slot_ids=slot_ids, dests=dests, depth=depth, v0=v0, v1=v1):
                            v = max(0, min(1, (value - v0) / (v1 - v0)))
                            prev_cents = sum(ls[1]) - ls[1][slot_ids[1]]
                            t = prev_cents + 100*depth*v - 14
                            cents = (t % 99) - prev_cents + 14
                            semi = t // 99
                            ls[0][slot_ids[0]] = semi
                            ls[1][slot_ids[1]] = cents
                            self.send(*dests[0], round(min(127, max(0, sum(ls[0])))))
                            self.send(*dests[1], round(min(127, max(0, sum(ls[1])))))

                        funs[num].append(fun)
                        continue

                    if addr_t == 'session':
                        dest = (self.channels[3], *user_dest)
                    else:
                        dest = (channel, *user_dest)

                    if dest not in slots:
                        if addr_t == 'patch':
                            slots[dest] = [synth_data[addr[0]]]
                        elif addr_t == 'session':
                            slots[dest] = [self.session_data[addr[0]]]

                    slot_idx = len(slots[dest])
                    slots[dest].append(0)

                    def fun(value, l=slots[dest], depth=depth, dest=dest, slot_idx=slot_idx, v0=v0, v1=v1):
                        l[slot_idx] = depth * max(0, min(1, (value - v0) / (v1 - v0)))
                        self.send(*dest, round(min(127, max(0, sum(l)))))

                    funs[num].append(fun)

        # Reset the state of previous user functions.
        for num in self.user_funs:
            for fun in self.user_funs[num]:
                fun(0)

        self.user_funs = funs

    def set_session_value(
        self, addr_ptr, value, silent=False, emulate=False, temporary=False
    ):
        if not temporary:
            self.session_data[addr_ptr[0]] = value
        msg = self.session_msgs[addr_ptr[0]]

        if msg is None:
            return True

        channel_idx, *param = msg

        self.compile_user_functions()

        if not silent and param is not None:
            self.send(self.channels[channel_idx], *param, value)

        if emulate and param is not None:
            self.handle(self.channels[channel_idx], *param, value)

        return True

    def set_user_value(self, idx, addr_ptr, value):
        if str(idx) not in self.user_data:
            self.user_data[str(idx)] = self.default_user_data.copy()
        user_data = self.user_data[str(idx)]
        user_data[addr_ptr[0]] = value

        self.compile_user_functions()

        return True

    def set_patch_value(
        self, idx, addr_ptr, value, silent=False, emulate=False, temporary=False
    ):
        patch_data = self.synth_data[str(idx)]

        if len(patch_data) == 0:
            return False
        if len(addr_ptr) == 3:
            param = self.patch_msgs[addr_ptr[0]][2*addr_ptr[1]]
            if not temporary:
                lo = min(self.patch_msgs[addr_ptr[0]][2*addr_ptr[1] + 1])
                data = patch_data[addr_ptr[0]]
                # bitwise overwrite the relevant bits in addr
                for i in range(addr_ptr[2]):
                    data ^= (-(((value - lo) >> i) & 1) ^ data) & (1 << (addr_ptr[1] + i))
                patch_data[addr_ptr[0]] = data
        else:
            param = self.patch_msgs[addr_ptr[0]]
            if not temporary:
                patch_data[addr_ptr[0]] = value

        self.compile_user_functions()

        if not silent and param is not None:
            self.send(self.channels[idx], *param[1:], value)

        if emulate and param is not None:
            self.handle(self.channels[idx], *param[1:], value)

        return True

    def get_session_value(self, addr_ptr, default=None):
        try:
            return self.session_data[addr_ptr[0]]
        except IndexError:
            return default

    def get_user_value(self, idx, addr_ptr, default=None):
        try:
            user_data = self.user_data[str(idx)]
        except KeyError:
            return default

        try:
            return user_data[addr_ptr[0]]
        except IndexError:
            return default

    def get_patch_value(self, idx, addr_ptr, default=None):
        try:
            patch_data = self.synth_data[str(idx)]
        except KeyError:
            return default

        try:
            if len(addr_ptr) == 3:
                # read `addr[2]` bits from `addr[1]` and forward
                lo = min(self.patch_msgs[addr_ptr[0]][2*addr_ptr[1] + 1])
                data = patch_data[addr_ptr[0]]
                return lo + ((data >> addr_ptr[1]) & (2**addr_ptr[2] - 1))
            else:
                return patch_data[addr_ptr[0]]
        except IndexError:
            return default

    def dump_patch(self, idx):
        return {
            "synth": self.synth_data[str(idx)],
            "user": self.user_data[str(idx)],
        }

    def send_synth_data(self, idx):
        if self.usb:
            data = self.synth_data[str(idx)]
            if len(data) > 0:
                self.midi_out.send_message((240, 0, 32, 41, 1, 96, 0, idx, 0, *data, 247))
        else:
            for addr_ptr in self.patch_addr_ptrs:
                if len(addr_ptr) == 3:
                    param = self.patch_msgs[addr_ptr[0]][2*addr_ptr[1]]
                else:
                    param = self.patch_msgs[addr_ptr[0]]
                if param is not None:
                    self.send(self.channels[idx], *param[1:], self.get_patch_value(idx, addr_ptr))
                    sleep(0.002)

    def request_synth_data(self, idx):
        self.midi_out.send_message((240, 0, 32, 41, 1, 96, 64, idx, 247))

    ##################################################
    ## Callback
    ##################################################

    def bootstrap_callback(self, msg, data):
        msg, delta_t = msg
        msg_t = msg[0]
        if msg_t != SYSEX:
            return
        data = tuple(msg[1:]) if len(msg) > 1 else ()

        if not self.handle_sysex(data):
            return

        self.tag = "".join(map(chr, self.synth_data["0"][:16])).strip()
        self.bank = Bank(self.tag)

    def callback(self, msg, data):
        msg, delta_t = msg
        msg_t = msg[0] >> 4
        if msg_t == 0xF:
            msg_t = msg[0]
            channel = None
        else:
            channel = msg[0] & 0x0F

        data = tuple(msg[1:]) if len(msg) > 1 else ()

        if msg_t == SYSEX:
            if self.handle_sysex(data):
                return

        # Ignore programe change events.
        if msg_t == PROGRAM_CHANGE:
            return

        if msg_t == CC and data[0] in (98, 99, 100, 101):
            self.msg_buffer.extend(msg)
            return
        elif len(self.msg_buffer) > 1:
            self.msg_buffer.extend(msg)
            msg = self.msg_buffer[:]
            self.msg_buffer = []
            if all(map(lambda x: x == msg[0], msg[3::3])):
                if msg[1] == 99 and msg[4] == 98:
                    msg_t = NRPN
                    data = msg[2::3]

        self.handle(channel, msg_t, *data)

    def handle_sysex(self, data):
        if (
            data[0] == 0 and
            data[1] == 32 and
            data[2] == 41 and
            data[3] == 1 and
            data[4] == 96 and
            data[5] == 0
        ):
            idx = data[6]

            if not idx in self.synth_data:
                self.synth_data[str(idx)] = self.default_patch_data.copy()

            for i, value in enumerate(data[8:-1]):
                self.synth_data[str(idx)][i] = value

            if self.tag is not None:
                self.synth_data[str(idx)][:16] = list(map(ord, self.tag.ljust(16)))

            for (handler_name, handler) in self.handlers.items():
                if handler.active:
                    handler.request_state(self)

            return True

        return False

    ##################################################
    ## Display
    ##################################################

    def show_active(self, channel, *data):
        self.send(channel, NOTE_ON, *data, 1)

    def hide_active(self, channel, *data):
        self.send(channel, NOTE_OFF, *data, 0)
