import cProfile
import io
import os
import pstats
import typing

from ..bank import Bank
from ..miditypes import *

from ._notemanager import NoteManager


class GenericDriver:
    name = "GenericDriver"

    controller_ignore = []
    locked = True
    clock_in = True
    clock_out = False
    hardware_sustain = False
    channels = range(16)
    bindings = {}
    only_send = False

    def __init__(self, kernel, name: str, port: int, midi_in, midi_out, **kwargs):
        self.kernel = kernel
        self.name = name
        self.port = port
        self.midi_in = midi_in
        self.midi_out = midi_out
        self.tag = kwargs.get("tag", None)
        self.handlers = kwargs.get("handlers", {})
        self.active = kwargs.get("active", True)
        self.__dict__.update(kwargs)

        if self.clock_out:
            self.midi_in.ignore_types(timing=False)

        self.nm = NoteManager(self)
        self.msg_buffer = []

        self.initialize()

    def generate_tag(self, name):
        self.tag = name
        return True

    def update_tag(self, tag):
        self.kernel.update_tag(self, tag)
        self.tag = tag

    def initialize(self):
        pass

    def _destroy(self):
        for (handler_name, handler) in self.handlers.items():
            handler.deregister(self)

        self.destroy()

    def destroy(self):
        pass

    def _bootstrap(self):
        if not self.bootstrapped():
            return False
        if self.bootstrap():
            for (handler_name, handler) in self.handlers.items():
                handler.register(self)
            return True

        return False

    def bootstrapped(self):
        if self.tag is None and not self.generate_tag(self.name):
            return False
        return True

    def bootstrap(self):
        self.bank = Bank(self.tag)
        return True

    def enable(self):
        pass

    ##################################################
    ## Description
    ##################################################

    def get_description(self) -> typing.Tuple[str, int, str, str]:
        return self.name, self.port, self.__class__.name, self.tag

    def get_identifier(self):
        return (self.name, self.port)

    ##################################################
    ## Session
    ##################################################

    def load_session(self, idx: int):
        self.bank.load_session(idx)
        self.bank.save_globals()

        for (handler_name, handler) in self.handlers.items():
            if handler.active:
                handler.load_session(self, idx)

        for channel in self.channels:
            self.nm.latch_release(channel)

    def save_session(self, idx: int):
        for (handler_name, handler) in self.handlers.items():
            if handler.active:
                handler.save_session(self, idx)

        self.bank.save_session(idx)
        self.bank.save_globals()

    def clear_session(self, idx: int):
        for (handler_name, handler) in self.handlers.items():
            if handler.active:
                handler.clear_session(self, idx)

        self.bank.clear_session(idx)
        self.bank.save_globals()

    ##################################################
    ## Modes
    ##################################################

    def _set_mode(self, mode: str):
        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler._set_mode(mode):
                return True
        return self.set_mode(mode)

    def _unset_mode(self, mode: str):
        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler._unset_mode(mode):
                return True
        return self.unset_mode(mode)

    def set_mode(self, mode: str):
        return False

    def unset_mode(self, mode: str):
        return False

    ##################################################
    ## Status
    ##################################################

    def _set_status(self, status, value):
        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler._set_status(status, value):
                return True
        return self.set_status(status, value)

    def _clear_status(self, status):
        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler._clear_status(status):
                return True
        return self.clear_status(status)

    def set_status(self, status, value):
        return False

    def clear_status(self, status):
        return False

    ##################################################
    ## Messages
    ##################################################

    def user(self, controller, value):
        pass

    def _send(self, channel, msg_t, *data):
        self.send(channel, msg_t, *data)

    def send(self, channel, msg_t, *data):
        if msg_t == NOTE_ON:
            return self.note_on(channel, *data)
        if msg_t == NOTE_OFF:
            return self.note_off(channel, *data)
        if msg_t == CC:
            return self.cc(channel, *data)
        if msg_t == NRPN:
            return self.nrpn(channel, *data)
        if msg_t == USER:
            return self.user(*data)
        if msg_t == TIME_START:
            return self.time_start()
        if msg_t == TIME_CONTINUE:
            return self.time_continue()
        if msg_t == TIME_STOP:
            return self.time_stop()

    def note_on(self, channel, note, vel, mod=0):
        if mod & LATCH_RELEASE:
            self.nm.latch_release(channel)
            return

        mod_vel = mod & 0xF
        if mod_vel:
            vel = 7 + 8 * mod_vel

        src = (channel, note)

        if mod & LATCH or channel in self.nm.sustained_channels:
            self.nm.latch(src)

        self.send_message(channel, NOTE_ON, note, vel)
        self.nm.note_refs[src] += 1

    def note_off(self, channel, note, vel, mod=0):
        if mod & LATCH_RELEASE:
            return

        src = (channel, note)
        nrefs = self.nm.note_refs[src]
        if nrefs == 1:
            self.send_message(channel, NOTE_OFF, note, vel)
        if nrefs > 0:
            self.nm.note_refs[src] -= 1

    def sustain_on(self, channel):
        if self.hardware_sustain:
            self.cc(channel, 64, 127)
        else:
            self.nm.sustain(channel)

    def sustain_off(self, channel):
        if self.hardware_sustain:
            self.cc(channel, 64, 0)
        else:
            self.nm.sustain_release(channel)

    def cc(self, channel, controller, value):
        self.send_message(channel, CC, controller, value)

    def nrpn(self, channel, param_msb, param_lsb, value_msb, value_lsb=None):
        self.send_message(channel, CC, 99, param_msb)
        self.send_message(channel, CC, 98, param_lsb)
        self.send_message(channel, CC, 6, value_msb)
        if value_lsb is not None:
            self.send_message(channel, CC, 38, value_msb)

    def all_off(self, channel=None):
        if channel is None:
            for channel in self.channels:
                self.send(channel, CC, 123, 0)
        else:
            self.send(channel, CC, 123, 0)
        self.nm.reset()

    def send_message(self, channel, msg_t, *data):
        if not self.only_send:
            self.midi_out.send_message(((msg_t << 4) + channel, *data))

    ##################################################
    ## Timing
    ##################################################

    def time_clock(self):
        if self.clock_in:
            self.midi_out.send_message((TIME_CLOCK,))

    def time_start(self):
        if self.clock_in:
            self.midi_out.send_message((TIME_START,))

    def time_continue(self):
        if self.clock_in:
            self.midi_out.send_message((TIME_CONTINUE,))

    def time_stop(self):
        if self.clock_in:
            self.midi_out.send_message((TIME_STOP,))

    ##################################################
    ## Beats
    ##################################################

    def beat(self):
        pass

    def offbeat(self):
        pass

    ##################################################
    ## Callback
    ##################################################

    def bootstrap_callback(self, msg, data):
        msg, delta_t = msg
        s = " ".join(map(lambda x: hex(x)[2:].upper().zfill(2), msg))
        print("[BOOTSTRAP] %s: %s" % (self.name, s))

    def callback(self, msg, data):
        msg, delta_t = msg
        msg_t = msg[0] >> 4
        if msg_t == 0xF:
            msg_t = msg[0]
            channel = None
        else:
            channel = msg[0] & 0x0F

        if self.clock_out and msg_t == TIME_CLOCK:
            self.kernel.time_clock()
        if self.clock_out and msg_t == TIME_START:
            self.kernel.time_start()
        if self.clock_out and msg_t == TIME_STOP:
            self.kernel.time_stop()
        if self.clock_out and msg_t == TIME_CONTINUE:
            self.kernel.time_continue()

        data = tuple(msg[1:]) if len(msg) > 1 else ()
        if msg_t == NOTE_ON and data[1] == 0:
            msg_t = NOTE_OFF

        if msg_t == CC and data[0] in (98, 99, 100, 101):
            self.msg_buffer.extend(msg)
            return
        elif len(self.msg_buffer) > 1:
            self.msg_buffer.extend(msg)
            msg = self.msg_buffer[:]
            self.msg_buffer = []
            if all(map(lambda x: x == msg[0], msg[3::3])):
                if msg[1] == 99 and msg[4] == 98:
                    msg_t = NRPN
                    data = msg[2::3]

        self.handle(channel, msg_t, *data)

    def test_callback(self, msg, data):
        msg, delta_t = msg
        msg_t = msg[0] >> 4
        if msg_t == 0xF:
            msg_t = msg[0]
            channel = None
        else:
            channel = msg[0] & 0x0F

        data = tuple(msg[1:]) if len(msg) > 1 else ()

        print("[TEST] %s: %s" % (self.name, (channel, msg_t, *data)))

    def profile_callback(self, msg, data):
        prof = cProfile.Profile()
        retval = prof.runcall(self.callback, msg)
        s = io.StringIO()
        sortby = "cumulative"
        ps = pstats.Stats(prof, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())

    ##################################################
    ## Packet handling
    ##################################################

    def handle(self, *packet):
        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler.handle(self, *packet):
                return True

        if "listen" in self.kernel.modes:
            self.program_handle(*packet)
        self.play_handle(*packet)

    def program_handle(self, channel, msg_t, *data):
        self.kernel.set_status("msg_t", msg_t)

        if "lock" in self.kernel.modes and not self.locked:
            self.locked = True
            print("%s: locked" % (self.name))
        elif "unlock" in self.kernel.modes and self.locked:
            self.locked = False
            print("%s: unlocked" % (self.name))

        if self.locked:
            return

        ### NOTE_ON
        if msg_t == NOTE_ON:
            note, vel = data

            if "bind" in self.kernel.modes:
                mod = self.kernel.get_mod()
                device = (self.tag, channel)
                dest = (*device, 0 if mod & LATCH_RELEASE else note, mod)

                if dest not in self.kernel.actives:
                    if not all(
                        self.kernel.router.bind(src, dest)
                        for src in self.kernel.actives
                    ):
                        self.kernel.note_off(dest[0], dest[1], dest[2], 0)

            else:
                src = (self.tag, channel, note)
                if "delete" in self.kernel.modes:
                    self.kernel.router.clear(src)
                    self.kernel.scene.clear(src)
                else:
                    if "shift" not in self.kernel.modes:
                        self.kernel.clear_active()

                    if not self.kernel.push_active(src):
                        self.kernel.pop_active(src)

        ### CC
        if msg_t == CC:
            controller, value = data
            dest = (self.tag, channel, CC, controller)
            if "delete" in self.kernel.modes:
                for src in self.kernel.actives:
                    self.kernel.scene.unbind(src, dest)
            elif (
                "bind" in self.kernel.modes and controller not in self.controller_ignore
            ):
                for src in self.kernel.actives:
                    self.kernel.scene.bind(src, dest, value)

        ### NRPN
        if msg_t == NRPN:
            msb, lsb, value = data
            dest = (self.tag, channel, NRPN, msb, lsb)
            if "delete" in self.kernel.modes:
                for src in self.kernel.actives:
                    self.kernel.scene.unbind(src, dest)
            elif (
                "bind" in self.kernel.modes and (msb, lsb) not in self.controller_ignore
            ):
                for src in self.kernel.actives:
                    self.kernel.scene.bind(src, dest, value)

    def play_handle(self, channel, msg_t, *data):
        if msg_t == NOTE_ON:
            note, vel = data
            src = (self.tag, channel, note)
            self.kernel.scene.enter(src)
            self.kernel.router.note_on(src, vel)

        if msg_t == NOTE_OFF:
            note, vel = data
            src = (self.tag, channel, note)
            self.kernel.router.note_off(src)
