from time import sleep

from .generic_driver import GenericDriver
from ..converters import load_grid, dump_grid
from ..miditypes import *


class LaunchpadPro(GenericDriver):
    name = "LaunchpadPro"

    locked = False
    channel = 0
    active_color = (63, 63, 63)
    default_base_color = 55
    unhandled_color = 3
    shift_color = 3
    active_button_color = 1  # 94
    save_color = 64
    delete_color = 5
    record_color = 5
    latch_color = 8
    latch_release_color = 7

    vel3_color = 55
    vel7_color = 54
    vel11_color = 53
    vel15_color = 52

    notes = {(i + 1) * 10 + j + 1 for i in range(8) for j in range(8)}

    buttons = {
        "up": 91,
        "down": 92,
        "left": 93,
        "right": 94,
        "session": 95,
        "note": 96,
        "device": 97,
        "user": 98,
        "click": 70,
        "undo": 60,
        "delete": 50,
        "quantise": 40,
        "duplicate": 30,
        "double": 20,
        "record": 10,
        "record arm": 1,
        "track select": 2,
        "mute": 3,
        "solo": 4,
        "volume": 5,
        "pan": 6,
        "sends": 7,
        "stop clip": 8,
        "1": 19,
        "2": 29,
        "3": 39,
        "4": 49,
        "5": 59,
        "6": 69,
        "7": 79,
        "8": 89,
    }

    persistent_buttons = {
        "shift": 80,
    }

    button_mode_map = {
        "session": "session",
        "device": "device",
        "user": "user",
        "shift": "shift",
        "click": "click",
        "delete": "delete",
        "duplicate": "duplicate",
        "set mod": "quantise",
        "latch": "double",
        "latch release": "double",
        "bind": "record",
        "listen": "record arm",
        "lock": "mute",
        "unlock": "solo",
        "velocity": "volume",
        "status msg_t": "stop clip",
        **{f"vel{x}": "volume" for x in range(1, 16)},
    }

    color_table = (
        # white
        1,
        117,
        2,
        3,
        # red
        7,
        6,
        5,
        4,
        # orange
        11,
        10,
        9,
        8,
        # yellow
        15,
        14,
        13,
        12,
        # lime
        19,
        18,
        17,
        16,
        # light green
        23,
        22,
        21,
        20,
        # moss green
        27,
        26,
        25,
        24,
        # aqua green
        31,
        30,
        29,
        28,
        # aquamarine
        35,
        34,
        33,
        32,
        # sky blue
        39,
        38,
        37,
        36,
        # ocean blue
        43,
        42,
        41,
        40,
        # indigo
        47,
        46,
        45,
        44,
        # purple
        51,
        50,
        49,
        48,
        # pink
        55,
        54,
        53,
        52,
        # magenta
        59,
        58,
        57,
        56,
    )

    status_colors = {
        NOTE_ON: 23,
        NOTE_OFF: 46,
        CC: 10,
    }

    def initialize(self):
        self._bootstrapped = False

        self.kernel.register_callback("active push", self.push_active)
        self.kernel.register_callback("active pop", self.pop_active)
        self.kernel.register_callback("active clear", self.clear_active)

    def bootstrapped(self):
        if self.tag is None and not self.generate_tag(self.name):
            return False
        if not self.set_programmer_mode():
            return False
        return True

    def set_programmer_mode(self):
        if self._bootstrapped is False:
            sleep(3.5)
            self._bootstrapped = True
        self.midi_out.send_message((240, 0, 32, 41, 2, 16, 44, 3, 247))
        return True

    def enable(self):
        self.coloring_mode = False
        self.coloring_pick_up = False
        self.base_color = 55

        self.pad_state_stack = [{}]
        self.button_state_stack = [{}]
        self.display_stack = [{}]
        self.coloring_button_state_cache_stack = [{}]
        self.persistent_button_state = {}
        self.flush_display()

        self.bindings = {
            "panic": (0, CC, 8, 127),
            "coloring on": (0, CC, 2, 127),
            "brighten color": (0, CC, 91, 127),
            "dim color": (0, CC, 92, 127),
            "prev color": (0, CC, 93, 127),
            "next color": (0, CC, 94, 127),
        }

        # Select programmer mode
        self.load_session(self.kernel.global_state.get("idx"))

    ##################################################
    ## Session
    ##################################################

    def load_session(self, idx):
        super().load_session(idx)

        self.active_xy = set()
        self.base_state = load_grid(self.bank.get("grid", {}))
        self.base_state["x0"] = self.bank.get("x0", 0)
        self.base_state["y0"] = self.bank.get("y0", 0)

        self.base_color = self.bank.get("color", self.default_base_color)
        if self.base_color not in self.color_table:
            self.base_color = self.default_base_color

        self.replace_display(self.base_state)

        self.kernel.callback("request state", self)

    def save_session(self, idx):
        self.bank.set("grid", dump_grid(self.base_state))
        self.bank.set("x0", self.base_state["x0"])
        self.bank.set("y0", self.base_state["y0"])

        if self.coloring_mode:
            self.bank.set("color", self.base_color)

        super().save_session(idx)

    ##################################################
    ## Conversion
    ##################################################

    def xy_to_note(self, x, y, origin=None):
        x0, y0 = origin if origin is not None else self.get_origin()
        if not (0 <= x - x0 < 8) or not (0 <= y - y0 < 8):
            return None
        return (1 + x - x0) + 10 * (1 + y - y0)

    def note_to_xy(self, note, origin=None):
        x0, y0 = origin if origin is not None else self.get_origin()
        return x0 + (note % 10) - 1, y0 + (note // 10) - 1

    def xy_to_idx(self, x, y, origin=None):
        x0, y0 = origin if origin is not None else self.get_origin()
        return x - x0 + 8 * (7 - y + y0)

    def idx_to_xy(self, idx, origin=None):
        x0, y0 = origin if origin is not None else self.get_origin()
        return x0 + (idx % 8), y0 + 7 - (idx // 8)

    def name_to_controller(self, name):
        if name in self.buttons:
            return self.buttons[name]
        if name in self.persistent_buttons:
            return self.persistent_buttons[name]
        raise KeyError("unknown button name: %s" % name)

    ##################################################
    ## Modes
    ##################################################

    def set_mode(self, mode):
        color = self.active_button_color
        if mode == "bind":
            self.set_button("record", self.record_color)
            return True
        elif mode == "latch":
            self.set_button("double", self.latch_color)
            return True
        elif mode == "latch release":
            self.set_button("double", self.latch_release_color)
            return True
        elif mode == "listen":
            if self.is_active_display():
                for (x, y) in self.active_xy:
                    self.refresh_pad(x, y)
        elif mode == "shift":
            self.set_button("shift", self.shift_color)
            return True
        elif mode == "vel3":
            self.set_button("volume", self.vel3_color)
            return True
        elif mode == "vel7":
            self.set_button("volume", self.vel7_color)
            return True
        elif mode == "vel11":
            self.set_button("volume", self.vel11_color)
            return True
        elif mode == "vel15":
            self.set_button("volume", self.vel15_color)
            return True

        if mode in self.button_mode_map:
            self.set_button(self.button_mode_map[mode], color)
            return True

        return False

    def unset_mode(self, mode):
        if mode == "listen":
            if self.is_active_display():
                for (x, y) in self.active_xy:
                    self.refresh_pad(x, y)
        if mode in self.button_mode_map:
            self.unset_button(self.button_mode_map[mode])
            return True

        return False

    ##################################################
    ## Active device
    ##################################################

    def push_active(self, src):
        if src[0] == self.tag:
            self.active_xy.add(src[2:])

        if self.is_active_display():
            for (x, y) in self.active_xy:
                self.refresh_pad(x, y)

    def pop_active(self, src):
        if src[0] == self.tag:
            self.active_xy.discard(src[2:])

        if self.is_active_display():
            for (x, y) in self.active_xy:
                self.refresh_pad(x, y)

    def clear_active(self):
        active_xy = self.active_xy.copy()
        self.active_xy.clear()

        if self.is_active_display():
            for (x, y) in active_xy:
                self.refresh_pad(x, y)

    def is_active_xy(self, x, y):
        if "listen" not in self.kernel.modes or not self.is_active_display():
            return False

        for (x_, y_) in self.active_xy:
            if x == x_ and y == y_:
                return True

        return False

    ##################################################
    ## Status
    ##################################################

    def set_status(self, status, value):
        button = self.button_mode_map.get("status %s" % status, None)
        if button is not None:
            self.set_button(button, self.status_colors.get(value, self.unhandled_color))

    def clear_status(self, status):
        button = self.button_mode_map.get("status %s" % status, None)
        if button is not None:
            self.unset_button(button)

    ##################################################
    ## Position
    ##################################################

    def get_origin(self):
        return self.display_stack[-1].get("x0", 0), self.display_stack[-1].get("y0", 0)

    def set_origin(self, x0, y0):
        self.display_stack[-1]["x0"] = x0
        self.display_stack[-1]["y0"] = y0

    def move(self, x=0, y=0, x_mod=None, y_mod=None):
        x0, y0 = self.get_origin()
        x0 = x0 + x if x_mod is None else (x0 + x) % x_mod
        y0 = y0 + y if y_mod is None else (y0 + y) % y_mod
        self.set_origin(x0, y0)
        self.refresh_display()

    ##################################################
    ## Messages
    ##################################################

    def send(self, channel, msg_t, *data):
        # Ignore note on/off messages
        if msg_t == NOTE_ON or msg_t == NOTE_OFF:
            return
        self.midi_out.send_message(((msg_t << 4) + channel, *data))

    def all_off(self):
        self.refresh_display()

    ##################################################
    ## Beats
    ##################################################

    def beat(self):
        self.set_button("click", self.beat_color)

    def offbeat(self):
        self.unset_button("click")

    ##################################################
    ## Callback
    ##################################################

    def callback(self, msg, data):
        msg, delta_t = msg
        msg_t = msg[0] >> 4
        if msg_t == 0xF:
            msg_t = msg[0]
            channel = None
        else:
            channel = msg[0] & 0x0F

        # Interrupt polyphonic aftertouch and send to buffer
        if msg_t == POLYPHONIC_AFTERTOUCH or msg_t == CHANNEL_AFTERTOUCH:
            self.buffer.raw[4] = msg[2]
            return

        data = tuple(msg[1:]) if len(msg) > 1 else ()

        if msg_t == NOTE_ON:
            msg_t = USER
            data = (*self.note_to_xy(data[0]), data[1])

        self.handle(channel, msg_t, *data)

    ##################################################
    ## Packet handling
    ##################################################

    def handle(self, *packet):
        if packet[1] == CC:
            if self.bindings["panic"] == packet:
                self.kernel.all_off()
                self._clear_status("msg_t")
            elif self.is_button_on(packet, "shift"):
                self.kernel.push_mode("shift")
            elif self.is_button_off(packet, "shift"):
                self.kernel.pop_mode("shift")

        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler.handle(self, *packet):
                return True

        if self.control_handle(*packet):
            return True

        if "listen" in self.kernel.modes:
            self.program_handle(*packet)
        self.play_handle(*packet)

    def control_handle(self, *packet):
        if "shift" not in self.kernel.modes:
            if not self.coloring_mode and self.is_button_on(packet, "up"):
                self.move(0, +1)
                return True
            if not self.coloring_mode and self.is_button_on(packet, "down"):
                self.move(0, -1)
                return True
            if not self.coloring_mode and self.is_button_on(packet, "left"):
                self.move(-1, 0)
                return True
            if not self.coloring_mode and self.is_button_on(packet, "right"):
                self.move(+1, 0)
                return True

        for (name, handler) in self.handlers.items():
            if self.is_button(name) and self.is_button_on(packet, name):
                handler.enable(self)
                return True

        if self.is_button_on(packet, self.button_mode_map["listen"]):
            if not self.kernel.toggle_mode("listen"):
                self.coloring_disable()
            return True

        if "listen" not in self.kernel.modes:
            return False

        if not self.coloring_mode and self.is_button_on(packet, "up"):
            for src in self.kernel.actives:
                self.kernel.router.set_transpose(src, 12)
            return True
        if not self.coloring_mode and self.is_button_on(packet, "down"):
            for src in self.kernel.actives:
                self.kernel.router.set_transpose(src, -12)
            return True
        if not self.coloring_mode and self.is_button_on(packet, "left"):
            for src in self.kernel.actives:
                self.kernel.router.set_transpose(src, -1)
            return True
        if not self.coloring_mode and self.is_button_on(packet, "right"):
            for src in self.kernel.actives:
                self.kernel.router.set_transpose(src, 1)
            return True

        if self.is_button_on(packet, self.button_mode_map["set mod"]):
            mod = self.kernel.get_mod()

            for src in self.kernel.actives:
                self.kernel.router.set_mod(src, mod)
            # No return, we handle that later.

        if self.is_button_on(packet, self.button_mode_map["latch"]):
            self.kernel.cycle_modes("latch", "latch release")
            return True

        if self.is_button_on(packet, self.button_mode_map["velocity"]):
            self.kernel.cycle_modes("vel3", "vel7", "vel11", "vel15")
            return True

        for mode in ("bind",):
            if self.is_button_on(packet, self.button_mode_map[mode]):
                self.kernel.toggle_mode(mode)
                return True

        for mode in ("delete", "device", "duplicate", "lock", "unlock"):
            if self.is_button_on(packet, self.button_mode_map[mode]):
                self.kernel.push_mode(mode)
                return True
            if self.is_button_off(packet, self.button_mode_map[mode]):
                self.kernel.pop_mode(mode)
                return True

        if self.coloring_handle(*packet):
            return True

        for name in ("quantise", "stop clip"):
            if self.is_button_on(packet, name):
                self.set_button(name, self.active_button_color)
                return True
            if self.is_button_off(packet, name):
                self.unset_button(name)
                return True

        return False

    def coloring_handle(self, *packet):
        if self.bindings["coloring on"] == packet:
            if "shift" in self.kernel.modes:
                self.coloring_pick_up = True
                self.coloring_enable()
            else:
                if self.coloring_mode:
                    self.coloring_disable()
                else:
                    self.coloring_enable()
            return True

        if self.coloring_mode and self.bindings["next color"] == packet:
            return self.set_base_color(self.next_color(self.base_color))

        if self.coloring_mode and self.bindings["prev color"] == packet:
            return self.set_base_color(self.prev_color(self.base_color))

        if self.coloring_mode and self.bindings["dim color"] == packet:
            return self.set_base_color(self.dim_color(self.base_color))

        if self.coloring_mode and self.bindings["brighten color"] == packet:
            return self.set_base_color(self.brighten_color(self.base_color))

    def program_handle(self, channel, msg_t, *data):
        if "lock" in self.kernel.modes and not self.locked:
            self.locked = True
            print("%s: locked" % (self.name))
        elif "unlock" in self.kernel.modes and self.locked:
            self.locked = False
            print("%s: unlocked" % (self.name))

        ### USER
        if msg_t == USER:
            x, y, vel = data
            src = (self.tag, channel, x, y)

            if self.coloring_pick_up:
                self.base_color = self.get_base_state((x, y))
                for name in ("up", "down", "left", "right", "track select"):
                    self.set_button(name, self.base_color)
                    self.coloring_pick_up = False

            elif "delete" in self.kernel.modes:
                if self.kernel.router.clear(src) and self.kernel.scene.clear(src):
                    self.unset_base_state((x, y))

            elif "duplicate" in self.kernel.modes:
                if vel > 0:
                    for act in self.kernel.actives:
                        self.kernel.router.merge(act, src)

                    if (
                        len(self.kernel.actives) > 0
                        and self.kernel.actives[-1][0] == self.tag
                    ):
                        color = self.get_base_state((*self.kernel.actives[-1][2:],))
                    else:
                        color = self.base_color
                    self.set_base_state((x, y), color, force=True)

            elif vel > 0:
                if "shift" not in self.kernel.modes:
                    self.kernel.clear_active()

                if not self.kernel.push_active(src):
                    self.kernel.pop_active(src)

                if "bind" in self.kernel.modes:
                    self.set_base_state(
                        (x, y), self.base_color, force=self.coloring_mode
                    )

    def play_handle(self, channel, msg_t, *data):
        if msg_t == USER:
            x, y, vel = data
            src = (self.tag, channel, x, y)
            if vel > 0:
                self.kernel.scene.enter(src)
                self.kernel.router.note_on(src, vel)
                color = (int(2 + vel // 2.05),) * 3
                self.set_pad(x, y, color)
            else:
                self.kernel.router.note_off(src)
                self.unset_pad(x, y)

    ##################################################
    ## Base states
    ##################################################

    def set_base_state(self, name, val, force=False):
        if force or self.get_base_state(name) == 0:
            self.display_stack[-1][name] = val
            return True
        return False

    def unset_base_state(self, name):
        if name in self.display_stack[-1]:
            del self.display_stack[-1][name]
            return True
        return False

    def get_base_state(self, name, stack_index=-1):
        return self.display_stack[stack_index].get(name, 0)

    def set_base_color(self, val):
        self.base_color = val
        for idx in ("up", "down", "left", "right", "track select"):
            self.set_button(idx, self.base_color)
        self.set_side_led(self.base_color)
        return True

    ##################################################
    ## Pads
    ##################################################

    def get_pad_state(self, x, y, stack_index=-1):
        note = self.xy_to_note(x, y)
        if note is None:
            return None, None, None

        state = self.pad_state_stack[stack_index]
        val = state.get(note, self.get_base_state((x, y), stack_index))
        if isinstance(val, int) and val < 0:
            return self.get_pad_state(x, y, stack_index + val)

        return state, note, val

    def refresh_pad(self, x, y):
        _, note, val = self.get_pad_state(x, y)
        if note is None:
            return False

        self.set_led(note, val, flash=self.is_active_xy(x, y))

    def set_pad(self, x, y, val, force=False):
        state, note, prev_val = self.get_pad_state(x, y)
        if note is None:
            return False

        if force or prev_val != val:
            self.set_led(note, val, flash=self.is_active_xy(x, y))
            state[note] = val

    def unset_pad(self, x, y):
        state, note, _ = self.get_pad_state(x, y)
        if note is None:
            return False

        if note in state:
            del state[note]
            self.refresh_pad(x, y)
            return True
        return False

    ##################################################
    ## Buttons
    ##################################################

    def is_button(self, name):
        return name in self.buttons or name in self.persistent_buttons

    def is_button_on(self, packet, name):
        return packet == (self.channel, CC, self.name_to_controller(name), 127)

    def is_button_off(self, packet, name):
        return packet == (self.channel, CC, self.name_to_controller(name), 0)

    def get_button_state(self, name, stack_index=-1):
        controller = self.name_to_controller(name)
        if name in self.persistent_buttons:
            state = self.persistent_button_state
        else:
            state = self.button_state_stack[stack_index]

        val = state.get(controller, self.get_base_state(name, stack_index))
        if val < 0:
            return self.get_button_state(name, stack_index + val)

        return state, controller, val

    def refresh_button(self, name):
        _, controller, val = self.get_button_state(name)
        self.set_led(controller, val)

    def get_button(self, name):
        _, _, val = self.get_button_state(name)
        return val

    def set_button(self, name, val, force=False):
        state, controller, prev_val = self.get_button_state(name)
        if force or prev_val != val:
            state[controller] = val
            self.set_led(controller, val)

    def unset_button(self, name):
        state, controller, _ = self.get_button_state(name)
        if controller in state:
            del state[controller]
            self.refresh_button(name)
            return True
        return False

    ##################################################
    ## Side LED
    ##################################################

    def set_side_led(self, val):
        self.set_led(99, val)

    ##################################################
    ## Coloring
    ##################################################

    def set_led(self, number, value, pulse=False, flash=False):
        if isinstance(value, int):
            alt_value = 1 if value == 0 else value

            if flash:
                self.midi_out.send_message((240, 0, 32, 41, 2, 16, 10, number, 0, 247))
                self.midi_out.send_message(
                    (240, 0, 32, 41, 2, 16, 35, number, alt_value, 247)
                )
            elif pulse:
                self.midi_out.send_message((240, 0, 32, 41, 2, 16, 10, number, 0, 247))
                self.midi_out.send_message(
                    (240, 0, 32, 41, 2, 16, 40, number, alt_value, 247)
                )
            else:
                self.midi_out.send_message(
                    (240, 0, 32, 41, 2, 16, 10, number, value, 247)
                )
        else:
            self.midi_out.send_message((240, 0, 32, 41, 2, 16, 11, number, *value, 247))

    def next_color(self, color):
        idx = self.color_table.index(color)
        return self.color_table[(idx + 4) % len(self.color_table)]

    def prev_color(self, color):
        idx = self.color_table.index(color)
        return self.color_table[(idx - 4) % len(self.color_table)]

    def dim_color(self, color):
        idx = self.color_table.index(color)
        return self.color_table[(idx - 1) % 4 + (idx // 4) * 4]

    def brighten_color(self, color):
        idx = self.color_table.index(color)
        return self.color_table[(idx + 1) % 4 + (idx // 4) * 4]

    def coloring_enable(self):
        cache_button_state = not self.coloring_mode
        self.coloring_mode = True

        if cache_button_state:
            self.coloring_button_state_cache_stack[-1] = {}
        for name in ("up", "down", "left", "right", "track select"):
            if cache_button_state:
                self.coloring_button_state_cache_stack[-1][name] = self.get_button(name)
            self.set_button(name, self.base_color)
        if self.coloring_pick_up:
            self.set_button("track select", self.active_color)

    def coloring_disable(self):
        self.coloring_mode = False
        self.coloring_pick_up = False

        for (name, color) in self.coloring_button_state_cache_stack[-1].items():
            self.set_button(name, color)

    ##################################################
    ## Display
    ##################################################

    def is_active_display(self):
        return self.base_state == self.display_stack[-1]

    def push_display(self, base_state):
        self.display_stack.append(base_state)
        self.pad_state_stack.append({})
        self.button_state_stack.append({})
        self.coloring_button_state_cache_stack.append({})

        self.coloring_disable()
        self.refresh_display()

    def pop_display(self, *, refresh=True):
        self.display_stack.pop()
        self.pad_state_stack.pop()
        self.button_state_stack.pop()
        self.coloring_button_state_cache_stack.pop()

        if refresh:
            self.coloring_disable()
            self.refresh_display()

    def replace_display(self, base_state):
        self.pop_display(refresh=False)
        self.push_display(base_state)

    def refresh_display(self):
        for note in self.notes:
            self.refresh_pad(*self.note_to_xy(note))
        for name in self.buttons:
            self.refresh_button(name)
        for name in self.persistent_buttons:
            self.refresh_button(name)
        self.set_side_led(self.base_color)

    def flush_display(self, val=0, force=True):
        for note in self.notes:
            self.set_pad(*self.note_to_xy(note), val, force=force)
        for name in self.buttons:
            self.set_button(name, val, force=force)
        for name in self.persistent_buttons:
            self.set_button(name, val, force=force)
        self.set_side_led(self.base_color)
