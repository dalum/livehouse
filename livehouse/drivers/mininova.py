from .generic_driver import GenericDriver
from ..miditypes import *

class MiniNova(GenericDriver):
    name = "MiniNova"

    locked = False
    disable_local_sustain = False

    def initialize(self):
        self.sustain = False

    def enable(self):
        self.midi_in.ignore_types(sysex=False)

    ##################################################
    ## Messages
    ##################################################

    def sustain_on(self, channel):
        self.sustain = True
        self.cc(channel, 64, 127)

    def sustain_off(self, channel):
        self.sustain = False
        self.cc(channel, 64, 0)

    ##################################################
    ## Callback
    ##################################################

    def play_handle(self, channel, msg_t, *data):
        if msg_t == CC:
            controller, value = data
            if controller == 64 and self.disable_local_sustain:
                if self.sustain and value < 64:
                    self.sustain_on(channel)
                elif not self.sustain and value >= 64:
                    self.sustain_off(channel)
