from .generic_driver import GenericDriver
from ..miditypes import *

class PerformVE(GenericDriver):
    name = "PerformVE"

    locked = False

    def enable(self):
        self.midi_in.ignore_types(sysex=False)
        self.load_session(self.kernel.global_state.get("idx"))

    ##################################################
    ## Session
    ##################################################

    def load_session(self, idx):
        super().load_session(idx)

        for msg in self.bank.get("sysex", []):
            self.midi_out.send_message(msg)

    ##################################################
    ## Messages
    ##################################################

    def note_on(self, channel, note, vel, mod=0):
        if mod & LATCH_RELEASE:
            self.nm.latch_release(channel)
            return

        src = (channel, note)
        if mod & LATCH:
            prerefs = self.nm.latch(src)
        else:
            prerefs = self.nm.note_refs[src]
        self.nm.note_refs[src] += 1
        if prerefs > 0:
            return
        self.send_message(channel, NOTE_ON, note, vel)

    ##################################################
    ## Callback
    ##################################################

    def callback(self, msg, data):
        msg, delta_t = msg
        msg_t = msg[0] >> 4
        if msg_t == 0xF:
            msg_t = msg[0]
            channel = None
        else:
            channel = msg[0] & 0x0F
        data = tuple(msg[1:]) if len(msg) > 1 else ()

        if msg_t == SYSEX:
            # Ignore 'success' response
            if msg == [240, 0, 1, 56, 0, 117, 52, 1, 0, 0, 247]:
                return
            X = self.bank.get("sysex", [])
            if delta_t > 1:
                X.clear()
            X.append(msg)
            return

        self.handle(channel, msg_t, *data)
