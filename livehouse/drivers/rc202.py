from .generic_driver import GenericDriver
from ..miditypes import *

class RC202(GenericDriver):
    name = "RC-202"

    locked = False

    def enable(self):
        self.midi_in.ignore_types(sysex=False)
        self.load_session(self.kernel.global_state.get("idx"))

    ##################################################
    ## Session
    ##################################################

    def load_session(self, idx):
        super().load_session(idx)

        for msg in self.bank.get("sysex", []):
            self.midi_out.send_message(msg)

    ##################################################
    ## Callback
    ##################################################

    def callback(self, msg, data):
        msg, delta_t = msg
        msg_t = msg[0] >> 4
        if msg_t == 0xF:
            msg_t = msg[0]
            channel = None
        else:
            channel = msg[0] & 0x0F
        data = tuple(msg[1:]) if len(msg) > 1 else ()

        self.handle(channel, msg_t, *data)
