from .generic_driver import GenericDriver

class ToucheSE(GenericDriver):
    name = "ToucheSE"

    ##################################################
    ## Callback
    ##################################################

    def callback(self, msg, data):
        msg = msg[0]
        if msg[0] == 0xB0:
            self.buffer.raw[msg[1] - 16] = msg[2]
