import json
import os

from copy import deepcopy
from pathlib import Path

class GlobalState:
    def __init__(self, name, default_state={"idx": 0}):
        self.name = name
        self.default_state = default_state
        self.load_state()

    @property
    def banks_path(self):
        return Path(
            os.environ.get("LIVEHOUSE_BANKS", os.path.expanduser("~/.livehouse/banks"))
        )

    def get_state_path(self):
        return self.banks_path.joinpath(f"{self.name}.json")

    def load_state(self):
        file_path = self.get_state_path()
        if os.path.exists(file_path):
            try:
                with open(file_path, "r") as fh:
                    self.state = json.load(fh)
            except json.decoder.JSONDecodeError:
                self.state = deepcopy(self.default_state)
        else:
            self.state = deepcopy(self.default_state)

    def save_state(self):
        file_path = self.get_state_path()
        print("[GLOBAL STATE] Saving to: %s ..." % (file_path), end=" ")
        try:
            with open(file_path, "w") as fh:
                json.dump(self.state, fh, indent=2)
        except Exception:
            print(self.state)
            raise
        print("Done.")

    def get(self, setting, default=None):
        if setting not in self.state:
            self.state[setting] = default
        return self.state[setting]

    def set(self, setting, value):
        if self.state[setting] != value:
            self.state[setting] = value
            self.save_state()
