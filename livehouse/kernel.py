import logging
import os
import rtmidi

from collections import Counter
from threading import Thread
from time import sleep

# from .chorder import Chorder
from .bank import Bank
from .global_state import GlobalState
from .miditypes import *
from .services import Clock, Router, Scene

from .drivers import GenericDriver


class Kernel:
    def __init__(
        self,
        name,
        directory,
        blacklist=[],
        whitelist=[],
        timing_whitelist=[],
        sysex_whitelist=[],
        channels=range(16),
        clock_enable=False,
        **kwargs,
    ):
        self.__dict__.update(locals())
        self.registered_drivers = {}

        self.modes = Counter()

        self.buffer = []

        self.actives = []

        self.global_state = GlobalState("global_state")

        self.clock = Clock(self, Bank("clock"))
        self.router = Router(self, Bank("router"))
        self.scene = Scene(self, Bank("scene"))

        self.in_prober = rtmidi.MidiIn()
        self.out_prober = rtmidi.MidiOut()
        self.all_devices = set()
        self.devices = {}

        self.callbacks = {}

        self.inputs_cache = {}
        self.outputs_cache = {}

        self.enable()

        if self.clock_enable:
            self.clock.start()

    def enable(self):
        self.load_session(self.global_state.get("idx"))

    ##################################################
    ## Thread
    ##################################################

    def start(self):
        t = Thread(target=self.probe_loop, args=(1,))
        t.daemon = True
        t.start()

    def probe_loop(self, interval):
        while True:
            self.probe()
            sleep(interval)

    ##################################################
    ## Device probing
    ##################################################

    def is_whitelisted(self, name_port):
        name, port = name_port
        if any(x in name for x in self.blacklist):
            return False
        if len(self.whitelist) > 0 and not any(x in name for x in self.whitelist):
            return False
        return True

    def probe(self):
        # Inputs
        try:
            ports = map(lambda x: x.split(), self.in_prober.get_ports())
            ports = map(lambda x: (" ".join(x[:-1]), x[-1]), ports)
            ports = filter(lambda x: self.is_whitelisted(x[1]), enumerate(ports))
            inputs = {x: idx for (idx, x) in ports}
        except:
            inputs = {}
        try:
            ports = map(lambda x: x.split(), self.out_prober.get_ports())
            ports = map(lambda x: (" ".join(x[:-1]), x[-1]), ports)
            ports = filter(lambda x: self.is_whitelisted(x[1]), enumerate(ports))
            outputs = {x: idx for (idx, x) in ports}
        except:
            outputs = {}

        if inputs == self.inputs_cache and outputs == self.outputs_cache:
            return
        self.inputs_cache = inputs
        self.outputs_cache = outputs

        lost_devices = list(
            filter(
                lambda x: x.get_identifier() not in inputs
                and x.get_identifier() not in outputs,
                self.get_devices(),
            )
        )
        for device in lost_devices:
            print(
                "[%s] Lost connection to: %s %s (%s: %s)"
                % (self.name, *device.get_description())
            )
            self.remove_device(device)

        identifiers = [dev.get_identifier() for dev in self.get_devices()]
        new_identifiers = {*inputs, *outputs}.difference(identifiers)

        for (name, port) in new_identifiers:
            input_index = inputs.get((name, port), None)
            output_index = outputs.get((name, port), None)

            driver, args, kwargs = self.find_driver(name)
            new_in = rtmidi.MidiIn()
            new_out = rtmidi.MidiOut()

            if input_index is not None:
                new_in.open_port(input_index)
            if output_index is not None:
                new_out.open_port(output_index)

            device = driver(self, name, port, new_in, new_out, *args, **kwargs)

            new_in.set_callback(device.bootstrap_callback)
            if not device._bootstrap():
                print(
                    "[%s] Failed to bootstrap: %s %s (%s: %s) [in: %s, out: %s]"
                    % (self.name, *device.get_description(), input_index, output_index)
                )
                continue

            if kwargs.get("profile", False):
                new_in.set_callback(device.profile_callback)
            elif kwargs.get("test", False):
                new_in.set_callback(device.test_callback)
            else:
                new_in.set_callback(device.callback)
            self.add_device(device)

            device.enable()

            print(
                "[%s] Connected to: %s %s (%s: %s) [in: %s, out: %s]"
                % (self.name, *device.get_description(), input_index, output_index)
            )

    ##################################################
    ## Drivers
    ##################################################

    def register_driver(self, driver, pattern, *args, **kwargs):
        self.registered_drivers[pattern] = (driver, args, kwargs)

    def find_driver(self, name):
        for (pattern, (driver, args, kwargs)) in self.registered_drivers.items():
            if pattern in name:
                return driver, args, kwargs
        return GenericDriver, (), {}

    ##################################################
    ## Devices
    ##################################################

    def add_device(self, device):
        if device.tag is not None:
            self.devices[device.tag] = device
        self.all_devices.add(device)

    def remove_device(self, device):
        device._destroy()
        self.all_devices.discard(device)
        if device.tag in self.devices:
            del self.devices[device.tag]

    def update_tag(self, device, tag):
        if device.tag in self.devices:
            del self.devices[device.tag]
        if tag is not None:
            self.devices[tag] = device

    def get_devices(self):
        return self.all_devices.copy()

    ##################################################
    ## Callbacks
    ##################################################

    def register_callback(self, name, fun):
        if name not in self.callbacks:
            self.callbacks[name] = []
        self.callbacks[name].append(fun)
        return True

    def remove_callback(self, name, fun):
        if name in self.callbacks:
            try:
                self.callbacks[name].remove(fun)
                return True
            except ValueError:
                pass
        return False

    def callback(self, name, *args):
        if name in self.callbacks:
            for fun in self.callbacks[name]:
                fun(*args)
            return True
        return False

    ##################################################
    ## Session
    ##################################################

    def load_session(self, idx):
        self.global_state.set("idx", idx)
        self.router.bank.load_session(idx)
        self.scene.bank.load_session(idx)
        self.clock.bank.load_session(idx)
        self.router.load()
        self.scene.load()
        self.clock.load()

        for device in self.get_devices():
            device.load_session(idx)

    def save_session(self, idx):
        self.global_state.set("idx", idx)
        self.router.save()
        self.scene.save()
        self.clock.save()
        self.router.bank.save_session(idx)
        self.scene.bank.save_session(idx)
        self.clock.bank.save_session(idx)

        for device in self.get_devices():
            device.save_session(idx)

    def clear_session(self, idx):
        if self.global_state.get("idx") == idx:
            self.router.bank.clear_session(idx)
            self.scene.bank.clear_session(idx)
            self.clock.bank.clear_session(idx)

            for device in self.get_devices():
                device.clear_session(idx)

    ##################################################
    ## Messages
    ##################################################

    def user(self, tag, *args):
        if tag in self.devices:
            self.devices[tag].user(*args)

    def cc(self, tag, *args):
        if tag in self.devices:
            self.devices[tag].cc(*args)

    def nrpn(self, tag, *args):
        if tag in self.devices:
            self.devices[tag].nrpn(*args)

    def note_on(self, tag, *args):
        if tag in self.devices:
            self.devices[tag].note_on(*args)

    def note_off(self, tag, *args):
        if tag in self.devices:
            self.devices[tag].note_off(*args)

    def send(self, tag, *args):
        if tag in self.devices:
            self.devices[tag].send(*args)

    def _send(self, tag, *args):
        if tag in self.devices:
            self.devices[tag]._send(*args)

    def all_off(self):
        for device in self.get_devices():
            device.all_off()

    def broadcast(self, msg):
        for device in self.get_devices():
            device.midi_out.send_message(msg)

    ##################################################
    ## Timing
    ##################################################

    def time_clock(self):
        for device in self.get_devices():
            device.time_clock()

    def time_start(self):
        for device in self.get_devices():
            device.time_start()

    def time_continue(self):
        for device in self.get_devices():
            device.time_continue()

    def time_stop(self):
        for device in self.get_devices():
            device.time_stop()

    ##################################################
    ## Active device
    ##################################################

    def push_active(self, src):
        if src in self.actives:
            return False

        self.actives.append(src)
        self.callback("active push", src)
        return True

    def pop_active(self, src):
        if src not in self.actives:
            return False

        self.actives.remove(src)
        self.callback("active pop", src)
        return True

    def clear_active(self):
        self.actives.clear()
        self.callback("active clear")

    ##################################################
    ## Modes
    ##################################################

    def cycle_modes(self, *modes, stop=True):
        """
        Returns `True` if one of the modes was activated, `False` otherwise.
        """
        for (mode, next_mode) in zip(modes[:-1], modes[1:]):
            if self.modes[mode] > 0:
                self.modes[mode] = 1
                self.pop_mode(mode)
                self.push_mode(next_mode)
                return True
        if self.modes[next_mode] > 0:
            self.modes[next_mode] = 1
            self.pop_mode(next_mode)
            return False
        self.push_mode(modes[0])
        return True

    def toggle_mode(self, mode):
        """
        Returns `True` if the mode is activated, `False` otherwise.
        """
        if self.modes[mode] > 0:
            self.modes[mode] = 1
            self.pop_mode(mode)
            return False
        else:
            self.push_mode(mode)
            return True

    def push_mode(self, mode):
        """
        Returns `True` if the mode is was activated, `False` otherwise.
        """
        self.modes[mode] += 1
        if self.modes[mode] == 1:
            for device in self.get_devices():
                device._set_mode(mode)
            return True
        return False

    def pop_mode(self, mode):
        """
        Returns `True` if the mode became deactivated, `False` otherwise.
        """
        if self.modes[mode] > 1:
            self.modes[mode] -= 1
            return False
        else:
            return self.unset_mode(mode)

    def unset_mode(self, mode):
        """
        Returns `True` if the mode became deactivated, `False` otherwise.
        """
        if self.modes[mode] == 0:
            return False
        self.modes.pop(mode)

        if mode == "bind":
            if "show" in self.modes:
                self.hide_active()
        elif mode == "listen":
            self.clear_status("msg_t")
            self.clear_modes()

        for device in self.get_devices():
            device._unset_mode(mode)

        return True

    def clear_modes(self):
        self.unset_mode("bind")
        self.unset_mode("latch")
        self.unset_mode("latch release")
        self.unset_mode("show")
        for x in range(1, 16):
            self.unset_mode(f"vel{x}")

    def get_mod(self):
        mod = (
            LATCH
            if "latch" in self.modes
            else LATCH_RELEASE
            if "latch release" in self.modes
            else NORMAL
        )

        for x in range(1, 16):
            if f"vel{x}" in self.modes:
                vel = x
                break
        else:
            vel = 0

        return mod | vel

    ##################################################
    ## Status
    ##################################################

    def set_status(self, status, value):
        for device in self.get_devices():
            device._set_status(status, value)

    def clear_status(self, status):
        for device in self.get_devices():
            device._clear_status(status)

    ##################################################
    ## Parser
    ##################################################

    def input(self):
        try:
            s = input("\033[93mLive\033[95mHouse\033[94m>\033[0m ")
            if len(s) > 0:
                self.parse(s)
        except EOFError:
            sleep(1)

    def parse(self, s):
        try:
            namespace = {
                "kernel": self,
                "clock": self.clock,
            }
            namespace.update(self.devices)

            ret = eval(s, globals(), namespace)
            print(ret)
            print()
        except KeyboardInterrupt:
            raise
        except Exception as e:
            logging.error(logging.traceback.format_exc())
