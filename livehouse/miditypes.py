NOTE_OFF = 0x8
NOTE_ON = 0x9
POLYPHONIC_AFTERTOUCH = 0xA
CC = 0xB
PROGRAM_CHANGE = 0xC
CHANNEL_AFTERTOUCH = 0xD
PITCH_BEND = 0xE
SYSEX = 0xF0

SONG_POSITION_POINTER = 0xF2
TIME_CLOCK = 0xF8
TIME_START = 0xFA
TIME_CONTINUE = 0xFB
TIME_STOP = 0xFC

# LiveHouse extensions
NRPN = 0x80
USER = 0xB0

NORMAL = 0x00
LATCH = 0x10
LATCH_RELEASE = 0x20
CYCLE = 0x40
