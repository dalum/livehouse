from time import sleep, time

from .generic_handler import GenericHandler
from ..miditypes import *

class CasioCDP100MiniNovaRouter(GenericHandler):
    name = "Casio CDP-100 + Mininova Router"
    casio_tag = ''
    mininova_tag = ''
    nanokontrol_tag = ''
    reroutes = {}

    mod = 0

    def initialize(self):
        self.devices = set()
        self.channels = set()
        self.last_toggle = time()
        self.routes = {}
        self.ranges = {}

        self.range_lower = None
        self.set_range_lower = False
        self.set_range_upper = False

    def register(self, obj):
        self.devices.add(obj)
        self.routes[obj] = {}
        self.ranges[obj] = {}

        for channel in obj.channels:
            reroute = self.reroutes.get(obj.tag, {})
            self.routes[obj][reroute.get(channel, channel)] = channel
            self.ranges[obj][reroute.get(channel, channel)] = range(128)

        if obj.tag == self.nanokontrol_tag:
            for channel in range(8):
                if channel in self.channels:
                    obj.cc(obj.channels[0], channel, 127)
                else:
                    obj.cc(obj.channels[0], channel, 0)

    def deregister(self, obj):
        self.devices.discard(obj)

    def note_on(self, note, vel):
        for device in self.devices:
            for channel in self.channels:
                if channel in self.routes[device] and note in self.ranges[device][channel]:
                    device.note_on(self.routes[device][channel], note, vel, self.mod)

    def note_off(self, note, vel):
        for device in self.devices:
            for channel in self.channels:
                if channel in self.routes[device] and note in self.ranges[device][channel]:
                    device.note_off(self.routes[device][channel], note, vel, self.mod)

    def sustain_on(self):
        for device in self.devices:
            for channel in self.channels:
                if channel in self.routes[device]:
                    device.sustain_on(self.routes[device][channel])

    def sustain_off(self):
        for device in self.devices:
            for channel in self.channels:
                if channel in self.routes[device]:
                    device.sustain_off(self.routes[device][channel])

    def all_off(self, channel):
        for device in self.devices:
            if channel in self.routes[device]:
                device.all_off(self.routes[device][channel])

    def toggle_channel(self, channel):
        if not (0 <= channel < 16):
            return False

        if channel not in self.channels:
            self.channels.add(channel)
            # print("[SIMPLE ROUTER] enabled:", channel)
        else:
            self.all_off(channel)
            self.channels.discard(channel)
            # print("[SIMPLE ROUTER] disabled:", channel)

    def handle(self, obj, *packet):
        if obj.tag == self.casio_tag:
            return self.casio_handle(obj, *packet)
        elif obj.tag == self.mininova_tag:
            return self.mininova_handle(obj, *packet)
        elif obj.tag == self.nanokontrol_tag:
            return self.nanokontrol_handle(obj, *packet)
        else:
            return False

    def casio_handle(self, obj, channel, msg_t, *data):
        if channel != 0:
            return False

        if msg_t == CC:
            controller, value = data
            if controller == 64:
                if value >= 64:
                    self.sustain_on()
                else:
                    self.sustain_off()

        if msg_t == NOTE_ON:
            note, vel = data
            if self.set_range_lower:
                self.set_range_lower = False
                self.set_range_upper = True
                self.range_lower = note
            elif self.set_range_upper:
                self.set_range_upper= False
                for device in self.devices:
                    for channel in self.channels:
                        if channel in self.ranges[device]:
                            self.ranges[device][channel] = range(self.range_lower, note + 1)
            else:
                self.note_on(note, vel)
        elif msg_t == NOTE_OFF:
            note, vel = data
            self.note_off(note, vel)

        return True

    def mininova_handle(self, obj, channel, msg_t, *data):
        if msg_t == CC:
            controller, value = data
            if controller == 64:
                self.set_range_lower = value >= 64
                self.set_range_upper = False

        return False

    def nanokontrol_handle(self, obj, channel, msg_t, *data):
        if msg_t == CC:
            controller, value = data
            if controller in range(64, 72):
                if value == 127:
                    self.toggle_channel(controller - 64)
                    if controller - 64 in self.channels:
                        obj.cc(channel, controller, 127)
                    else:
                        obj.cc(channel, controller, 0)
                return True

        return False
