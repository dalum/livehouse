from collections import deque
from threading import Thread
from time import sleep, time

from ..miditypes import *


class Clock:
    def __init__(
        self, kernel, bank, running=False, self_monitor=False, default_tempo=120, max_drop=10
    ):
        self.__dict__.update(locals())

        self.subscribers = set()

        self.taps = deque([], 5)
        self._taps = deque([], 3)
        self.max_tempo = 300
        self.min_tempo = 30
        self.threshold = 0.05

        self.count = 0
        self._count_beat = 0

    def subscribe(self, obj):
        """Clock.subscribe(obj)

        Subscribe to the beat and offbeats of the clock.  Subscribing
        objects must define the methods `beat(self)` and
        `offbeat(self)`.
        """
        self.subscribers.add(obj)

    def unsubscribe(self, obj):
        self.subscribers.discard(obj)

    def load(self):
        self.set_tempo(self.bank.get("tempo", self.default_tempo))

    def save(self):
        self.bank.set("tempo", self.tempo)

    def start(self):
        t = Thread(target=self.tick_loop, args=())
        t.daemon = True
        t.start()

    def tick_loop(self):
        while True:
            t1 = self.t0 + self.count * self.inc
            delta = t1 - time()
            sleep(max(0, delta))

            if -delta > self.max_drop * self.inc:
                self.count = 0
                self.t0 = time()

            self.tick()

    def tick(self):
        self.count += 1
        if self.count % 24 == self._count_beat:
            self.beat()
        elif self.count % 24 == self._count_offbeat:
            self.offbeat()

        self.kernel.time_clock()
        # print("[CLOCK]: %d" % self.count)

    def time_start(self):
        self.set_beat()
        self.kernel.time_start()
        self.running = True

    def time_continue(self):
        self.set_beat()
        self.kernel.time_continue()
        self.running = True

    def time_stop(self):
        self.kernel.time_stop()
        self.running = False

    def get_tempo(self):
        return self.tempo

    def set_beat(self):
        self._count_beat = self.count % 24
        self._count_offbeat = (self._count_beat + 12) % 24
        self.beat()

    def set_tempo(self, tempo):
        self.tempo = max(30, min(int(tempo), 300))
        self._count_beat = (self._count_beat - self.count) % 24
        self._count_offbeat = (self._count_beat + 12) % 24
        self.count = 0
        self.inc = 60 / self.get_tempo() / 24
        self.t0 = time()

    def tap(self):
        t = time()
        if len(self.taps) == 0 or t - self.taps[-1] > self.threshold:
            self.taps.append(time())
        else:
            return
        diffs = [
            60 / (self.taps[i + 1] - self.taps[i]) for i in range(len(self.taps) - 1)
        ]
        N = len(diffs)
        if N > 3:
            self.set_beat()
            self.set_tempo(sum(diffs) / len(diffs))

    def beat(self):
        for obj in self.subscribers:
            obj.beat()

    def offbeat(self):
        for obj in self.subscribers:
            obj.offbeat()

    # def _tap(self):
    #     self._taps.append(time())
    #     if len(self._taps) < 2:
    #         return
    #
    #     diff = (len(self._taps) - 1) * 60 / (self._taps[-1] - self._taps[0])
    #     self._tempo = diff
    #
    # def tempos(self):
    #     return self.get_tempo(), self._tempo
