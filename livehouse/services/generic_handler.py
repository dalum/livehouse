class GenericHandler:
    def __init__(self, kernel, active=False, handlers=None, **kwargs):
        self.__dict__.update(kwargs)
        self.kernel = kernel
        self.active = active
        self.handlers = {} if handlers is None else handlers
        self.initialize()

    def initialize(self):
        pass

    def register(self, obj):
        for (handler_name, handler) in self.handlers.items():
            if handler.active:
                handler.register(obj)

    def deregister(self, obj):
        for (handler_name, handler) in self.handlers.items():
            if handler.active:
                handler.deregister(obj)

    def request_state(self, obj):
        for (handler_name, handler) in self.handlers.items():
            if handler.active:
                handler.request_state(self)

    def handle(self, obj, *packet):
        for (handler_name, handler) in self.handlers.items():
            if handler.active and handler.handle(obj, *packet):
                return True

        return False

    def load_session(self, obj, idx):
        pass

    def save_session(self, obj, idx):
        pass

    def clear_session(self, obj, idx):
        pass

    def _set_mode(self, mode):
        pass

    def _unset_mode(self, mode):
        pass

    def _clear_status(self, status):
        pass

    def _set_status(self, status, value):
        pass
