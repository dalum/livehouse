from ..converters import note_to_tuple, tuple_to_note
from ..miditypes import *


class Router:
    def __init__(self, kernel, bank, **kwargs):
        self.kernel = kernel
        self.bank = bank
        self.__dict__.update(kwargs)
        self.cycles = {}

    def load(self):
        self.tables = {
            note_to_tuple(pair["from"]): list(map(note_to_tuple, pair["to"]))
            for pair in self.bank.get("data", [])
            if pair["from"] is not None
        }

    def save(self):
        self.bank.set(
            "data",
            [
                {"from": tuple_to_note(k), "to": list(map(tuple_to_note, v))}
                for k, v in self.tables.items()
            ],
        )

    def len(self, src):
        if src not in self.kernel.bank.get("router", {}):
            return 0
        return len(self.kernel.bank.get("router", {})[src])

    def duplicate(self, src, dest):
        if src not in self.tables:
            return False

        self.tables[dest] = self.tables[src].copy()
        return True

    def merge(self, src, dest):
        if src not in self.tables:
            return False

        if dest not in self.tables:
            self.tables[dest] = []

        for entry in self.tables[src]:
            if entry not in self.tables[dest]:
                self.tables[dest].append(entry)

        return True

    def bind(self, src, dest):
        if src not in self.tables:
            self.tables[src] = []
        table = self.tables[src]

        if dest not in table:
            table.append(dest)
            return True
        else:
            table.remove(dest)
            return False

    def clear(self, src):
        if src in self.tables:
            del self.tables[src]
        return True

    def set_tag(self, src, new_tag):
        if src not in self.tables:
            return False
        for (idx, (tag, channel, note, mod)) in enumerate(self.tables[src]):
            self.tables[src][idx] = (new_tag, new_channel, note, mod)
        return True

    def set_channel(self, src, new_channel):
        if src not in self.tables:
            return False
        for (idx, (tag, channel, note, mod)) in enumerate(self.tables[src]):
            self.tables[src][idx] = (tag, new_channel, note, mod)
        return True

    def set_transpose(self, src, transpose):
        if src not in self.tables:
            return False
        for (idx, (tag, channel, note, mod)) in enumerate(self.tables[src]):
            self.tables[src][idx] = (tag, channel, note + transpose, mod)
        return True

    def set_mod(self, src, new_mod):
        if src not in self.tables:
            return False
        for (idx, (tag, channel, note, mod)) in enumerate(self.tables[src]):
            self.tables[src][idx] = (tag, channel, note, new_mod)
        return True

    def note_on(self, src, vel, transpose=0):
        if src not in self.tables:
            return False

        ncycle = -1
        for (tag, channel, note, mod) in self.tables[src]:
            if mod & CYCLE:
                ncycle += 1
                if ncycle != self.cycles.setdefault(src, 0):
                    continue
            self.kernel.note_on(tag, channel, note + transpose, vel, mod)
        return True

    def note_off(self, src, transpose=0):
        if src not in self.tables:
            return False

        ncycle = -1
        for (tag, channel, note, mod) in self.tables[src]:
            if mod & CYCLE:
                ncycle += 1
                if ncycle != self.cycles.setdefault(src, 0):
                    continue
            self.kernel.note_off(tag, channel, note + transpose, 0, mod)

        if ncycle >= 0:
            self.cycles[src] = (self.cycles.setdefault(src, 0) + 1) % (ncycle + 1)

        return True

    def show(self, src, vel=1, mod=0):
        if src not in self.tables:
            return

        for (tag, channel, note, mod_) in self.tables[src]:
            if mod_ != mod:
                continue
            self.kernel.note_on(tag, channel, note, vel)
