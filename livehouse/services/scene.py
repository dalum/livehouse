from ..converters import note_to_tuple, tuple_to_note, spec_to_pair, tuple_val_to_spec
from ..miditypes import *


class Scene:
    def __init__(self, kernel, bank, **kwargs):
        self.kernel = kernel
        self.bank = bank
        self.__dict__.update(kwargs)

    def load(self):
        self.tables = {
            note_to_tuple(pair["from"]): {
                k: v for k, v in map(spec_to_pair, pair["to"])
            }
            for pair in self.bank.get("data", [])
        }

    def save(self):
        self.bank.set(
            "data",
            [
                {
                    "from": tuple_to_note(k),
                    "to": [tuple_val_to_spec(tup, val) for tup, val in v.items()],
                }
                for k, v in self.tables.items()
            ],
        )

    def bind(self, src, dest, value):
        if src not in self.tables:
            self.tables[src] = {}
        table = self.tables[src]
        table[dest] = value

    def unbind(self, src, dest):
        if src in self.tables and dest in self.tables[src]:
            del self.tables[src][dest]

    def clear(self, src):
        if src in self.tables:
            del self.tables[src]
        return True

    def enter(self, src):
        if src in self.tables:
            for (dest, value) in self.tables[src].items():
                self.kernel._send(*dest, value)
