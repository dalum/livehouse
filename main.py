#!/usr/bin/env -S python3 -u

import argparse

from time import sleep

from livehouse import *
from livehouse.services import *
from livehouse.drivers import *
from livehouse.daemons import *


class MyBuffer(Buffer):
    interval = 0.0005
    window_size = 0.005
    length = 5

    def __init__(self, kernel, *tags):
        self.kernel = kernel
        self.tags = tuple(tags)
        self.raw = [0] * self.length
        self.x = self.raw.copy()

    def send(self):
        for i in range(self.length):
            if self.raw[i] != self.x[i]:
                self.x[i] = self.raw[i]
                for tag in self.tags:
                    self.kernel.user(tag, i, self.x[i])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i",
        "--interactive",
        dest="interactive",
        action="store_const",
        const=True,
        default=False,
        help="make the session interactive",
    )
    args = parser.parse_args()

    # Setup
    kernel = Kernel(
        "KERNEL",
        "~/.livehouse",
        blacklist=[
            "System",
            "Midi Through",
            "RtMidi",
            "Launchpad Pro MIDI 1",
            "RC-202 MIDI 2",
            "R16 MIDI 1",
            "Client-",
        ],
        clock_enable=True,
    )

    seq = Sequencer(
        "SEQUENCER",
    )

    buf = MyBuffer(kernel, "circuit_1", "circuit_yellow", "circuit_green")
    cp = LaunchpadProCircuitProgrammer(kernel)
    # sm = CircuitProgrammerSmartMenu(kernel, active=True)
    sm = LaunchpadProCircuitProgrammerSmartMenu(kernel, active=True)
    sends = LaunchpadProSendsOverlay(kernel)
    sh = LaunchpadProSessionOverlay(kernel)
    tempo = LaunchpadProTempoOverlay(kernel)
    note = LaunchpadProNoteOverlay(kernel, handlers={"sm": sm})
    rout = CasioCDP100MiniNovaRouter(
        kernel,
        active=True,
        casio_tag="casio_cdp100",
        mininova_tag="mininova",
        nanokontrol_tag="nanokontrol",
        reroutes={
            "rc202": {0: 1},
            "mininova": {0: 0},
            "circuit_1": {0: 2, 1: 3},
            "circuit_yellow": {0: 2, 1: 3},
            "monostation": {0: 4, 1: 5},
            "circuit_green": {0: 6, 1: 7},
        },
    )

    kernel.register_driver(
        LaunchpadPro,
        "Launchpad Pro MIDI 2",
        handlers={
            "user": cp,
            "session": sh,
            "note": note,
            "click": tempo,
            "sends": sends,
            "sm": sm,
        },
        buffer=buf,
        tag="launchpad_pro",
    )

    kernel.register_driver(
        Circuit,
        "Circuit MIDI 1",
        channels=(0, 1, 9, 15),
        handlers={"user": cp, "note": note, "sm": sm, "router": rout},
        buffer=buf,
        autodetect_tag=True,
        # local_mode=True,
        # reduced_session=True,
    )

    kernel.register_driver(
        GenericDriver,
        "Circuit Mono Station MIDI 1",
        buffer=buf,
        tag="circuit_mono_station",
    )

    kernel.register_driver(
        PerformVE,
        "Perform-VE MIDI 1",
        tag="perform_ve",
        handlers={"note": note},
    )
    kernel.register_driver(RC202, "RC-202 MIDI 1", tag="rc202")

    kernel.register_driver(
        ToucheSE,
        "TOUCHE_SE MIDI 1",
        buffer=buf,
        tag="touche_se",
    )

    kernel.register_driver(
        GenericDriver,
        "MIDIMATE II",
        buffer=buf,
        clock_in=False,
    )

    kernel.register_driver(
        GenericDriver,
        "MIDI4x4 MIDI 1",
        handlers={"router": rout},
        channels=(0, 1),
        tag="casio_cdp100",
    )

    kernel.register_driver(
        GenericDriver,
        "MIDI4x4 MIDI 2",
        clock_out=False,
        clock_in=False,
        tag="rc202",
        handlers={"router": rout},
        channels=(0,),
    )

    kernel.register_driver(
        MiniNova,
        "MIDI4x4 MIDI 3",
        handlers={"router": rout},
        tag="mininova",
        channels=(0,),
        disable_local_sustain=True,
    )

    kernel.register_driver(
        GenericDriver,
        "MIDI4x4 MIDI 4",
        handlers={"router": rout},
        tag="monostation",
        channels=(0, 1),
    )

    kernel.register_driver(
        GenericDriver,
        "nanoKONTROL2 MIDI 1",
        handlers={"router": rout},
        tag="nanokontrol",
        channels=(14,),
    )

    kernel.register_driver(
        GenericDriver,
        "ESI MIDIMATE eX MIDI 1",
        tag="nord_drum_3p",
        clock_in=False,
    )

    kernel.register_driver(
        GenericDriver,
        "ESI MIDIMATE eX MIDI 2",
        tag="nord_drum_3p",
        clock_in=False,
    )

    kernel.start()
    buf.start()

    while True:
        if args.interactive:
            kernel.input()
        else:
            sleep(1)
